Rot               | HSV:   0° 100% 100% | HSL:   0° 100%  50% | RGB: 100%   0%   0%
Orange            | HSV:  30° 100% 100% | HSL:  30° 100%  50% | RGB: 100%  50%   0%
Gelb              | HSV:  60° 100% 100% | HSL:  60° 100%  50% | RGB: 100% 100%   0%
Dunkelgrün        | HSV: 120° 100%  50% | HSL: 120° 100%  25% | RGB:   0%  50%   0%
Violett           | HSV: 270° 100% 100% | HSL: 270° 100%  50% | RGB:  50%   0% 100%
Schwarz           | HSV:   0°   0%   0% | HSL:   0°   0%   0% | RGB:   0%   0%   0%
Blau              | HSV: 240° 100% 100% | HSL: 240° 100%  50% | RGB:   0%   0% 100%
Braun             | HSV:  20°  75%  36% | HSL:  20°  60%  23% | RGB:  36%  18%   9%
Weiß              | HSV:   0°   0% 100% | HSL:   0°   0% 100% | RGB: 100% 100% 100%
Grün              | HSV: 120° 100% 100% | HSL: 120° 100%  50% | RGB:   0% 100%   0%
Cyan              | HSV: 180° 100% 100% | HSL: 180° 100%  50% | RGB:   0% 100% 100%
Magenta           | HSV: 300° 100% 100% | HSL: 300° 100%  50% | RGB: 100%   0% 100%
Blaugrün          | HSV: 150° 100% 100% | HSL: 150° 100%  50% | RGB:   0% 100%  50%
Grünblau          | HSV: 210° 100% 100% | HSL: 210° 100%  50% | RGB:   0%  50% 100%
Grüngelb          | HSV:  90° 100% 100% | HSL:  90° 100%  50% | RGB:  50% 100%   0%
Blaurot           | HSV: 330° 100% 100% | HSL: 330° 100%  50% | RGB: 100%   0%  50%
Zinnober          | HSV:  15° 100% 100% | HSL:  15° 100%  50% | RGB: 100%  25%   0%
Indigo            | HSV: 255° 100% 100% | HSL: 255° 100%  50% | RGB:  25%   0% 100%
Leichtes Blaugrün | HSV: 135° 100% 100% | HSL: 135° 100%  50% | RGB:   0% 100%  25%
Blaucyan          | HSV: 195° 100% 100% | HSL: 195° 100%  50% | RGB:   0%  75% 100%
Leichtes Grüngelb | HSV:  75° 100% 100% | HSL:  75° 100%  50% | RGB:  75% 100%   0%
Rotmagenta        | HSV: 315° 100% 100% | HSL: 315° 100%  50% | RGB: 100%   0%  75%
Safran            | HSV:  45° 100% 100% | HSL:  45° 100%  50% | RGB: 100%  75%   0%
Blaumagenta       | HSV: 285° 100% 100% | HSL: 285° 100%  50% | RGB:  75%   0% 100%
Grüncyan          | HSV: 165° 100% 100% | HSL: 165° 100%  50% | RGB:   0% 100%  75%
Leichtes Grünblau | HSV: 225° 100% 100% | HSL: 225° 100%  50% | RGB:   0%  25% 100%
Limett            | HSV: 105° 100% 100% | HSL: 105° 100%  50% | RGB:  25% 100%   0%
Leichtes Blaurot  | HSV: 345° 100% 100% | HSL: 345° 100%  50% | RGB: 100%   0%  25%
===== RESULT =====
["Leichtes Blaurot",255i,0i,63i]
