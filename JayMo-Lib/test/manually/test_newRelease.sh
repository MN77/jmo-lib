#!/bin/bash

for i in *.jmo; do
	echo
	echo "=== Test: $i"
	
#	java -cp "/home/mike/Prog/Java/Jar/jmo-lib.jar" -jar /home/mike/Prog/Java/Jar/jmo.jar "$i"
#	java -cp "/home/mike/Prog/Java/Jar/jmo-lib.jar,/home/mike/Prog/Java/Jar/jmo.jar" org.jmo_lang.cli.JMo_CLI "$i"
#	java -cp "/home/mike/Prog/Java/Jar/jmo-lib.jar:/home/mike/Prog/Java/Jar/jmo.jar" org.jmo_lang.cli.JMo_CLI "$i"
#	java -cp "/home/mike/Prog/Java/Jar/jmo-lib.jar:/home/mike/Prog/Java/Jar/jmo.jar" org.jmo_lang.JMo "$i"
	java -cp "/home/mike/Dist/build/OUT/jaymo-cli-aio.jar" org.jaymo_lang.cli.Main_JayMo "$i"

#	java -cp "/home/mike/Prog/Java/Jar/jmo.jar" org.jmo_lang.cli.JMo_CLI "$i"
done
