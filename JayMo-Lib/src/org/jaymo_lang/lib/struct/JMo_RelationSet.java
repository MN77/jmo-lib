/*******************************************************************************
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.struct;

import java.util.HashMap;
import java.util.Set;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Number;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.lib.struct.RelationSet;


/**
 * @author Michael Nitsche
 * @created 11.09.2022
 */
public class JMo_RelationSet extends A_ObjectSimple {

	private final ArgCallBuffer   initArg;
	private RelationSet<I_Object> rs = null;


	public JMo_RelationSet( final Call size ) {
		this.initArg = new ArgCallBuffer( 0, size );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final JMo_Int arg = this.initArg.init( cr, this, JMo_Int.class );
		this.rs = new RelationSet<>( arg.rawInt( cr ) );
	}

	@Override
	public String toString() {
		return this.getTypeName() + "<" + this.rs.used() + ">";
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		return type == STYPE.DESCRIBE
			? this.rs.toString()
			: this.toString();
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "put":
				return this.mPut( cr );
			case "link":
				return this.mLink( cr );
			case "linked":
				return this.mLinked( cr );
			case "linkedWith":
				return this.mLinkedWith( cr );
			case "getRelation":
				return this.mGetRelation( cr );
			case "setRelation":
				return this.mSetRelation( cr );
			case "relatedWith":
				return this.mRelatedWith( cr );
			case "unlink":
				return this.mUnlink( cr );
			case "len":
			case "length":
				return this.mLength( cr );
			case "size":
				return this.mSize( cr );
		}
		return null;
	}

	/**
	 * °getRelation(Object o1, Object o2)Dec # Returns the relation between two objects.
	 */
	private JMo_Dec mGetRelation( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_Object.class, I_Object.class );
		final double r = this.rs.getRelation( args[0], args[1] );
		return JMo_Dec.valueOf( cr, r );
	}

	/**
	 * °len ^ length
	 * °length()Int # Returns the current length of this RelationSet.
	 */
	private JMo_Int mLength( final CallRuntime cr ) {
		cr.argsNone();
		return new JMo_Int( this.rs.used() );
	}

	/**
	 * °link(Object o, Object other...) Same # Link one object with other objects.
	 */
	private JMo_RelationSet mLink( final CallRuntime cr ) {
		final I_Object[] args = cr.argsVarAdvance( this, 1, 1 );

		for( int i = 1; i < args.length; i++ )
			try {
				this.rs.link( args[0], args[i] );
			}
			catch( final Exception e ) {
				throw new RuntimeError( cr, "Link failed", e.getMessage() );
			}

		return this;
	}

	/**
	 * °linked(Object o1, Object o2)Bool # Returns true, if these objects are connected.
	 */
	private JMo_Bool mLinked( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_Object.class, I_Object.class );
		final boolean b = this.rs.linked( args[0], args[1] );
		return JMo_Bool.getObject( b );
	}

	/**
	 * °linkedWith(Object o)List # Returns a list with all linked objects.
	 */
	private JMo_List mLinkedWith( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_Object.class )[0];
		final SimpleList<I_Object> linked = this.rs.linkedWith( arg );

		return new JMo_List( linked );
	}

	/**
	 * °put(Object o...)Same # Add a object to this set, if it is not already present.
	 */
	private JMo_RelationSet mPut( final CallRuntime cr ) {
		final I_Object[] args = cr.argsVar( this, 1, I_Object.class );
		for( final I_Object o : args )
			this.rs.put( o );
		return this;
	}

	/**
	 * °relatedWith(Object o)Map # Returns a map with all linked objects.
	 */
	private JMo_Map mRelatedWith( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_Object.class )[0];
		final HashMap<I_Object, Double> linked = this.rs.relatedWith( arg );
		final Set<I_Object> keySet = linked.keySet();

		final SimpleList<I_Object> keys = new SimpleList<>( keySet.size() );
		keys.addAll( keySet );

		final SimpleList<I_Object> objects = new SimpleList<>( linked.values().size() );
		for( final Double d : linked.values() )
			objects.add( JMo_Dec.valueOf( cr, d ) );

		return new JMo_Map( keys, objects );
	}

	/**
	 * °setRelation(Object o1, Object o2, Number value)Same # Set the relation of two objects to the given value.
	 */
	private JMo_RelationSet mSetRelation( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_Object.class, I_Object.class, I_Number.class );
		final double value = Lib_Convert.toDouble( cr, args[2] );
		Lib_Error.ifNotBetween( cr, 0, 1, value, "value" );

		this.rs.setRelation( args[0], args[1], value );
		return this;
	}

	/**
	 * °size()Int # Returns the maximum size of items to store in this RelationSet.
	 */
	private JMo_Int mSize( final CallRuntime cr ) {
		cr.argsNone();
		return new JMo_Int( this.rs.size() );
	}

	/**
	 * °unlink(Object o, Object other...) Same # Remove the link between one object to other objects.
	 */
	private JMo_RelationSet mUnlink( final CallRuntime cr ) {
		final I_Object[] args = cr.argsVarAdvance( this, 1, 1 );
		final I_Object base = args[0];

		for( int i = 1; i < args.length; i++ )
			this.rs.unlink( base, args[i] );
		return this;
	}

}
