/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib;

import org.jaymo_lang.apix.HELP_FILE;
import org.jaymo_lang.apix.TypeDictionary;
import org.jaymo_lang.lib.bricklink.JMo_BrickLinkAPI;
import org.jaymo_lang.lib.crypt.JMo_Base32;
import org.jaymo_lang.lib.crypt.JMo_Base32Hex;
import org.jaymo_lang.lib.crypt.JMo_Base64;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherBlowfish;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherDES;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherRC2;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherRC4;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherRijndael;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherRot13;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherRot18;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherRot26;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherRot47;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherRot5;
import org.jaymo_lang.lib.crypt.cipher.JMo_CipherTripleDES;
import org.jaymo_lang.lib.crypt.hash.JMo_HashMD5;
import org.jaymo_lang.lib.crypt.hash.JMo_HashSHA;
import org.jaymo_lang.lib.css.JMo_Css;
import org.jaymo_lang.lib.csv.JMo_Csv;
import org.jaymo_lang.lib.emoji.JMo_Emoji;
import org.jaymo_lang.lib.font.JMo_Font;
import org.jaymo_lang.lib.fuzzy.JMo_Fuzzy;
import org.jaymo_lang.lib.graphic.JMo_Color;
import org.jaymo_lang.lib.graphic.JMo_ColorHSL;
import org.jaymo_lang.lib.graphic.JMo_ColorHSV;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.http.JMo_HttpClient;
import org.jaymo_lang.lib.jar.JMo_Jar;
import org.jaymo_lang.lib.json.JMo_Json;
import org.jaymo_lang.lib.json.JMo_JsonObject;
import org.jaymo_lang.lib.miniconf.JMo_MIniConf;
import org.jaymo_lang.lib.miniconf.JMo_MIniConfResult;
import org.jaymo_lang.lib.ntp.JMo_NtpClient;
import org.jaymo_lang.lib.ntp.JMo_SntpClient;
import org.jaymo_lang.lib.ollama.JMo_OllamaAPI;
import org.jaymo_lang.lib.properties.JMo_PropertiesFile;
import org.jaymo_lang.lib.struct.JMo_RelationSet;
import org.jaymo_lang.lib.swing.control.JMo_Swing_Button;
import org.jaymo_lang.lib.swing.control.JMo_Swing_Label;
import org.jaymo_lang.lib.swing.control.JMo_Swing_ProgressBar;
import org.jaymo_lang.lib.swing.control.JMo_Swing_Scale;
import org.jaymo_lang.lib.swing.control.JMo_Swing_Slider;
import org.jaymo_lang.lib.swing.control.JMo_Swing_TabbedPane;
import org.jaymo_lang.lib.swing.control.JMo_Swing_TextArea;
import org.jaymo_lang.lib.swing.control.JMo_Swing_ToggleButton;
import org.jaymo_lang.lib.swing.control.scroll.JMo_Swing_List;
import org.jaymo_lang.lib.swing.control.scroll.JMo_Swing_TextField;
import org.jaymo_lang.lib.swing.control.scroll.composit.JMo_Swing_ComboBox;
import org.jaymo_lang.lib.swing.control.scroll.composit.JMo_Swing_Spinner;
import org.jaymo_lang.lib.swing.control.scroll.composit.JMo_Swing_Table;
import org.jaymo_lang.lib.swing.control.scroll.composit.canvas.JMo_Swing_Image;
import org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco.JMo_Swing_Dialog;
import org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco.JMo_Swing_Frame;
import org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco.JMo_Swing_Main;
import org.jaymo_lang.lib.swing.dialog.JMo_Swing_DialogMessage;
import org.jaymo_lang.lib.swing.dialog.JMo_Swing_DialogYesNo;
import org.jaymo_lang.lib.swing.layout.JMo_Swing_LayoutBorder;
import org.jaymo_lang.lib.swing.layout.JMo_Swing_LayoutFlex;
import org.jaymo_lang.lib.swing.layout.JMo_Swing_LayoutGrid;
import org.jaymo_lang.lib.swing.layout.JMo_Swing_LayoutPanel;
import org.jaymo_lang.lib.swing.layout.JMo_Swing_LayoutRow;
import org.jaymo_lang.lib.tcp.JMo_TcpClient;
import org.jaymo_lang.lib.tcp.JMo_TcpConnection;
import org.jaymo_lang.lib.tcp.JMo_TcpServer;
import org.jaymo_lang.lib.type.JMo_Uri;
import org.jaymo_lang.lib.udp.JMo_UdpPush;
import org.jaymo_lang.lib.udp.JMo_UdpService;
import org.jaymo_lang.lib.wiki77.JMo_Wiki77;
import org.jaymo_lang.lib.xml.JMo_Xml;
import org.jaymo_lang.lib.xml.JMo_XmlDocument;
import org.jaymo_lang.lib.xml.JMo_XmlNode;
import org.jaymo_lang.lib.zip.JMo_ZLib;
import org.jaymo_lang.object.I_Object;


/**
 * @author Michael Nitsche
 * @created 20.06.2022
 */
public class Types implements TypeDictionary {

	public String[] allTypes() {
		return new String[]{ "Base32", "Base32Hex", "Base64", "BrickLinkAPI", "CipherBlowfish", "CipherDES", "CipherRC2", "CipherRC4", "CipherRijndael", "CipherTripleDES", "Color", "ColorHSL",
			"ColorHSV", "ColorRGB", "Css", "Csv", "Emoji", "Fuzzy", "HashMD5", "HashSHA", "HttpClient", "Image", "Jar", "Json", "JsonObject", "MIniConf", "MIniConfResult", "NtpClient",
			"PropertiesFile", "RelationSet", "SntpClient", "Swing_Button", "Swing_ComboBox", "Swing_Dialog", "Swing_DialogMessage", "Swing_DialogYesNo", "Swing_Image", "Swing_Label",
			"Swing_LayoutBorder", "Swing_LayoutFlex", "Swing_LayoutGrid", "Swing_LayoutPanel", "Swing_LayoutRow", "Swing_List", "Swing_Main", "Swing_ProgressBar", "Swing_Scale", "Swing_Slider",
			"Swing_Spinner", "Swing_TabbedPane", "Swing_Table", "Swing_Text", "Swing_TextArea", "Swing_ToggleButton", "Swing_Window", "TcpClient", "TcpConnection", "TcpServer", "UdpPush",
			"UdpService", "Uri", "Wiki77", "Xml", "XmlDocument", "XmlNode", "ZLib" };
	}

	public Class<? extends I_Object> lookup( final String typename ) {

		switch( typename ) {
			case "Base32":
				return JMo_Base32.class;
			case "Base32Hex":
				return JMo_Base32Hex.class;
			case "Base64":
				return JMo_Base64.class;
			case "BrickLinkAPI":
				return JMo_BrickLinkAPI.class;
			case "CipherBlowfish":
				return JMo_CipherBlowfish.class;
			case "CipherDES":
				return JMo_CipherDES.class;
			case "CipherRC2":
				return JMo_CipherRC2.class;
			case "CipherRC4":
				return JMo_CipherRC4.class;
			case "CipherRijndael":
				return JMo_CipherRijndael.class;
			case "CipherRot13":
				return JMo_CipherRot13.class;
			case "CipherRot18":
				return JMo_CipherRot18.class;
			case "CipherRot26":
				return JMo_CipherRot26.class;
			case "CipherRot47":
				return JMo_CipherRot47.class;
			case "CipherRot5":
				return JMo_CipherRot5.class;
			case "CipherTripleDES":
				return JMo_CipherTripleDES.class;
			case "ColorHSL":
				return JMo_ColorHSL.class;
			case "ColorHSV":
				return JMo_ColorHSV.class;
			case "Color":
				return JMo_Color.class;
			case "Css":
				return JMo_Css.class;
			case "Csv":
				return JMo_Csv.class;
			case "Emoji":
				return JMo_Emoji.class;
			case "Font":
				return JMo_Font.class;
			case "Fuzzy":
				return JMo_Fuzzy.class;
			case "HashMD5":
				return JMo_HashMD5.class;
			case "HashSHA":
				return JMo_HashSHA.class;
			case "HttpClient":
				return JMo_HttpClient.class;
			case "Image":
				return JMo_Image.class;
			case "Jar":
				return JMo_Jar.class;
			case "Json":
				return JMo_Json.class;
			case "JsonObject":
				return JMo_JsonObject.class;
			case "MIniConf":
				return JMo_MIniConf.class;
			case "MIniConfResult":
				return JMo_MIniConfResult.class;
			case "NtpClient":
				return JMo_NtpClient.class;
			case "OllamaAPI":
				return JMo_OllamaAPI.class;
			case "PropertiesFile":
				return JMo_PropertiesFile.class;
			case "RelationSet":
				return JMo_RelationSet.class;
			case "SntpClient":
				return JMo_SntpClient.class;
			case "Swing_Button":
				return JMo_Swing_Button.class;
			case "Swing_ComboBox":
				return JMo_Swing_ComboBox.class;
			case "Swing_Dialog":
				return JMo_Swing_Dialog.class;
			case "Swing_DialogMessage":
				return JMo_Swing_DialogMessage.class;
			case "Swing_DialogYesNo":
				return JMo_Swing_DialogYesNo.class;
			case "Swing_Image":
				return JMo_Swing_Image.class;
			case "Swing_Label":
				return JMo_Swing_Label.class;
			case "Swing_LayoutBorder":
				return JMo_Swing_LayoutBorder.class;
			case "Swing_LayoutFlex":
				return JMo_Swing_LayoutFlex.class;
			case "Swing_LayoutGrid":
				return JMo_Swing_LayoutGrid.class;
			case "Swing_LayoutPanel":
				return JMo_Swing_LayoutPanel.class;
			case "Swing_LayoutRow":
				return JMo_Swing_LayoutRow.class;
			case "Swing_List":
				return JMo_Swing_List.class;
			case "Swing_Main":
				return JMo_Swing_Main.class;
			case "Swing_ProgressBar":
				return JMo_Swing_ProgressBar.class;
			case "Swing_Scale":
				return JMo_Swing_Scale.class;
			case "Swing_Slider":
				return JMo_Swing_Slider.class;
			case "Swing_Spinner":
				return JMo_Swing_Spinner.class;
			case "Swing_TabbedPane":
				return JMo_Swing_TabbedPane.class;
			case "Swing_Table":
				return JMo_Swing_Table.class;
			case "Swing_TextField":
				return JMo_Swing_TextField.class;
			case "Swing_TextArea":
				return JMo_Swing_TextArea.class;
			case "Swing_ToggleButton":
				return JMo_Swing_ToggleButton.class;
			case "Swing_Window":
				return JMo_Swing_Frame.class;
			case "TcpClient":
				return JMo_TcpClient.class;
			case "TcpConnection":
				return JMo_TcpConnection.class;
			case "TcpServer":
				return JMo_TcpServer.class;
			case "UdpPush":
				return JMo_UdpPush.class;
			case "UdpService":
				return JMo_UdpService.class;
			case "Uri":
				return JMo_Uri.class;
			case "Wiki77":
				return JMo_Wiki77.class;
			case "Xml":
				return JMo_Xml.class;
			case "XmlDocument":
				return JMo_XmlDocument.class;
			case "XmlNode":
				return JMo_XmlNode.class;
			case "ZLib":
				return JMo_ZLib.class;
		}

		return null;
	}

	public String helpFile( HELP_FILE hf ) {
		return switch(hf) {
			case NEW -> "/org/jaymo_lang/lib/constructors.txt";
			case EXTENDS -> "/org/jaymo_lang/lib/extends.txt";
			case FUNCTIONS -> "/org/jaymo_lang/lib/functions.txt";
			case TYPES -> "/org/jaymo_lang/lib/types.txt";
		};
	}

}
