/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt;

import de.mn77.lib.crypt.CodecBase32;


/**
 * @author Michael Nitsche
 * @created 30.08.2021
 */
public class JMo_Base32 extends A_BaseCodec {

	@Override
	protected byte[] pDecode( final byte[] ba ) {
		final String s = new String( ba );
		return CodecBase32.decodeToBytes( s );
	}

	@Override
	protected byte[] pDecode( final String s ) {
		return CodecBase32.decodeToBytes( s );
	}

	@Override
	protected String pEncode( final byte[] ba ) {
		return CodecBase32.encode( ba );
	}

	@Override
	protected String pEncode( final String s ) {
		return CodecBase32.encode( s );
	}

}
