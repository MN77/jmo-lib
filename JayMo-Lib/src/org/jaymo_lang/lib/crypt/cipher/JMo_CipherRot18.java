/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt.cipher;

import de.mn77.lib.crypt.cipher.Cipher_Rot18;
import de.mn77.lib.crypt.cipher.I_RotStringCipher;


/**
 * @author Michael Nitsche
 * @created 04.12.2023
 */
public class JMo_CipherRot18 extends A_RotCipher {

	private final Cipher_Rot18 cipher = new Cipher_Rot18();

	@Override
	protected I_RotStringCipher getCipher() {
		return this.cipher;
	}

}
