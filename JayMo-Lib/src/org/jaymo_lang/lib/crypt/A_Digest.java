/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 */
public abstract class A_Digest extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "add":
				this.mAdd( cr );
				return this;
			case "compute":
				return this.mCompute( cr );
		}
		return null;
	}

	protected abstract byte[] compute();

	protected abstract void pAdd( CallRuntime cr, I_Object obj );

	/**
	 * °add(Object o...)Same # Add more data.
	 */
	private void mAdd( final CallRuntime cr ) {
		final I_Object[] args = cr.argsVar( this, 1, I_Object.class );
		for( final I_Object arg : args )
			this.pAdd( cr, arg );
	}

	/**
	 * #°get ^ calculate
	 * #°calc ^ calculate
	 * °compute()ByteArray # Compute the hash value.
	 * °compute(Object o...)ByteArray # Add the given objects and compute the hash value.
	 */
	private I_Object mCompute( final CallRuntime cr ) {
		final I_Object[] args = cr.argsVar( this, 0, I_Object.class );

		for( final I_Object arg : args )
			this.pAdd( cr, arg );

		final byte[] ba = this.compute();
		return new JMo_ByteArray( ba );
	}

}
