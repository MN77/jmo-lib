/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.wiki77;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.lib.wiki77.Wiki77_Parser;


/**
 * @author Michael Nitsche
 * @created 2020-10-09
 */
public class JMo_Wiki77 extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "parse":
			case "toHtml":
			case "toHTML":
				return this.parse( cr );

			default:
				return null;
		}
	}

	/**
	 * °parse ^ toHTML
	 * °toHtml ^ toHTML
	 * °toHTML(Str wiki)Str # Parse a wiki encoded text and convert it to HTML.
	 */
	private I_Object parse( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Str.class )[0];
		final String s = Lib_Convert.toStr( cr, arg ).rawString();
		final Wiki77_Parser wiki = new Wiki77_Parser();
		return new JMo_Str( wiki.parse( s ) );
	}

}
