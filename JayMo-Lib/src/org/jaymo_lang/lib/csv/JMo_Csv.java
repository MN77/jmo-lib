/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.csv;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.Err_Runtime;
import de.mn77.lib.csv.CsvParser;
import de.mn77.lib.csv.CsvTableFile;


public class JMo_Csv extends A_ObjectSimple {

	private enum READTYPE {
		COMPLETE,
		HEAD,
		BODY
	}


	private final Call call_delimiter_field, call_delimiter_value, call_delimiter_line;
	private char       delimiterField  = '"';
	private char       delimiterColumn = ','; // [;:\t ]


	private String delimiterLine = "\n";


	/**
	 * ! CSV-Object to read, handle and write a CSV-File
	 * + Csv()
	 */
	public JMo_Csv() {
		this( null, null, null );
	}

	/**
	 * + Csv(Char valueDelimiter)
	 */
	public JMo_Csv( final Call delimiter_value ) {
		this( null, delimiter_value, null );
	}

	/**
	 * + Csv(Char fieldDelimiter, Char valueDelimiter, Char lineDelimiter)
	 */
	public JMo_Csv( final Call field_delimiter, final Call value_delimiter, final Call line_delimiter ) {
		this.call_delimiter_field = field_delimiter;
		this.call_delimiter_value = value_delimiter;
		this.call_delimiter_line = line_delimiter;
	}

	@Override
	public void init( final CallRuntime cr ) {
		if( this.call_delimiter_field != null )
			this.delimiterField = Lib_Convert.toChar( cr, cr.execInit( this.call_delimiter_field, this ) ).rawChar(cr);
		if( this.call_delimiter_value != null )
			this.delimiterColumn = Lib_Convert.toChar( cr, cr.execInit( this.call_delimiter_value, this ) ).rawChar(cr);
		if( this.call_delimiter_line != null )
			this.delimiterLine = Lib_Convert.toStr( cr, cr.execInit( this.call_delimiter_line, this ) ).rawString();
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "readHead":
				return this.mRead( cr, READTYPE.HEAD );

			case "readBody":
			case "readData":
				return this.mRead( cr, READTYPE.BODY );

			case "read":
				return this.mRead( cr, READTYPE.COMPLETE );

			case "parseHead":
				return this.mParse( cr, READTYPE.HEAD );

			case "parseBody":
			case "parseData":
				return this.mParse( cr, READTYPE.BODY );

			case "parse":
				return this.mParse( cr, READTYPE.COMPLETE );

			case "write":
				return this.mWrite( cr, false, false );
			case "writeFull":
				return this.mWrite( cr, true, false );
			case "append":
				return this.mWrite( cr, false, true );

			default:
				return null;
		}
	}

	/**
	 * °parseHead(Str s)List # Parse the first line of an csv-string.
	 * °parseBody ^ parseData
	 * °parseData(Str s)Table # Parse all lines without the first line of an csv-string.
	 * °parse(Str s)Table # Parse the complete csv-string.
	 */
	private I_Object mParse( final CallRuntime cr, final READTYPE rt ) {
		final I_Object arg = cr.args( this, JMo_Str.class )[0];
		final String csv = Lib_Convert.toStr( cr, arg ).rawString();

		try {
			final CsvParser parser = new CsvParser( this.delimiterField, this.delimiterColumn, this.delimiterLine );

			switch( rt ) {
				case COMPLETE:
					final I_Table<String> tab1 = parser.parse( csv, true, true );
					return Lib_Convert.toJMo( tab1 );
				case HEAD:
					final I_Table<String> tab2 = parser.parse( csv, true, false );
					return Lib_Convert.toJMo( tab2.getRow( 0 ) ); // TODO Fehlerprüfung
				case BODY:
					final I_Table<String> tab3 = parser.parse( csv, false, true );
					return Lib_Convert.toJMo( tab3 );
				default:
					throw Err.impossible();
			}
		}
		catch( final Err_Runtime r ) {
			throw new RuntimeError( cr, "CSV-Table-Error", r.getLocalizedMessage() );
		}
	}

	/**
	 * °readHead(Str|File file)List # Read the first line of an csv-file.
	 * °readBody ^ readData
	 * °readData(Str|File file)Table # Read all lines without the first line of an csv-file.
	 * °read(Str|File file)Table # Read the complete csv-file.
	 */
	private I_Object mRead( final CallRuntime cr, final READTYPE rt ) {
		final I_Object arg = cr.argExt( this, JMo_Str.class, JMo_File.class );
		final String file = arg instanceof JMo_File ? ((JMo_File)arg).getInternalFile().getAbsolutePath() : Lib_Convert.toStr( cr, arg ).rawString();

		final CsvTableFile csv = new CsvTableFile( file );
		csv.setDelimiters( this.delimiterField, this.delimiterColumn, this.delimiterLine );

		try {

			switch( rt ) {
				case COMPLETE:
					return Lib_Convert.toJMo( csv.read() );
				case HEAD:
					return Lib_Convert.toJMo( csv.readFirstLine() );
				case BODY:
					return Lib_Convert.toJMo( csv.readExceptFirstLine() );
				default:
					throw Err.impossible();
			}
		}
		catch( final Err_Runtime r ) {
			throw new RuntimeError( cr, "CSV-Table-Error", r.getMessage() );
		}
		catch( final Err_FileSys f ) {
			throw new ExternalError( cr, "File-Access-Error", f.getMessage() );
		}
	}

	/**
	 * °write(Str|File file, Table data)Same # Write a table of data to a file
	 * °writeFull(Str|File file, Table data)Same # Write a table with title header to a file
	 * °append(Str|File file, Table data)Same # Appends a table to a file.
	 */
	private I_Object mWrite( final CallRuntime cr, final boolean writeTitles, final boolean append ) {
		//TODO Str,File
		final I_Object[] args = cr.argsExt( this, new Class<?>[]{ JMo_Str.class, JMo_File.class }, new Class[]{ JMo_Table.class } );
		final String file = args[0] instanceof JMo_File ? ((JMo_File)args[0]).getInternalFile().getAbsolutePath() : Lib_Convert.toStr( cr, args[0] ).rawString();

		final CsvTableFile csvFile = new CsvTableFile( file );
		csvFile.setDelimiters( this.delimiterField, this.delimiterColumn, this.delimiterLine );

		try {
//			cr.parType(oa[1], JMo_Table.class);
			final JMo_Table table = (JMo_Table)args[1];
			final I_Table<String> ts = Lib_Convert.toTableString( table );

			// Insert titles
			if( writeTitles && !append ) {
				final I_Object[] jmoTitles = table.getTitles();

				if( jmoTitles != null ) {
					final String[] titles = Lib_Convert.toStringArray( jmoTitles );
					ts.add( 0, titles );
				}
			}

			if( append )
				csvFile.append( ts );
			else
				csvFile.write( ts );
		}
		catch( final Err_FileSys f ) {
			throw new ExternalError( cr, "File-Access-Error", f.getLocalizedMessage() );
		}
		return this;
	}

}
