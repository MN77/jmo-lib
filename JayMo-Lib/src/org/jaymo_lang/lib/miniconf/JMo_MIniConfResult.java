/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.miniconf;

import java.util.Map.Entry;
import java.util.Set;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.miniconf.result.MIniConfObject;
import de.mn77.miniconf.result.MIniConfResult;


/**
 * @author Michael Nitsche
 * @created 11.02.2022
 */
public class JMo_MIniConfResult extends A_ObjectSimple {

	private final MIniConfResult result;


	public JMo_MIniConfResult( final MIniConfResult result ) {
		this.result = result;
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "bool":
			case "asBool":
				return this.mAsBool( cr );
//			case "asInteger":
			case "int":
			case "asInt":
				return this.mAsInt( cr );
//			case "asDecimal":
			case "dec":
			case "asDec":
				return this.mAsDec( cr );
			case "double":
			case "asDouble":
				return this.mAsDouble( cr );
//			case "asString":
			case "str":
			case "asStr":
				return this.mAsStr( cr );
			case "list":
			case "asList":
				return this.mAsList( cr );
			case "object":
			case "asObject":
				return this.mAsObject( cr );

			case "keys":
				return this.mKeys( cr );
			case "toMap":
				return this.mToMap( cr );
		}
		return null;
	}

	/**
	 * °bool ^ asBool
	 * °asBool(Str keypath)Bool? # Returns the matching entry for keypath. If no key available, nil will be returned.
	 * °asBool(Str keypath, Bool default)Bool # Returns the matching entry for keypath. If no key available, default will be returned.
	 */
	private I_Object mAsBool( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final I_Object arg0 = cr.argType( args[0], JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, arg0 ).rawString();

		if( args.length == 1 ) {
			final Boolean b = this.result.getBoolean( key );
			return b == null
				? Nil.NIL
				: JMo_Bool.getObject( b );
		}
		else {
			final I_Object arg1 = cr.argType( args[1], JMo_Bool.class );
			final boolean def = Lib_Convert.toBoolean( cr, arg1 );
			return JMo_Bool.getObject( this.result.getBoolean( key, def ) );
		}
	}

	/**
	 * °dec ^ asDec
	 * °asDec(Str keypath)Dec? # Returns the matching entry for keypath. If no key available, nil will be returned.
	 * °asDec(Str keypath, Dec default)Dec # Returns the matching entry for keypath. If no key available, default will be returned.
	 */
	private I_Object mAsDec( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final I_Object arg0 = cr.argType( args[0], JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, arg0 ).rawString();

		if( args.length == 1 ) {
			final Double d = this.result.getDouble( key );
			return d == null
				? Nil.NIL
				: JMo_Dec.valueOf( cr, d );
		}
		else {
			final I_Object arg1 = cr.argType( args[1], JMo_Dec.class );
			final double def = Lib_Convert.toDouble( cr, arg1 );
			return JMo_Dec.valueOf( cr, this.result.getDouble( key, def ) );
		}
	}

	/**
	 * °double ^ asDouble
	 * °asDouble(Str keypath)Double? # Returns the matching entry for keypath. If no key available, nil will be returned.
	 * °asDouble(Str keypath, Double default)Double # Returns the matching entry for keypath. If no key available, default will be returned.
	 */
	private I_Object mAsDouble( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final I_Object arg0 = cr.argType( args[0], JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, arg0 ).rawString();

		if( args.length == 1 ) {
			final Double d = this.result.getDouble( key );
			return d == null
				? Nil.NIL
				: new JMo_Double( d );
		}
		else {
			final I_Object arg1 = cr.argType( args[1], JMo_Double.class );
			final double def = Lib_Convert.toDouble( cr, arg1 );
			return new JMo_Double( this.result.getDouble( key, def ) );
		}
	}

	/**
	 * °int ^ asInt
	 * °asInt(Str keypath)Int? # Returns the matching entry for keypath. If no key available, nil will be returned.
	 * °asInt(Str keypath, Int default)Int # Returns the matching entry for keypath. If no key available, default will be returned.
	 */
	private I_Object mAsInt( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final I_Object arg0 = cr.argType( args[0], JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, arg0 ).rawString();

		if( args.length == 1 ) {
			final Integer i = this.result.getInteger( key );
			return i == null
				? Nil.NIL
				: new JMo_Int( i );
		}
		else {
			final I_Object arg1 = cr.argType( args[1], JMo_Int.class );
			final int def = Lib_Convert.toInt( cr, arg1 );
			return new JMo_Int( this.result.getInteger( key, def ) );
		}
	}

	/**
	 * °list ^ asList
	 * °asList(Str keypath)List? # Returns the matching entry for keypath. If no key available, nil will be returned.
	 * °asList(Str keypath, List default)List # Returns the matching entry for keypath. If no key available, default will be returned.
	 */
	private I_Object mAsList( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final I_Object arg0 = cr.argType( args[0], JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, arg0 ).rawString();

		final String[] sa = this.result.getList( key );

		if( sa == null )
			return args.length == 1
				? Nil.NIL
				: cr.argType( args[1], JMo_List.class );
		else {
			final SimpleList<I_Object> al = new SimpleList<>( sa.length );

			for( final String s : sa )
				al.add( new JMo_Str( s ) );
			return new JMo_List( al );
		}
	}

	/**
	 * °object ^ asObject
	 * °asObject(Str keypath)List? # Returns the matching entry for keypath. If no key available, nil will be returned.
	 * °asObject(Str keypath, Str defaultType, List defaultArgs)List # Returns the matching entry for keypath. If no key available, default will be returned.
	 */
	private I_Object mAsObject( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		Lib_Error.ifIs( cr, 2, args.length, "amount of arguments" );

		final I_Object arg0 = cr.argType( args[0], JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, arg0 ).rawString();

		final MIniConfObject mo = this.result.getObject( key );

		if( mo == null ) {

			if( args.length == 1 )
				return Nil.NIL;
			else { // args.len == 3
				final I_Object arg1 = cr.argType( args[1], JMo_Str.class );
				final JMo_List arg2 = cr.argType( args[1], JMo_List.class );

				final String arg1s = Lib_Convert.toStr( cr, arg1 ).rawString();
				final SimpleList<I_Object> al = new SimpleList<>();
				al.add( new JMo_Str( arg1s ) );
				al.add( arg2 );
				return new JMo_List( al );
			}
		}
		else {
			final SimpleList<I_Object> al1 = new SimpleList<>();
			al1.add( new JMo_Str( mo.type ) );

			final SimpleList<I_Object> al2 = new SimpleList<>( mo.args.length );
			for( final String s : mo.args )
				al2.add( new JMo_Str( s ) );

			al1.add( new JMo_List( al2 ) );
			return new JMo_List( al1 );
		}
	}

	/**
	 * °str ^ asStr
	 * °asStr(Str keypath)Str? # Returns the matching entry for keypath. If no key available, nil will be returned.
	 * °asStr(Str keypath, Str default)Str # Returns the matching entry for keypath. If no key available, default will be returned.
	 */
	private I_Object mAsStr( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final I_Object arg0 = cr.argType( args[0], JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, arg0 ).rawString();

		if( args.length == 1 ) {
			final String s = this.result.getString( key );
			return s == null
				? Nil.NIL
				: new JMo_Str( s );
		}
		else {
			final I_Object arg1 = cr.argType( args[1], JMo_Str.class );
			final String def = Lib_Convert.toStr( cr, arg1 ).rawString();
			return new JMo_Str( this.result.getString( key, def ) );
		}
	}

	/**
	 * °keys()List # Returns a list with all keys.
	 */
	private JMo_List mKeys( final CallRuntime cr ) {
		cr.argsNone();
		final Set<String> keys = this.result.getKeys();
		final SimpleList<I_Object> al = new SimpleList<>( keys.size() );
		for( final String s : keys )
			al.add( new JMo_Str( s ) );
		return new JMo_List( al );
	}

	/**
	 * °toMap()Map # Convert this result to a Map with Strings.
	 */
	private I_Object mToMap( final CallRuntime cr ) {
		cr.argsNone();
		final int size = this.result.getItems().size();
		final SimpleList<I_Object> keys = new SimpleList<>( size );
		final SimpleList<I_Object> objects = new SimpleList<>( size );

		for( final Entry<String, String> e : this.result.getItems() ) {
			keys.add( new JMo_Str( e.getKey() ) );
			objects.add( new JMo_Str( e.getValue() ) );
		}

		return new JMo_Map( keys, objects );
	}

}
