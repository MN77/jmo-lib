/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.xml;

import java.util.ArrayList;
import java.util.Collection;

import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 */
public class Util_XmlTextTable {

	private Util_XmlTextTable() {}

	
	public static JMo_Table createTextTable( final CallRuntime cr, final Node doc, final JMo_List rows, final JMo_List cols ) {
		if( doc == null )
			throw new JayMoError( cr, "Nothing to read", "Got empty document!" );

		final Collection<String> rowsAL = new ArrayList<>();
		for( final I_Object o : rows.getInternalCollection() )
			rowsAL.add( Lib_Convert.toStr( cr, o ).rawString() );

		final Collection<String[]> colsAL = new ArrayList<>();
		for( final I_Object o : cols.getInternalCollection() )
			if( o instanceof JMo_List ) {
				final Collection<String> c2 = new ArrayList<>();
				for( final I_Object c2o : ((JMo_List)o).getInternalCollection() )
					c2.add( Lib_Convert.toStr( cr, c2o ).rawString() );
				colsAL.add( c2.toArray( new String[c2.size()] ) );
			}
			else if( o instanceof JMo_Str )
				colsAL.add( new String[]{ Lib_Convert.toStr( cr, o ).rawString() } );
			else
				throw new RuntimeError( cr, "Invalid row type", "" + o ); //TODO Hack


		final ArrayTable<I_Object> result = new ArrayTable<>( cols.getInternalCollection().size() );
		Util_XmlTextTable.iTextTableRows( result, doc.getChildNodes(), rowsAL.toArray( new String[rowsAL.size()] ), 0, colsAL.toArray( new String[colsAL.size()][] ) );
		return new JMo_Table( result );
	}

	private static void iTextTableCol( final I_Object[] rowData, final Node node, final int columnIndex, final String[] column, final int columnDepth ) {
		if( !node.getNodeName().equals( column[columnDepth] ) )
			return;

		if( column.length == columnDepth + 1 )
			rowData[columnIndex] = new JMo_Str( node.getTextContent() );
		else if( node.hasChildNodes() ) {
			final NodeList childs = node.getChildNodes();
			for( int ci = 0; ci < childs.getLength(); ci++ )
				Util_XmlTextTable.iTextTableCol( rowData, childs.item( ci ), columnIndex, column, columnDepth + 1 );
		}
	}

	private static void iTextTableCols( final I_Object[] rowData, final NodeList nodes, final String[][] columns ) {

		for( int idx = 0; idx < nodes.getLength(); idx++ ) {
			final Node node = nodes.item( idx );
			final String nodeName = node.getNodeName();
			if( nodeName.equals( "#text" ) )
				continue;

			for( int cidx = 0; cidx < columns.length; cidx++ )
				Util_XmlTextTable.iTextTableCol( rowData, node, cidx, columns[cidx], 0 );
		}
	}

	private static void iTextTableRows( final I_Table<I_Object> tab, final NodeList nodes, final String[] row, final int rowOffset, final String[][] columns ) {

		for( int idx = 0; idx < nodes.getLength(); idx++ ) {
			final Node node = nodes.item( idx );
			if( node.getNodeName().equals( row[rowOffset] ) )
				if( rowOffset == row.length - 1 ) {
					final I_Object[] rowData = new I_Object[columns.length];
					Util_XmlTextTable.iTextTableCols( rowData, node.getChildNodes(), columns );

					// Fill empty cells
					for( int rowIdx = 0; rowIdx < rowData.length; rowIdx++ )
						if( rowData[rowIdx] == null )
							rowData[rowIdx] = Nil.NIL;

					tab.add( rowData );
				}
				else
					Util_XmlTextTable.iTextTableRows( tab, node.getChildNodes(), row, rowOffset + 1, columns );
		}
	}

}
