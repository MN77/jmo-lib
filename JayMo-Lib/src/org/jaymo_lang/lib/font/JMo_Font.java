/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.font;

import java.awt.Font;
import java.awt.GraphicsEnvironment;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Java;
import org.jaymo_lang.util.Lib_Type;


/**
 * @author Michael Nitsche
 * @created 26.10.2023
 */
public class JMo_Font extends A_ObjectSimple {

	private final ArgCallBuffer par_name, par_size;
	private String  name   = Font.SANS_SERIF;
	private int     size   = 16;
	private boolean bold   = false;
	private boolean italic = false;


	public JMo_Font() {
		this.par_name = null;
		this.par_size = null;
	}

	/**
	 * + Font()
	 * + Font(Str name, IntNumber size)
	 */
	public JMo_Font( final Call fontname, final Call size ) {
		this.par_name = new ArgCallBuffer( 0, fontname );
		this.par_size = new ArgCallBuffer( 1, size );
	}


	public String getName() {
		return this.name;
	}

	public int getSize() {
		return this.size;
	}

	@Override
	public void init( final CallRuntime cr ) {
		if( this.par_name != null )
			this.name = Lib_Convert.toStr( cr, this.par_name.init( cr, this, JMo_Str.class ) ).rawString();
		if( this.par_size != null )
			this.size = Lib_Convert.toInt( cr, this.par_size.init( cr, this, I_IntNumber.class ) );
	}

	public boolean isBold() {
		return this.bold;
	}

	public boolean isItalic() {
		return this.italic;
	}

	public Font toAwtFont() {
		int style = Font.PLAIN;
		if( this.bold )
			style = style | Font.BOLD;
		if( this.italic )
			style = style | Font.ITALIC;
		return new Font( this.name, style, this.size );
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		final StringBuilder sb = new StringBuilder();

		switch( type ) {
			case IDENT:
				return Lib_Type.getName( this );
			case DESCRIBE:
				sb.append( Lib_Type.getName( this.getClass(), this ) );
				sb.append( "(" );
				sb.append( this.name );
				sb.append( ',' );
				sb.append( this.size );
				if( this.bold )
					sb.append( ",bold" );
				if( this.italic )
					sb.append( ",italic" );
				sb.append( ")" );
				return sb.toString();
			default:
				sb.append( this.name );
				sb.append( ':' );
				sb.append( this.size );
				if( this.bold )
					sb.append( ":bold" );
				if( this.italic )
					sb.append( ":italic" );
				return sb.toString();
		}
	}

	/**
	 * °plain ^ regular
	 * °regular()Same # Reset this font to regular style.
	 * °bold()Same # Make this font bold.
	 * °italic()Same # Make this font italic.
	 * °allFonts()List # Returns a list with all available fonts
	 */
	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
//			case "availableFonts":
//			case "fonts":
			case "allFonts":
				return this.mAllFonts( cr );

			case "regular":
			case "plain":
				cr.argsNone();
				this.bold = false;
				this.italic = false;
				return this;
			case "bold":
				cr.argsNone();
				this.bold = true;
				return this;
			case "italic":
				cr.argsNone();
				this.italic = true;
				return this;

			default:
				return null;
		}
	}

	/**
	 * °allFonts()List # Returns a List with all available fonts.
	 */
	private JMo_List mAllFonts( final CallRuntime cr ) {
		cr.argsNone();
		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final String[] fontList = ge.getAvailableFontFamilyNames();
		return (JMo_List)Lib_Java.javaToJmo( fontList );
	}

}
