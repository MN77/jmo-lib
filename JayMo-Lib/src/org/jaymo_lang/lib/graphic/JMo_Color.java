/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.graphic;

import java.awt.Color;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.numsys.NumSys_Hex;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.03.2020
 * @implNote Color is the short version of ColorRGB because, RGB is the default and mostly used.
 */
public class JMo_Color extends A_ColorModel {

	private final ArgCallBuffer par1, par2, par3;
	private Color               color = null;


	/**
	 * + ColorRGB()
	 */
	public JMo_Color() {
		this( 0, 0, 0 );
	}

	/**
	 * + ColorRGB(Str htmlColor)
	 */
	public JMo_Color( final Call p1 ) {
		this.par1 = new ArgCallBuffer( 0, p1 );
		this.par2 = null;
		this.par3 = null;
	}

	/**
	 * ! Object of a RGB-Color (Red,Green,Blue), each between 0 and 255.
	 * + ColorRGB(IntNumber red, IntNumber green, IntNumber blue)
	 */
	public JMo_Color( final Call p1, final Call p2, final Call p3 ) {
		this.par1 = new ArgCallBuffer( 0, p1 );
		this.par2 = new ArgCallBuffer( 1, p2 );
		this.par3 = new ArgCallBuffer( 2, p3 );
	}

	public JMo_Color( final Color c ) {
		this.par1 = null;
		this.par2 = null;
		this.par3 = null;
		this.color = c;
	}

	public JMo_Color( final int r, final int g, final int b ) {
		this.par1 = null;
		this.par2 = null;
		this.par3 = null;
		this.color = new Color( r, g, b );
	}


	@Override
	public Color getColor() {
		return this.color;
	}

	@Override
	public void init( final CallRuntime cr ) {
		if( this.color != null )
			return; // already set

		if( this.par1 != null && this.par2 != null && this.par3 != null ) {
			final I_IntNumber po1 = this.par1.init( cr, this, I_IntNumber.class );
			final I_IntNumber po2 = this.par2.init( cr, this, I_IntNumber.class );
			final I_IntNumber po3 = this.par3.init( cr, this, I_IntNumber.class );
			final int pi1 = Lib_Convert.toInt( cr, po1 );
			final int pi2 = Lib_Convert.toInt( cr, po2 );
			final int pi3 = Lib_Convert.toInt( cr, po3 );
			Lib_Error.ifNotBetween( cr, 0, 255, pi1, "Value for red" );
			Lib_Error.ifNotBetween( cr, 0, 255, pi2, "Value for green" );
			Lib_Error.ifNotBetween( cr, 0, 255, pi3, "Value for blue" );
			this.color = new Color( pi1, pi2, pi3 );
		}
		else if( this.par1 != null && this.par2 == null && this.par3 == null ) {
			final JMo_Str po1 = this.par1.init( cr, this, JMo_Str.class );
			final String ps = Lib_Convert.toStr( cr, po1 ).rawString();
			if( ps.length() != 7 && ps.length() != 4 || ps.charAt( 0 ) != '#' )
				throw new RuntimeError( cr, "Invalid html color", "Need something like '#fff' or '#000000', but got: " + ps );

			int r = 0;
			int g = 0;
			int b = 0;

			if( ps.length() == 7 ) {
				//			final int i = Hex.fromHex(this.value);
//				MOut.exit(ps, ps.substring(1, 3), ps.substring(3, 5), ps.substring(5,7));
				r = Integer.parseInt( ps.substring( 1, 3 ), 16 );
				g = Integer.parseInt( ps.substring( 3, 5 ), 16 );
				b = Integer.parseInt( ps.substring( 5, 7 ), 16 );
			}
			else {
				r = Integer.parseInt( ps.substring( 1, 2 ), 16 );
				r = r * 16 + r;
				g = Integer.parseInt( ps.substring( 2, 3 ), 16 );
				g = g * 16 + g;
				b = Integer.parseInt( ps.substring( 3, 4 ), 16 );
				b = b * 16 + b;
			}

			this.color = new Color( r, g, b );
		}
		else
			Err.invalid( this.par1, this.par2, this.par3 );
//		else
//			this.color = new Color(pi1, pi2, pi3);
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {

		switch( type ) {
			case NESTED:
				return Lib_Type.getName( this );
			case DESCRIBE:
				return Lib_Type.getName( this ) + "(" + this.color.getRed() + "i," + this.color.getGreen() + "i," + this.color.getBlue() + "i)";
			default:
				return Lib_Type.getName( this ) + "(" + this.color.getRed() + "," + this.color.getGreen() + "," + this.color.getBlue() + ")";
		}
	}

	/**
	 * °BLACK()ColorRGB # Color: Black
	 * °BLUE()ColorRGB # Color: Blue
	 * °CYAN()ColorRGB # Color: Cyan
	 * °DARK_GRAY()ColorRGB # Color: Dark gray
	 * °GRAY()ColorRGB # Color: Gray
	 * °GREEN()ColorRGB # Color: Green
	 * °LIGHT_GRAY()ColorRGB # Color: Light gray
	 * °MAGENTA()ColorRGB # Color: Magenta
	 * °PINK()ColorRGB # Color: Pink
	 * °ORANGE()ColorRGB # Color: Orange
	 * °RED()ColorRGB # Color: Red
	 * °WHITE()ColorRGB # Color: White
	 * °YELLOW()ColorRGB # Color: Yellow
	 */
	@Override
	public final A_Immutable getConstant( final CallRuntime cr, final String name ) {

		switch( name ) {
			case "BLACK":
				return new JMo_Color( Color.BLACK );
			case "BLUE":
				return new JMo_Color( Color.BLUE );
			case "CYAN":
				return new JMo_Color( Color.CYAN );
			case "DARK_GRAY":
				return new JMo_Color( Color.DARK_GRAY );
			case "GRAY":
				return new JMo_Color( Color.GRAY );
			case "GREEN":
				return new JMo_Color( Color.GREEN );
			case "LIGHT_GRAY":
				return new JMo_Color( Color.LIGHT_GRAY );
			case "MAGENTA":
				return new JMo_Color( Color.MAGENTA );
			case "PINK":
				return new JMo_Color( Color.PINK );
			case "ORANGE":
				return new JMo_Color( Color.ORANGE );
			case "RED":
				return new JMo_Color( Color.RED );
			case "WHITE":
				return new JMo_Color( Color.WHITE );
			case "YELLOW":
				return new JMo_Color( Color.YELLOW );
//			case "brown":
//				return new JMo_Color(Color.br);
//				return new JMo_Color(255,0,0);

			default:
				return null;
		}
	}

	@Override
	protected I_Object call3( final CallRuntime cr, final String method ) {

		switch( method ) {
			/**
			 * °red()Int # Get value of the red color
			 * °green()Int # Get value of the green color
			 * °blue()Int # Get value of the blue color
			 */
			case "red":
				cr.argsNone();
				return new JMo_Int( this.color.getRed() );
			case "green":
				cr.argsNone();
				return new JMo_Int( this.color.getGreen() );
			case "blue":
				cr.argsNone();
				return new JMo_Int( this.color.getBlue() );

//			case "html":
			case "toHtml":
				return this.mHtml( cr );

			case "diff":
				return this.mDiff( cr );

			default:
				return null;
		}
	}

	/**
	 * °diff(ColorRGB other)ColorRGB # Get the difference as new ColorRGB Object.
	 */
	private I_Object mDiff( CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Color.class )[0];

		final Color cArg = ((JMo_Color)arg).getColor();
		final int r = Util_Color.diffValue( this.color.getRed(), cArg.getRed() );
		final int g = Util_Color.diffValue( this.color.getGreen(), cArg.getGreen() );
		final int b = Util_Color.diffValue( this.color.getBlue(), cArg.getBlue() );

		final Color c = new Color( r, g, b );
		return new JMo_Color( c );
	}

	/**
	 * °toHtml()Str # Get the color as HTML-Encoding.
	 */
	private JMo_Str mHtml( final CallRuntime cr ) {
		cr.argsNone();

		StringBuilder sb = new StringBuilder();
		sb.append( "#" );
		sb.append( NumSys_Hex.toHex( this.color.getRed() ) );
		sb.append( NumSys_Hex.toHex( this.color.getGreen() ) );
		sb.append( NumSys_Hex.toHex( this.color.getBlue() ) );
		return new JMo_Str( sb.toString().toLowerCase() );
	}

}
