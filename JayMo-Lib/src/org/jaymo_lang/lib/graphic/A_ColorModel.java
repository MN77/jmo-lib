/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.graphic;

import java.awt.Color;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 06.03.2020
 */
public abstract class A_ColorModel extends A_Immutable {

	/**
	 * °toRgb ^ toRGB
	 * °toRGB()ColorRGB # Convert the current color to RGB-Colorspace.
	 * °toHsv ^ toHSV
	 * °toHSV()ColorHSV # Convert the current color to HSV-Colorspace.
	 * °toHsl ^ toHSL
	 * °toHSL()ColorHSL # Convert the current color to HSL-Colorspace.
	 */
	@Override
	public final I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "toRgb":
			case "toRGB":
				cr.argsNone();
				return new JMo_Color( this.getColor() );

			case "toHsv":
			case "toHSV":
				cr.argsNone();
				return new JMo_ColorHSV( this.getColor() );

			case "toHsl":
			case "toHSL":
				cr.argsNone();
				return new JMo_ColorHSL( this.getColor() );

			default:
				return this.call3( cr, method );
		}
	}

	@Override
	public boolean equals( final Object other ) {
		if( other instanceof A_ColorModel )
			return this.getColor().equals( ((A_ColorModel)other).getColor() );
		return false;
	}

	@Override
	public boolean equalsLazy( final Object other ) {
		return this.equals( other );
	}

	public abstract Color getColor();

	protected abstract I_Object call3( final CallRuntime cr, final String method );

}
