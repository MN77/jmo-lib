/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.graphic;

import java.awt.Color;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.I_Number;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.lib.graphic.IMAGE_FORMAT;
import de.mn77.lib.graphic.I_ImageX;
import de.mn77.lib.graphic.MImageX;


/**
 * @author Michael Nitsche
 * @created 06.03.2020
 */
public class JMo_Image extends A_ObjectSimple {

	private final ArgCallBuffer par1, par2;
	private I_ImageX            image = null;


	/**
	 * ! Object of an image
	 * + Image(Str file)
	 * + Image(File file)
	 * + Image(ByteArray data)
	 */
	public JMo_Image( final Call image ) {
		this.par1 = new ArgCallBuffer( 0, image );
		this.par2 = null;
	}

	/**
	 * + Image(IntNumber dx, IntNumber dy)
	 */
	public JMo_Image( final Call dx, final Call dy ) {
		this.par1 = new ArgCallBuffer( 0, dx );
		this.par2 = new ArgCallBuffer( 1, dy );
	}

	public JMo_Image( final I_ImageX init ) {
		this.image = init;
		this.par1 = null;
		this.par2 = null;
	}

	@Override
	public void init( final CallRuntime cr ) {

		if( this.par1 != null && this.par2 != null ) {
			final I_IntNumber odx = this.par1.init( cr, this, I_IntNumber.class );
			final I_IntNumber ody = this.par2.init( cr, this, I_IntNumber.class );
			final int dx = Lib_Convert.toInt( cr, odx );
			final int dy = Lib_Convert.toInt( cr, ody );
			this.image = new MImageX( dx, dy, 0, 0, 0, 0 );
			return;
		}

		if( this.par1 != null && this.par2 == null ) {
			final I_Object o1 = this.par1.initExt( cr, this, JMo_Str.class, JMo_File.class, JMo_ByteArray.class );
			Object o2 = null;

			if( o1 instanceof JMo_Str oStr )
				o2 = oStr.rawString();
			else if( o1 instanceof JMo_File oFile)
				o2 = oFile.getInternalFile();
			else if( o1 instanceof JMo_ByteArray oBA)
				o2 = oBA.getValue();
			else
				Err.impossible( o2 );

			try {
				Err.ifNull( o2 );
				this.image = new MImageX( o2 );
			}
			catch( final Exception e ) {
				Err.show( e );
				new ExternalError( cr, "Opening Image failed", e.getMessage() );
			}
		}
	}

	public I_ImageX internalGet() {
		return this.image;
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			/**
			 * °width()Int # Returns the width of this image
			 */
			case "width":
				cr.argsNone();
				return new JMo_Int( this.image.getWidth() );

			/**
			 * °height()Int # Returns the height of this image
			 */
			case "height":
				cr.argsNone();
				return new JMo_Int( this.image.getHeight() );

			/**
			 * °copy()Image # Create a copy of the image
			 */
			case "copy":
				cr.argsNone();
				return new JMo_Image( this.image.copy() );

			/**
			 * °clear(IntNumber r, IntNumber g, IntNumber b)Same # Clear the image to one color
			 */
			case "clear":
				cr.argsNone();
				this.iFillRGBA( 0, 0, 0, null );
				return this;

			case "fill":
				this.iFill( cr );
				return this;

				/**
				 * °writePNG ^ writePng
				 * °writePng(Str file)Same # Write a PNG-Image to the disk
				 */
				case "writePNG":
				case "writePng": final String dest = ((JMo_Str)cr.args( this, JMo_Str.class )[0]).rawString();
				try {
					this.image.save( dest, IMAGE_FORMAT.PNG, false );
				}
				catch( final Err_FileSys e ) {
					new ExternalError( cr, "Write-Error", e.getMessage() );
				}
				return this;

			/**
			 * °writeJPG ^ writeJpg
			 * °writeJpg(Str file)Same # Write a JPG-Image to the disk
			 * °writeJpg(Str file, IntNumber quality)Same # Write a JPG-Image to the disk, quality between 0 (worst) and 100 (best)
			 */
			case "writeJPG":
			case "writeJpg":
				final I_Object[] parsJpg = cr.argsFlex( this, 1, 2 );
				final String dest2 = cr.argType( parsJpg[0], JMo_Str.class ).rawString();

				try {

					if( parsJpg.length == 2 ) {
						final int quality = Lib_Convert.toInt( cr, cr.argType( parsJpg[1], I_IntNumber.class ) );
						this.image.saveJPEG( dest2, quality, false );
					}
					else
						this.image.saveJPEG( dest2, false );
				}
				catch( final Err_FileSys e ) {
					new ExternalError( cr, "Write-Error", e.getMessage() );
				}
				return this;

			/**
			 * °scale(IntNumber dx, IntNumber dy)Same # Scale this image to the new size. >0 = Max, 0 = ignored, <0 = Min. Max and X have higher priority.
			 */
			case "scale":
				final I_Object[] pars2 = cr.args( this, I_IntNumber.class, I_IntNumber.class );
				final int dx2 = Lib_Convert.toInt( cr, pars2[0] );
				final int dy2 = Lib_Convert.toInt( cr, pars2[1] );
				this.image.scale( dx2, dy2 );
				return this;

			/**
			 * °distort(IntNumber dx, IntNumber dy)Same # Distort this image
			 */
			case "distort":
				final I_Object[] pars3 = cr.args( this, I_IntNumber.class, I_IntNumber.class );
				final int dx3 = Lib_Convert.toInt( cr, pars3[0] );
				final int dy3 = Lib_Convert.toInt( cr, pars3[1] );
				this.image.distort( dx3, dy3 );
				return this;

			/**
			 * °cut(IntNumber dx, IntNumber dy, MagicPosition pos)Same # Cut out a part of the picture
			 * °cut(IntNumber dx, IntNumber dy, MagicPosition pos, IntNumber offset_x, IntNumber offset_y)Same # Cut out a part of the picture
			 */
			case "cut":
				final I_Object[] pars4 = cr.argsFlex( this, 3, 5 );
				final int dx4 = Lib_Convert.toInt( cr, cr.argType( pars4[0], I_IntNumber.class ) );
				final int dy4 = Lib_Convert.toInt( cr, cr.argType( pars4[1], I_IntNumber.class ) );
				final POSITION grav = cr.argType( pars4[2], MagicPosition.class ).get();

				if( pars4.length == 4 )
					throw new CodeError( cr, "Invalid count of parameters", "Need 3 or 5, got " + pars4.length );

				if( pars4.length == 3 )
					this.image.cut( dx4, dy4, grav );
				else { // 5
					final int ox = Lib_Convert.toInt( cr, cr.argType( pars4[3], I_IntNumber.class ) );
					final int oy = Lib_Convert.toInt( cr, cr.argType( pars4[4], I_IntNumber.class ) );
					this.image.cut( dx4, dy4, grav, ox, oy, Color.BLACK );
				}

				return this;

			/**
			 * °brightness ^ fade
			 * °fade(IntNumber b)Same # Change the brightness of the image (-100 to +100)
			 */
			case "brightness":
			case "fade":
				final I_IntNumber par5 = (I_IntNumber)cr.args( this, I_IntNumber.class )[0];
				this.image.brightness( par5.rawInt( cr ) );
				return this;

			/**
			 * °contrast(IntNumber b)Same # Change the contrast of the image (-100 to +100)
			 */
			case "contrast":
				final I_IntNumber par6 = (I_IntNumber)cr.args( this, I_IntNumber.class )[0];
				final int p6 = par6.rawInt( cr );
				this.image.contrast( p6 );
				return this;

			/**
			 * °negative ^ invert
			 * °invert()Same # Invert all colors
			 */
			case "negative":
			case "invert":
				cr.argsNone();
				this.image.negative();
				return this;

			/**
			 * °gamma(Number n)Same # Change the gamma-value
			 */
			case "gamma":
				final I_Number par7 =(I_Number) cr.args( this, I_Number.class )[0];
				this.image.gamma( par7.rawDouble() );
				return this;

			/**
			 * °flipX()Same # Flip the image horizontally
			 */
			case "flipX":
				cr.argsNone();
				this.image.flipX();
				return this;

			/**
			 * °flipY()Same # Flip the image vertically
			 */
			case "flipY":
				cr.argsNone();
				this.image.flipY();
				return this;

			/**
			 * °rotate(Number degrees)Same # Rotate the image by degrees (- = left, + = right)
			 * °rotate(Number degrees, Color background)Same # Rotate the image by degrees (-+), color the background
			 */
			case "rotate":
				final I_Object[] pars8 = cr.argsFlex( this, 1, 2 );
				final I_Number pars8_0 = cr.argType( pars8[0], I_Number.class );
				final double p8 = pars8_0.rawDouble();

				if( pars8.length == 1 )
					this.image.rotate( p8 );
				else {
					final JMo_Color col = cr.argType( pars8[1], JMo_Color.class );
					this.image.rotate( p8, col.getColor() );
				}

				return this;

			/**
			 * °add(Image image, MagicPosition pos)Same # Add another image to this image
			 * °add(Image image, MagicPosition pos, IntNumber offset_x, IntNumber offset_y)Same # Add another image to this image
			 */
			case "add":
				final I_Object[] pars9 = cr.argsFlex( this, 2, 4 );
				final JMo_Image image9 = cr.argType( pars9[0], JMo_Image.class );
				final POSITION grav9 = cr.argType( pars9[1], MagicPosition.class ).get();

				if( pars9.length == 3 )
					throw new CodeError( cr, "Invalid count of parameters", "Need 2 or 4, got " + pars9.length );

				if( pars9.length == 2 )
					this.image.add( image9.image, grav9 );
				else { // 4
					final int ox = cr.argType( pars9[2], I_IntNumber.class ).rawInt( cr );
					final int oy = cr.argType( pars9[3], I_IntNumber.class ).rawInt( cr );
					this.image.add( image9.image, grav9, ox, oy );
				}

				return this;

			/**
			 * °bytes ^ toByteArray
			 * °toByteArray()ByteArray # Converts this image to a sequence of bytes.
			 */
			case "bytes":
			case "toByteArray":
				return new JMo_ByteArray( this.image.getBytesPNG() );
		}

		return null;
	}

	/**
	 * °fill(Color color)Same # Fill this image with a color.
	 * °fill(Color color, IntNumber alpha)Same # Fill this image with a color and alpha channel.
	 * °fill(IntNumber red, IntNumber green, IntNumber blue)Same # Fill this image with a color.
	 * °fill(IntNumber red, IntNumber green, IntNumber blue, IntNumber alpha)Same # Fill this image with a color and alpha channel.
	 */
	private void iFill( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 4 );
		final int argsLen = args.length;
		int r = 0;
		int g = 0;
		int b = 0;

		if( argsLen == 1 || argsLen == 2 ) {
			final Color col = cr.argType( args[0], A_ColorModel.class ).getColor();
			r = col.getRed();
			g = col.getGreen();
			b = col.getBlue();
		}
		else {
			r = cr.argType( args[0], I_IntNumber.class ).rawInt( cr );
			g = cr.argType( args[1], I_IntNumber.class ).rawInt( cr );
			b = cr.argType( args[2], I_IntNumber.class ).rawInt( cr );
		}

		if( argsLen == 2 || argsLen == 4 ) {
			final int a = Lib_Convert.toInt( cr, cr.argType( args[argsLen - 1], I_IntNumber.class ) );
			this.iFillRGBA( r, g, b, a );
		}
		else
			this.iFillRGBA( r, g, b, null );
	}

	private void iFillRGBA( final int r, final int g, final int b, final Integer a ) {
		Err.ifOutOfBounds( 0, 255, r );
		Err.ifOutOfBounds( 0, 255, g );
		Err.ifOutOfBounds( 0, 255, b );
		final int dx = this.image.getWidth();
		final int dy = this.image.getHeight();

		if( a != null ) {
			Err.ifOutOfBounds( 0, 255, a );
			this.image = new MImageX( dx, dy, r, g, b, a );
		}
		else
			this.image = new MImageX( dx, dy, r, g, b );
	}

}
