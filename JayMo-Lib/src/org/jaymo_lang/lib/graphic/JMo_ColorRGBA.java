/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.graphic;

import java.awt.Color;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.numsys.NumSys_Hex;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 16.03.2024
 */
public class JMo_ColorRGBA extends A_ColorModel {

	private final ArgCallBuffer par1, par2, par3, par4;
	private Color               color = null;


	/**
	 * + ColorRGBA()
	 */
	public JMo_ColorRGBA() {
		this( 0, 0, 0, 0 );
	}

	/**
	 * + ColorRGBA(Str htmlColor)
	 */
	public JMo_ColorRGBA( final Call p1 ) {
		this.par1 = new ArgCallBuffer( 0, p1 );
		this.par2 = null;
		this.par3 = null;
		this.par4 = null;
	}

	/**
	 * ! Object of a RGBA-Color (Red,Green,Blue,Alpha), each between 0 and 255.
	 * + ColorRGBA(IntNumber red, IntNumber green, IntNumber blue, IntNumber alpha)
	 */
	public JMo_ColorRGBA( final Call p1, final Call p2, final Call p3, final Call p4 ) {
		this.par1 = new ArgCallBuffer( 0, p1 );
		this.par2 = new ArgCallBuffer( 1, p2 );
		this.par3 = new ArgCallBuffer( 2, p3 );
		this.par4 = new ArgCallBuffer( 3, p4 );
	}

	public JMo_ColorRGBA( final Color c ) {
		this.par1 = null;
		this.par2 = null;
		this.par3 = null;
		this.par4 = null;
		this.color = c;
	}

	public JMo_ColorRGBA( final int r, final int g, final int b, final int a ) {
		this.par1 = null;
		this.par2 = null;
		this.par3 = null;
		this.par4 = null;
		this.color = new Color( r, g, b, a );
	}


	@Override
	public Color getColor() {
		return this.color;
	}

	@Override
	public void init( final CallRuntime cr ) {
		if( this.color != null )
			return; // already set

		if( this.par1 != null && this.par2 != null && this.par3 != null && this.par4 != null ) {
			final I_IntNumber po1 = this.par1.init( cr, this, I_IntNumber.class );
			final I_IntNumber po2 = this.par2.init( cr, this, I_IntNumber.class );
			final I_IntNumber po3 = this.par3.init( cr, this, I_IntNumber.class );
			final I_IntNumber po4 = this.par4.init( cr, this, I_IntNumber.class );

			final int pi1 = Lib_Convert.toInt( cr, po1 );
			final int pi2 = Lib_Convert.toInt( cr, po2 );
			final int pi3 = Lib_Convert.toInt( cr, po3 );
			final int pi4 = Lib_Convert.toInt( cr, po4 );

			Lib_Error.ifNotBetween( cr, 0, 255, pi1, "Value for red" );
			Lib_Error.ifNotBetween( cr, 0, 255, pi2, "Value for green" );
			Lib_Error.ifNotBetween( cr, 0, 255, pi3, "Value for blue" );
			Lib_Error.ifNotBetween( cr, 0, 255, pi4, "Value for alpha" );

			this.color = new Color( pi1, pi2, pi3, pi4 );
		}
		else if( this.par1 != null && this.par2 == null && this.par3 == null && this.par4 == null ) {
			final JMo_Str po1 = this.par1.init( cr, this, JMo_Str.class );
			final String ps = Lib_Convert.toStr( cr, po1 ).rawString();
			if( ps.length() != 9 || ps.charAt( 0 ) != '#' )
				throw new RuntimeError( cr, "Invalid html color", "Need something like '#000000FF', but got: " + ps );

			int r = 0;
			int g = 0;
			int b = 0;
			int a = 0;

			r = Integer.parseInt( ps.substring( 1, 3 ), 16 );
			g = Integer.parseInt( ps.substring( 3, 5 ), 16 );
			b = Integer.parseInt( ps.substring( 5, 7 ), 16 );
			a = Integer.parseInt( ps.substring( 7, 9 ), 16 );

			this.color = new Color( r, g, b, a );
		}
		else
			Err.invalid( this.par1, this.par2, this.par3, this.par4 );
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {

		switch( type ) {
			case NESTED:
				return Lib_Type.getName( this );
			case DESCRIBE:
				return Lib_Type.getName( this ) + "(" + this.color.getRed() + "i," + this.color.getGreen() + "i," + this.color.getBlue() + "i," + this.color.getAlpha() + "i)";
			default:
				return Lib_Type.getName( this ) + "(" + this.color.getRed() + "," + this.color.getGreen() + "," + this.color.getBlue() + "," + this.color.getAlpha() + ")";
		}
	}

	@Override
	protected I_Object call3( final CallRuntime cr, final String method ) {

		switch( method ) {
			/**
			 * °red()Int # Get value of the red color
			 * °green()Int # Get value of the green color
			 * °blue()Int # Get value of the blue color
			 * °alpha()Int # Get alpha value
			 */
			case "red":
				cr.argsNone();
				return new JMo_Int( this.color.getRed() );
			case "green":
				cr.argsNone();
				return new JMo_Int( this.color.getGreen() );
			case "blue":
				cr.argsNone();
				return new JMo_Int( this.color.getBlue() );
			case "alpha":
				cr.argsNone();
				return new JMo_Int( this.color.getAlpha() );

//			case "html":
			case "toHtml":
				return this.mToHtml( cr );

			case "diff":
				return this.mDiff(cr);

			default:
				return null;
		}
	}

	/**
	 * °diff(ColorRGBA other)ColorRGBA # Get the difference as new ColorRGBA Object.
	 */
	private I_Object mDiff( CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_ColorRGBA.class )[0];

		final Color cArg = ((JMo_ColorRGBA)arg).getColor();
		final int r = Util_Color.diffValue( this.color.getRed(), cArg.getRed() );
		final int g = Util_Color.diffValue( this.color.getGreen(), cArg.getGreen() );
		final int b = Util_Color.diffValue( this.color.getBlue(), cArg.getBlue() );
		final int a = Util_Color.diffValue( this.color.getAlpha(), cArg.getAlpha() );

		final Color c = new Color( r, g, b, a );
		return new JMo_ColorRGBA( c );
	}

	/**
	 * °toHtml()Str # Get the color as HTML-Encoding.
	 */
	private JMo_Str mToHtml( final CallRuntime cr ) {
		cr.argsNone();

		StringBuilder sb = new StringBuilder();
		sb.append( "#" );
		sb.append( NumSys_Hex.toHex( this.color.getRed() ) );
		sb.append( NumSys_Hex.toHex( this.color.getGreen() ) );
		sb.append( NumSys_Hex.toHex( this.color.getBlue() ) );
		sb.append( NumSys_Hex.toHex( this.color.getAlpha() ) );
		return new JMo_Str( sb.toString().toLowerCase() );
	}

}
