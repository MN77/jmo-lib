/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.graphic;

import java.awt.Color;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.lib.graphic.U_CMYK;


/**
 * @author Michael Nitsche
 * @created 16.03.2024
 */
public class JMo_ColorCMYK extends A_ColorModel {

	private final ArgCallBuffer par1, par2, par3, par4;
	private int                 cyan    = -1;
	private int                 magenta = -1;
	private int                 yellow  = -1;
	private int                 black   = -1;
	private Color               rgbColor   = null;


	/**
	 * + ColorCMYK()
	 */
	public JMo_ColorCMYK() {
		this( 0, 0, 0, 0 );
	}

	/**
	 * ! Object of a CMYK-Color (Cyan,Magenta,Yellow,Black), each between 0 and 255.
	 * + ColorCMYK(IntNumber cyan, IntNumber magenta, IntNumber yellow, IntNumber black)
	 */
	public JMo_ColorCMYK( final Call p1, final Call p2, final Call p3, final Call p4 ) {
		this.par1 = new ArgCallBuffer( 0, p1 );
		this.par2 = new ArgCallBuffer( 1, p2 );
		this.par3 = new ArgCallBuffer( 2, p3 );
		this.par4 = new ArgCallBuffer( 3, p4 );
	}

	public JMo_ColorCMYK( final int c, final int m, final int y, final int k ) {
		this.par1 = null;
		this.par2 = null;
		this.par3 = null;
		this.par4 = null;
		this.cyan = c;
		this.magenta = m;
		this.yellow = y;
		this.black = k;
	}


	@Override
	public Color getColor() {
		return this.rgbColor;
	}

	@Override
	public void init( final CallRuntime cr ) {

		if( this.par1 != null && this.par2 != null && this.par3 != null && this.par4 != null ) {
			final I_IntNumber po1 = this.par1.init( cr, this, I_IntNumber.class );
			final I_IntNumber po2 = this.par2.init( cr, this, I_IntNumber.class );
			final I_IntNumber po3 = this.par3.init( cr, this, I_IntNumber.class );
			final I_IntNumber po4 = this.par4.init( cr, this, I_IntNumber.class );
			final int pi1 = Lib_Convert.toInt( cr, po1 );
			final int pi2 = Lib_Convert.toInt( cr, po2 );
			final int pi3 = Lib_Convert.toInt( cr, po3 );
			final int pi4 = Lib_Convert.toInt( cr, po4 );
			this.iCheckBounds( cr, pi1, pi2, pi3, pi4 );

			this.cyan = pi1;
			this.magenta = pi2;
			this.yellow = pi3;
			this.black = pi4;
		}
		else
			this.iCheckBounds( cr, this.cyan, this.magenta, this.yellow, this.black );

		final int[] rgb = U_CMYK.cmyk_to_rgb_poor( this.cyan, this.magenta, this.cyan, this.black ); // TODO ICC-Profile should be used
		this.rgbColor = new Color( rgb[0], rgb[1], rgb[2] );
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {

		switch( type ) {
			case NESTED:
				return Lib_Type.getName( this );
			case DESCRIBE:
				return Lib_Type.getName( this ) + "(" + this.cyan + "i," + this.magenta + "i," + this.yellow + "i," + this.black + "i)";
			default:
				return Lib_Type.getName( this ) + "(" + this.cyan + "," + this.magenta + "," + this.yellow + "," + this.black + ")";
		}
	}

	@Override
	protected I_Object call3( final CallRuntime cr, final String method ) {

		switch( method ) {
			/**
			 * °cyan()Int # Get value of the cyan color
			 * °magenta()Int # Get value of the magenta color
			 * °yellow()Int # Get value of the yellow color
			 * °black()Int # Get value of the black color
			 * °diff(ColorCMYK other)ColorCMYK # Get a new color with the difference to the current color
			 */
			case "cyan":
				cr.argsNone();
				return new JMo_Int( this.cyan );
			case "magenta":
				cr.argsNone();
				return new JMo_Int( this.magenta );
			case "yellow":
				cr.argsNone();
				return new JMo_Int( this.yellow );
			case "black":
				cr.argsNone();
				return new JMo_Int( this.black );

			case "diff":
				return this.iDiff( cr );

			default:
				return null;
		}
	}

	private void iCheckBounds( final CallRuntime cr, final int c, final int m, final int y, final int k ) {
		Lib_Error.ifNotBetween( cr, 0, 255, c, "Value for cyan" );
		Lib_Error.ifNotBetween( cr, 0, 255, m, "Value for magenta" );
		Lib_Error.ifNotBetween( cr, 0, 255, y, "Value for yellow" );
		Lib_Error.ifNotBetween( cr, 0, 255, k, "Value for black" );
	}

	private I_Object iDiff( final CallRuntime cr ) {
		final JMo_ColorCMYK arg = cr.arg( this, JMo_ColorCMYK.class );
		final int c = Util_Color.diffValue( this.cyan, arg.cyan );
		final int m = Util_Color.diffValue( this.magenta, arg.magenta );
		final int y = Util_Color.diffValue( this.yellow, arg.yellow );
		final int k = Util_Color.diffValue( this.black, arg.black );

		return new JMo_ColorCMYK( c, m, y, k );
	}

}
