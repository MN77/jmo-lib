/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.http;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.lib.http.Lib_Http;


/**
 * @author Michael Nitsche
 * @created 2020-08-16
 */
public class JMo_HttpClient extends A_ObjectSimple {

	private final Map<String, String> propertys = new HashMap<>();


	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "fetch":
//			case "get":
				return this.fetch( cr );

			case "addProperty":
				this.addProperty( cr );
				return this;

			default:
				return null;
		}
	}

	/**
	 * °addProperty(Str key, Str value)Same # Set a HTTP-Property.
	 */
	private void addProperty( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String val = Lib_Convert.toStr( cr, args[1] ).rawString();
		this.propertys.put( key, val );
	}

	/**
	 * °fetch(Str url)Str # Fetch website from the URL.
	 */
	private JMo_Str fetch( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Str.class )[0];
		final String url = Lib_Convert.toStr( cr, arg ).rawString();
		final String result = this.iConnect( cr, url );
		return new JMo_Str( result );
	}

	private String iConnect( final CallRuntime cr, final String url ) {

		try {
			return Lib_Http.fetch( url, null, true, this.propertys ); // Follow
		}
		catch( final MalformedURLException e ) {
			throw new RuntimeError( cr, "Malformed URL", e.getMessage() );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "Connection error", e.getMessage() );
		}
	}

}
