Base32	BaseCodec	0	0		
Base32Hex	BaseCodec	0	0		
Base64	BaseCodec	0	0		
BaseCodec	Object	1	0		
BrickLinkAPI	Object	0	0		
CipherBlowfish	Cipher	0	0		
CipherDES	Cipher	0	0		
Cipher	Object	1	0		
CipherRC2	Cipher	0	0		
CipherRC4	Cipher	0	0		
CipherRijndael	Cipher	0	0		
CipherRot13	RotCipher	0	0		
CipherRot18	RotCipher	0	0		
CipherRot26	RotCipher	0	0		
CipherRot47	RotCipher	0	0		
CipherRot5	RotCipher	0	0		
CipherTripleDES	Cipher	0	0		
ColorCMYK	Color	0	0		Object of a CMYK-Color (Cyan,Magenta,Yellow,Black), each between 0 and 255.
ColorHSL	Color	0	0		
ColorHSV	Color	0	0		
ColorRGBA	Color	0	0		Object of a RGBA-Color (Red,Green,Blue,Alpha), each between 0 and 255.
ColorRGB	Color	0	0		Object of a RGB-Color (Red,Green,Blue), each between 0 and 255.
Css	Object	0	0		Object of a Cascading Style Sheet Tree
Csv	Object	0	0		CSV-Object to read, handle and write a CSV-File
Digest	Object	1	0		
Emoji	Object	0	0		Helper, to create Emoji-Chars
Font	Object	0	0		
Fuzzy	Object	0	0		
HashMD5	Digest	0	0		
HashSHA	Digest	0	0		
HttpClient	Object	0	0		
Image	Object	0	0		Object of an image
Jar	Object	0	0		
Json	Object	0	0		
JsonObject	Object	0	0		
MIniConf	Object	0	0		
MIniConfResult	Object	0	0		
NtpClient	Object	0	0		
PropertiesFile	Object	0	0		
RelationSet	Object	0	0		
RotCipher	Cipher	1	0		
SntpClient	Object	0	0		
Swing_Button	Swing_JComponent	0	0		
Swing_ComboBox	Swing_JComponent	0	0		
Swing_DialogMessage	EventObject	0	0		
Swing_Dialog	Swing_Object	0	0		Object of a single Swing-Dialog
Swing_DialogYesNo	EventObject	0	0		
Swing_Font	Object	0	0		
Swing_Frame	Swing_Object	1	0		Object of a single Swing-Frame
Swing_Image	Swing_JComponent	0	0		
Swing_JComponent	Swing_Object	1	0		
Swing_JTextComponent	Swing_JComponent	1	0		
Swing_Label	Swing_JComponent	0	0		
Swing_LayoutBorder	Swing_Object	0	0		
Swing_LayoutFlex	Swing_Object	0	0		
Swing_LayoutGrid	Swing_JComponent	0	0		
Swing_LayoutPanel	Swing_JComponent	0	0		
Swing_LayoutRow	Swing_Object	0	0		
Swing_List	Swing_JComponent	0	0		
Swing_Main	Swing_Frame	0	0		Main-Frame of a Swing-Application
Swing_Object	EventObject	1	0		
Swing_ProgressBar	Swing_JComponent	0	0		
Swing_Scale	Swing_JComponent	0	0		
Swing_Slider	Swing_JComponent	0	0		
Swing_Spinner	Swing_JComponent	0	0		
Swing_TabbedPane	Swing_JComponent	0	0		
Swing_Table	Swing_JComponent	0	0		
Swing_TextArea	Swing_JComponent	0	0		
Swing_Text	Swing_JTextComponent	0	0		
Swing_ToggleButton	Swing_JComponent	0	0		
Swing_TreeNode	Object	0	0		
Swing_Tree	Swing_JComponent	0	0		
Swing_Window	Swing_Frame	0	0		Object of a single Swing-Window
TcpClient	EventObject	0	0		
TcpConnection	EventObject	0	0		
TcpServer	EventObject	0	0		
UdpPush	Object	0	0		
UdpService	EventObject	0	0		
Uri	Immutable	0	0		
Wiki77	Object	0	0		
XmlDocument	XmlNode	0	0		
XmlNode	Object	0	0		
Xml	Object	0	0		
