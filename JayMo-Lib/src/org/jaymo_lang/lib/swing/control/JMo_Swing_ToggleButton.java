/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import java.awt.Font;

import javax.swing.Icon;
import javax.swing.JToggleButton;

import org.jaymo_lang.lib.font.JMo_Font;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.control.A_JG_ToggleButtonI;
import org.jaymo_lang.lib.gui.control.A_JG_ToggleButtonM;
import org.jaymo_lang.lib.swing.tools.Util_Swing_Font;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 16.10.2020
 */
public class JMo_Swing_ToggleButton extends A_Swing_JComponent {

	private JToggleButton       button;
	private final ArgCallBuffer initArg1;
	private final ArgCallBuffer initArg2;


	private final A_JG_ToggleButtonM methods = new A_JG_ToggleButtonM() {

		@Override
		protected void setText( String text ) {
			JMo_Swing_ToggleButton.this.button.setText( text );
		}

		@Override
		protected String getText() {
			return JMo_Swing_ToggleButton.this.button.getText();
		}

		@Override
		protected boolean isSelected() {
			return JMo_Swing_ToggleButton.this.button.isSelected();
		}

		@Override
		protected void setSelected( boolean b ) {
			JMo_Swing_ToggleButton.this.button.setSelected( b );
		}

		@Override
		protected void setImage( JMo_Image image ) {
			Err.ifNull( image );
			final Icon icon = image.internalGet().getIcon();
			JMo_Swing_ToggleButton.this.button.setIcon( icon );
		}

		@Override
		protected void removeImage() {
			JMo_Swing_ToggleButton.this.button.setIcon( null );
		}

		@Override
		protected void setFont( JMo_Font jmoFont ) {
//			Font font = Font.decode("Nunito");
			final Font font = Util_Swing_Font.toSwingFont( jmoFont );
			JMo_Swing_ToggleButton.this.button.setFont( font );
		}
	};

	private final A_JG_ToggleButtonI inits = new A_JG_ToggleButtonI() {

		@Override
		protected void arg0str( JMo_Str s ) {
			JMo_Swing_ToggleButton.this.button.setText( s.rawString() );
		}

		@Override
		protected void atSelect( CallRuntime cr, String event ) {
			JMo_Swing_ToggleButton.this.button.addActionListener( e -> JMo_Swing_ToggleButton.this.eventRun( cr, event, JMo_Swing_ToggleButton.this ) );
		}

		@Override
		protected void argImageNorm( JMo_Image img ) {
			JMo_Swing_ToggleButton.this.button.setIcon( img.internalGet().getIcon() );
		}

		@Override
		protected void argImagePress( JMo_Image img ) {
			final Icon pressed = img.internalGet().getIcon();
			JMo_Swing_ToggleButton.this.button.setPressedIcon( pressed );
			JMo_Swing_ToggleButton.this.button.setSelectedIcon( pressed );
		}
	};


	/**
	 * +Swing_ToggleButton()
	 * +Swing_ToggleButton( Str title )
	 * +Swing_ToggleButton( Image icon )
	 * +Swing_ToggleButton( Image defaultIcon, Image otherIcon )
	 */
	public JMo_Swing_ToggleButton() {
		this.initArg1 = null;
		this.initArg2 = null;
	}

	public JMo_Swing_ToggleButton( final Call title ) {
		this.initArg1 = new ArgCallBuffer( 0, title );
		this.initArg2 = null;
	}

	public JMo_Swing_ToggleButton( final Call icon1, final Call icon2 ) {
		this.initArg1 = new ArgCallBuffer( 0, icon1 );
		this.initArg2 = new ArgCallBuffer( 1, icon2 );
	}

	/**
	 * °setFont(Font f)Same # Set the font of the text.
	 * °setText(Atomic text)Same # Set the text on this button.
	 * °getText()Str # Returns the text of this button.
	 * ###°isSelected ^ getSelection
	 * °isSelected()Bool # Returns true, if the button is selected.
	 * °setImage ^ setIcon
	 * °setIcon(Image img)Same # Set the icon image of this button.
	 * °setSelected(Bool b)Same # Stet the selection state ofthe button.
	 */
	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JToggleButton getSwing() {
		return this.button;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.button = new JToggleButton();
		this.inits.init(cr, this, this.initArg1, this.initArg2);
	}

//	@Override
//	protected String resolveDefault() {
//		return "@select";
//	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {

		switch( type ) {
			case NESTED:
			case IDENT:
				return this.getTypeName();
			default:
				final String text = this.button == null ? "" : this.button.getText();
				return this.toString() + "(\"" + text + "\")";
		}
	}

	/**
	 * °@select # Executed on selection.
	 */
	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

}
