/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll.composit;

import javax.swing.JComponent;
import javax.swing.JSpinner;

import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_SpinnerI;
import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_SpinnerM;
import org.jaymo_lang.lib.swing.control.A_Swing_JComponent;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 17.10.2020
 */
public class JMo_Swing_Spinner extends A_Swing_JComponent {

	private JSpinner spinner;
	private Integer minValue = null;
	private Integer maxValue = null;

	/**
	 * °getValue()Int # Returns the current value
	 * °getMin()Int # Returns the minimum limit
	 * °getMax()Int # Returns the maximum limit
	 * °setValue(Int value)Same # Set value
 	 * °setRange(Range r)Same # Set the minimum and maximum
 	 * °setRange(Int Range r)Same # Set the minimum and maximum
	 */
	private final A_JG_SpinnerM methods = new A_JG_SpinnerM() {

		@Override
		protected void setValue( int value ) {
			JMo_Swing_Spinner.this.spinner.setValue( value );
		}

		@Override
		protected int getValue() {
			return (Integer)JMo_Swing_Spinner.this.spinner.getValue();
		}

		@Override
		protected void setMinMax( int min, int max ) {
			JMo_Swing_Spinner.this.minValue = min;
			JMo_Swing_Spinner.this.maxValue = max;
		}

		@Override
		protected int getMin() {
			return JMo_Swing_Spinner.this.minValue == null
				? Integer.MIN_VALUE
				: JMo_Swing_Spinner.this.minValue;
		}

		@Override
		protected int getMax() {
			return JMo_Swing_Spinner.this.maxValue == null
				? Integer.MAX_VALUE
				: JMo_Swing_Spinner.this.maxValue;
		}
	};

	/**
	 * °@change # Executed on a change.
	 */
	private final A_JG_SpinnerI inits = new A_JG_SpinnerI() {

		@Override
		protected void atChange( CallRuntime cr, String event ) {
			JMo_Swing_Spinner.this.spinner.addChangeListener(
				e -> JMo_Swing_Spinner.this.eventRun( cr, event, JMo_Swing_Spinner.this )
			);
		}

		@Override
		protected void argsMinMax( int min, int max ) {
			JMo_Swing_Spinner.this.minValue = min;
			JMo_Swing_Spinner.this.maxValue = max;
		}
	};


	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JComponent getSwing() {
		return this.spinner;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.spinner = new JSpinner();
		this.inits.init( cr, this );

		JMo_Swing_Spinner.this.spinner.addChangeListener( e -> {
			if(this.minValue != null) {
				int value = (Integer)JMo_Swing_Spinner.this.spinner.getValue();
				if(value < this.minValue)
					this.spinner.setValue( this.minValue );
			}

			if(this.maxValue != null) {
				int value = (Integer)JMo_Swing_Spinner.this.spinner.getValue();
				if(value > this.maxValue)
					this.spinner.setValue( this.maxValue );
			}
		});
	}

	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

//	@Override
//	protected String resolveDefault() {
//		return "@change";
//	}

}
