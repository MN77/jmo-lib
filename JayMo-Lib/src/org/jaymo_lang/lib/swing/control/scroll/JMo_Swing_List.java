/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;

import org.jaymo_lang.lib.gui.control.scroll.A_JG_ListI;
import org.jaymo_lang.lib.gui.control.scroll.A_JG_ListM;
import org.jaymo_lang.lib.swing.control.A_Swing_JComponent;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Java;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 24.11.2020
 * @implNote
 * 		http://www.java2s.com/Tutorials/Java/Swing_How_to/JList/Add_String_item_to_JList.htm
 */
public class JMo_Swing_List extends A_Swing_JComponent {

	private DefaultListModel<Object> dlm;
	private JList<Object> list;
	private JScrollPane   pane;
	private ArgCallBuffer arg0 = null;

	/**
	 * °set ^ setData
	 * °setData(List data)Same # Set the data of this list.
	 * °getSelection()List # Returns the selected values.
	 */
	private final A_JG_ListM methods = new A_JG_ListM() {
		@Override
		public void add(JMo_Str[] items) {
			for(JMo_Str item : items)
				JMo_Swing_List.this.dlm.addElement( item.rawString() );
		}

		@Override
		public JMo_List getSelection() {
			final List<Object> selected = JMo_Swing_List.this.list.getSelectedValuesList();
			final SimpleList<I_Object> al = new SimpleList<>( selected.size() );

			for( final Object e : selected )
				al.add( Lib_Java.javaToJmo( e ) );

			return new JMo_List( al );
		}

		@Override
		public void setItems(CallRuntime cr, JMo_List items) {
			JMo_Swing_List.this.iSetData(cr, items);
		}
	};

	/**
	 * °@select # Executed on selection.
	 */
	private final A_JG_ListI inits = new A_JG_ListI() {

		@Override
		protected void arg0list( CallRuntime cr, JMo_List list ) {
			JMo_Swing_List.this.iSetData( cr, list );
		}

		@Override
		protected void atSelect( CallRuntime cr, String event ) {
			JMo_Swing_List.this.list.addListSelectionListener( e -> JMo_Swing_List.this.eventRun( cr, event, JMo_Swing_List.this ) ); // Will be aktivated at cursor move and twice at mouse click!
		}
	};


	/**
	 * +Swing_List()
	 * +Swing_List(List items)
	 */
	public JMo_Swing_List() {}

	public JMo_Swing_List( final Call list ) {
		this.arg0 = new ArgCallBuffer( 0, list );
	}


	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JComponent getSwing() {
		return this.pane;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.dlm = new DefaultListModel<>();
		this.list = new JList<>(this.dlm);
		this.pane = new JScrollPane( this.list );

		this.inits.init( cr, this, this.arg0 );
	}

	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

//	@Override
//	protected String resolveDefault() {
//		return "@select";
//	}

	private void iSetData( final CallRuntime cr, final JMo_List list ) {
		final SimpleList<I_Object> al = list.getInternalCollection();
		final int size = al.size();
		final Object[] listData = new String[size];
		for( int i = 0; i < size; i++ )
//			listData[i] = Lib_Convert.getStringValue(cr, al.get(i));
			listData[i] = Lib_Java.jmoToJava( cr, al.get( i ), null );

		this.list.setListData( listData );
	}

}
