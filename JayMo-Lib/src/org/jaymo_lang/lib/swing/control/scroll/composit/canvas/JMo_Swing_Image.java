/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll.composit.canvas;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.control.scroll.composit.canvas.A_JG_ImageI;
import org.jaymo_lang.lib.gui.control.scroll.composit.canvas.A_JG_ImageM;
import org.jaymo_lang.lib.swing.I_Swing_Object;
import org.jaymo_lang.lib.swing.control.A_Swing_JComponent;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.position.Lib_Position;
import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;
import de.mn77.base.data.constant.position.POSITION_V;
import de.mn77.lib.graphic.MImageX;


/**
 * @author Michael Nitsche
 * @created 05.04.2020
 */
public class JMo_Swing_Image extends A_Swing_JComponent implements I_Object, I_Swing_Object {

	private JLabel              label;
	private final ArgCallBuffer arg0; // Image


	private final A_JG_ImageM methods = new A_JG_ImageM() {
		@Override
		public JMo_Image getImage() {
			Icon icon = JMo_Swing_Image.this.label.getIcon();
			MImageX image = new MImageX(icon);
			return new JMo_Image(image);
		}

		@Override
		public POSITION getAlign() {
			final int ah = JMo_Swing_Image.this.label.getHorizontalAlignment();
			final int av = JMo_Swing_Image.this.label.getVerticalAlignment();

			if(ah == SwingConstants.LEFT) {
				return av == SwingConstants.TOP
					? POSITION.TOP_LEFT
					: av == SwingConstants.CENTER
						? POSITION.LEFT
						: POSITION.BOTTOM_LEFT;
			}
			else if(ah == SwingConstants.CENTER) {
				return av == SwingConstants.TOP
					? POSITION.TOP
					: av == SwingConstants.CENTER
						? POSITION.CENTER
						: POSITION.BOTTOM;
			}
			else { // RIGHT
				return av == SwingConstants.TOP
					? POSITION.TOP_RIGHT
					: av == SwingConstants.CENTER
						? POSITION.RIGHT
						: POSITION.BOTTOM_RIGHT;
			}
		}

		@Override
		public void setImage( JMo_Image image ) {
			JMo_Swing_Image.this.label.setIcon( image.internalGet().getIcon() );
		}

		@Override
		public void setAlign( POSITION position ) {
			final POSITION_H pos_h = Lib_Position.getHorizontal( position );
			final int pos_hi =
				pos_h == POSITION.LEFT
				? SwingConstants.LEFT
				: pos_h == POSITION.RIGHT
					? SwingConstants.RIGHT
					: SwingConstants.CENTER;

			final POSITION_V pos_v = Lib_Position.getVertical( position );
			final int pos_vi =
				pos_v == POSITION.TOP
				? SwingConstants.TOP
				: pos_v == POSITION.BOTTOM
					? SwingConstants.BOTTOM
					: SwingConstants.CENTER;

			JMo_Swing_Image.this.label.setHorizontalAlignment( pos_hi );
			JMo_Swing_Image.this.label.setVerticalAlignment( pos_vi );
		}
	};

	private final A_JG_ImageI inits = new A_JG_ImageI() {
		@Override
		protected void arg0image( JMo_Image s ) {
			final Icon icon = s.internalGet().getIcon();
			JMo_Swing_Image.this.label.setIcon( icon );
		}
	};


	public JMo_Swing_Image() {
		this.arg0 = null;
	}

	public JMo_Swing_Image( final Call image ) {
		this.arg0 = new ArgCallBuffer( 0, image );
	}

	@Override
	public I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °setImage(Image image)Same # Set the image.
	 */
	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JLabel getSwing() {
		return this.label;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.label = new JLabel();
		this.label.setHorizontalAlignment( SwingConstants.CENTER );
		this.label.setVerticalAlignment( SwingConstants.CENTER );

		this.inits.init( cr, this, this.arg0 );

//		this.label.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				this.eventRun("@select");
//			}
//		});
	}

	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}


	/**
	 * °setAlign(MagicPosition pos)Same # Set the alignment of the image.
	 */

//	private void tip(final CallRuntime cr) {
//		final I_Object arg = cr.args(this, Str.class)[0];
//		final String s = Lib_Convert.getStringValue(cr, arg);
//		this.label.setToolTipText(s);
//	}

}
