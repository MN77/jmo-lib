/*******************************************************************************
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Window;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.ImageIcon;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.lib.gui.control.scroll.composit.canvas.deco.A_JG_WindowM;
import org.jaymo_lang.lib.swing.A_Swing_Object;
import org.jaymo_lang.lib.swing.I_Swing_Object;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 *
 * @implNote
 *           https://docs.oracle.com/javase/tutorial/uiswing/layout/visual.html
 *           https://docs.oracle.com/javase/tutorial/uiswing/layout/gridbag.html
 */
public abstract class A_Swing_Window extends A_Swing_Object {

	protected enum STATE {
		PREPARED,
		RUNNING,
		CLOSED
	}

	private enum SIZE {
		CURRENT,
		MIN,
		MAX
	}


	private static final HashSet<JMo_Swing_Main> activeMains = new HashSet<>();

	private final ArgCallBuffer par_parent, par_dx, par_dy, par_title;
	private Window              window = null;
	private STATE               state  = STATE.PREPARED;
	private ArrayList<Window>   childs = null;

	/**
	 * °add(Swing_Object obj)Same # Add a object to this frame.
	 * °close()Nil # Close this frame.
	 * #°close()Nil # Terminates the Swing-Application.
	 * °setSize(Int dx, Int dy)Same # Set the size of this frame.
	 * °getTitle()Str # Returns the title of this frame.
	 * °setTitle(Str name)Same # Set the title of this frame.
	 */
	private final A_JG_WindowM methods = new A_JG_WindowM() {

		@Override
		protected void add( final I_Object obj ) {
			final I_Swing_Object sObj = (I_Swing_Object)obj;
			A_Swing_Window.this.pReplace( sObj.getSwing() );
		}

		@Override
		protected void close( final CallRuntime cr ) {
			A_Swing_Window.this.closeFrame( cr, true );
			A_Swing_Window.activeMains.remove( A_Swing_Window.this );
//			cr.getApp().checkExit(true); 	// TODO
		}

		@Override
		protected String getTitle() {
			return A_Swing_Window.this.pGetTitle();
		}

		@Override
		protected void setSize( final int width, final int height ) {
			A_Swing_Window.this.iSize( SIZE.CURRENT, width, height );
		}

		@Override
		protected void setTitle( final String title ) {
			A_Swing_Window.this.pSetTitle( title );
		}

	};


	public static void terminateAll() {
		for( final JMo_Swing_Main frame : A_Swing_Window.activeMains )
			frame.closeFrame( null, false );
		A_Swing_Window.activeMains.clear();
	}


	public A_Swing_Window( final Call parent ) {
		this.par_parent = parent == null ? null : new ArgCallBuffer( 0, parent );
		this.par_dx = null;
		this.par_dy = null;
		this.par_title = null;
	}

	public A_Swing_Window( final Call parent, final Call title ) {
		this.par_parent = parent == null ? null : new ArgCallBuffer( 0, parent );
		this.par_dx = null;
		this.par_dy = null;
		this.par_title = new ArgCallBuffer( 1, title );
	}

	public A_Swing_Window( final Call parent, final Call dx, final Call dy ) {
		this.par_parent = parent == null ? null : new ArgCallBuffer( 0, parent );
		this.par_dx = new ArgCallBuffer( 1, dx );
		this.par_dy = new ArgCallBuffer( 2, dy );
		this.par_title = null;
	}

	/**
	 * ! Object of a single Swing-Frame
	 * #+ Swing_Window(Swing_Window parent)
	 * #+ Swing_Window(Swing_Window parent, Str title)
	 * #+ Swing_Window(Swing_Window parent, IntNumber dx, IntNumber dy)
	 * #+ Swing_Window(Swing_Window parent, IntNumber dx, IntNumber dy, Str title)
	 */
	public A_Swing_Window( final Call parent, final Call dx, final Call dy, final Call title ) {
		this.par_parent = parent == null ? null : new ArgCallBuffer( 0, parent );
		this.par_dx = new ArgCallBuffer( 1, dx );
		this.par_dy = new ArgCallBuffer( 2, dy );
		this.par_title = new ArgCallBuffer( 1, title );
	}

	public STATE getState() {
		return this.state;
	}

	@Override
	public final void init1( final CallRuntime cr ) {
		final A_Swing_Window parent = this.par_parent != null
			? this.par_parent.init( cr, this, A_Swing_Window.class )
			: null;

		final Frame parentFrame = parent != null && parent.window instanceof Frame
			? (Frame)parent.window
			: null;

		this.window = this.pCreateFrame( parentFrame );
		Err.ifNull( this.window );

		if( parent != null )
			parent.registerFrame( this.window );

//		this.frame.setLayout(new BorderLayout());
		final GridLayout grid = new GridLayout( 0, 1 );
		this.window.setLayout( grid );

		final ImageIcon img = new ImageIcon( this.getClass().getClassLoader().getResource( "jar/logo/jaymo_icon_256.png" ) );
		this.window.setIconImage( img.getImage() );

		if( this.par_dx != null && this.par_dy != null ) {
			final int width = Lib_Convert.toInt( cr, this.par_dx.init( cr, this, I_IntNumber.class ) );
			final int height = Lib_Convert.toInt( cr, this.par_dy.init( cr, this, I_IntNumber.class ) );
			this.window.setSize( width, height );
		}

		if( this.par_title != null ) {
			final String title = Lib_Convert.toStr( cr, this.par_title.init( cr, this, JMo_Str.class ) ).rawString();
			this.pSetTitle( title );
		}

		this.init2( cr );
	}

	public abstract void init2( final CallRuntime cr );

	protected void addActiveMain( final JMo_Swing_Main main ) {
		A_Swing_Window.activeMains.add( main );
	}

	@Override
	protected final I_Object callMethod1( final CallRuntime cr, final String method ) {
		if( this.state == STATE.CLOSED )
			throw new RuntimeError( cr, "Illegal Frame-Access", "This Frame is already closed" );

		final I_Object result = this.methods.call( cr, method, this );
		if( result != null )
			return result;

		switch( method ) {
			case "setSizeMin":
				this.mSize( cr, SIZE.MIN );
				return this;
//			case "setSizeMax":	return size(c, SIZE.MAX);	//TODO Doesn't work
//			case "setIcon":	// TODO
//			case "getIcon": // TODO

			/**
			 * °pack()Same # Resize the Frame, so that all components use their preferred size and layouts.
			 */
			case "pack":
				cr.argsNone();
				this.window.pack();
				return this;

			default:
				return this.callMethod2( cr, method );
		}
	}

	protected abstract I_Object callMethod2( final CallRuntime cr, String method );

	protected void cleanUp( final CallRuntime cr ) {
//		for(final Frame f : Frame.getFrames()) { // !!!CAUTION!!! This also close Frames from other Main-Frames! And it closes JayMo-Edit
		if( this.childs != null )
			for( final Window w : this.childs ) {
				w.setVisible( false );
				w.dispose(); // Otherwise, the Frame is running further in background
			}

		this.window.dispose(); // Otherwise, the Frame is running further in background

		if( this.par_parent == null ) // Because, only Main-Frames are registered Forks
			cr.getApp().checkExit( true );
	}

	protected void closeFrame( final CallRuntime cr, final boolean stateCheck ) {
		if( stateCheck )
			switch( this.state ) {
				case PREPARED:
					throw new RuntimeError( cr, "Illegal Frame-Access", "This frame is not running." );
				case CLOSED:
					throw new RuntimeError( cr, "Illegal Frame-Access", "This frame is already closed." );
				case RUNNING:
			}

		this.window.dispose();
		this.state = STATE.CLOSED;
	}

	protected boolean isRunning() {
		return this.state == STATE.RUNNING;
	}

	protected abstract Window pCreateFrame( Frame parent );

	protected abstract String pGetTitle();

	protected abstract void pReplace( Component swing );

	protected abstract void pSetTitle( String title );

	protected void setRunState( final CallRuntime cr, final boolean stateCheck ) {
		if( stateCheck )
			switch( this.state ) {
				case RUNNING:
					throw new RuntimeError( cr, "Illegal Frame-Access", "This frame is already running." );
				case CLOSED:
					throw new RuntimeError( cr, "Illegal Frame-Access", "This frame is closed." );
				case PREPARED:
			}

		this.state = STATE.RUNNING;
	}

	protected void setState( final STATE s ) {
		this.state = s;
	}

	private void iSize( final SIZE s, final int dx, final int dy ) {

		switch( s ) {
			case CURRENT:
				this.window.setSize( dx, dy );
				break;
			case MIN:
				this.window.setMinimumSize( new Dimension( dx, dy ) ); //TODO null possible to deactivate
				break;
			case MAX: //TODO Doesn't work today
				this.window.setMaximumSize( new Dimension( dx, dy ) ); //TODO null possible to deactivate
				break;
		}
	}

	/**
	 * °setSizeMin(Int dx, Int dy)Same # Set the minimum size of this frame.
	 */
	private A_Swing_Window mSize( final CallRuntime cr, final SIZE s ) {
		final I_Object[] args = cr.args( this, JMo_Int.class, JMo_Int.class );
		final int dx = Lib_Convert.toInt( cr, args[0] );
		final int dy = Lib_Convert.toInt( cr, args[1] );

		this.iSize( s, dx, dy );
		return this;
	}

	private void registerFrame( final Window window ) {
		Err.ifNull( window );
		if( this.childs == null )
			this.childs = new ArrayList<>();
		this.childs.add( window );
	}

}
