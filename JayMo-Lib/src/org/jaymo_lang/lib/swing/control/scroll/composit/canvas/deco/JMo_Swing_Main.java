/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.WindowConstants;

import org.jaymo_lang.lib.gui.control.scroll.composit.canvas.deco.A_JG_MainM;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Char;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 04.04.2020
 */
public class JMo_Swing_Main extends A_Swing_Window {

	private JFrame frame = null;


	private final A_JG_MainM methods = new A_JG_MainM() {

		@Override
		protected void run( final CallRuntime cr ) {
			JMo_Swing_Main.this.iRun( cr );
		}

	};


	public JMo_Swing_Main() {
		super( null );
	}

	public JMo_Swing_Main( final Call title ) {
		super( null, title );
	}

	public JMo_Swing_Main( final Call dx, final Call dy ) {
		super( null, dx, dy );
	}

	/**
	 * ! Main-Frame of a Swing-Application
	 * + Swing_Main()
	 * + Swing_Main(Str title)
	 * + Swing_Main(IntNumber dx, IntNumber dy)
	 * + Swing_Main(IntNumber dx, IntNumber dy, Str title)
	 */
	public JMo_Swing_Main( final Call dx, final Call dy, final Call title ) {
		super( null, dx, dy, title );
	}

	public Component getSwing() {
		return this.frame;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.frame.addWindowListener( new WindowListener() {

			public void windowActivated( final WindowEvent e ) {}

			public void windowClosed( final WindowEvent e ) {
//				MOut.temp("Main-Frame closed");
				JMo_Swing_Main.this.cleanUp( cr );
			}

			public void windowClosing( final WindowEvent e ) {}

			public void windowDeactivated( final WindowEvent e ) {}

			public void windowDeiconified( final WindowEvent e ) {}

			public void windowIconified( final WindowEvent e ) {}

			public void windowOpened( final WindowEvent e ) {}

		} );
	}

	/**
	 * °@exec # Executed when a Enter-Key is pressed.
	 * °@key # Executed when a key is pressed.
	 * °@ready # Executed when the GUI is started and ready.
	 */
	@Override
	public boolean validateEvent( final String event ) {

		switch( event ) {
			case "@exec":
			case "@key":
			case "@ready":
				return true;
			default:
				return false;
		}
	}


	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	@Override
	protected I_Object callMethod2( final CallRuntime cr, final String method ) {
		final I_Object result = this.methods.call( cr, method, this );
		if( result != null )
			return result;

		switch( method ) {
			case "setLook":
				this.mSetLook( cr );
				return this;
			case "getLook":
				return this.mGetLook( cr );
//			case "getLooks":	// No "set", no "get"
//			case "availableLooks":
			case "allLooks":
//			case "looks":
				return this.mAllLooks( cr );

			/**
			 * °maximize()Same # Maximize this frame.
			 */
			case "maximize":
				cr.argsNone();
				this.frame.setExtendedState( this.frame.getExtendedState() | Frame.MAXIMIZED_BOTH );
				return this;
		}

		return null;
	}

	@Override
	protected Window pCreateFrame( final Frame parent ) {
		return this.frame = new JFrame();
	}

	@Override
	protected String pGetTitle() {
		return this.frame.getTitle();
	}


	// Internal private //

	@Override
	protected void pReplace( final Component comp ) {
		((RootPaneContainer)this.frame).getContentPane().add( comp );
	}

	@Override
	protected void pSetTitle( final String title ) {
		this.frame.setTitle( title );
	}

	/**
	 * °run()Same # Start the Swing-Application and show the main frame.
	 */
	private void iRun( final CallRuntime cr ) {
//		cr.argsNone();
		this.setRunState( cr, true );
		cr.getApp().registerFork();
		this.addActiveMain( this );

		SwingUtilities.invokeLater( () -> {
//			MOut.print((Object)this.getLooks());
//			this.iSetLook("Metal");

//			final GridLayout l = new GridLayout(0, 1);
//			this.frame.setLayout(l);

			//this.frame.setSize(400, 200);
			JMo_Swing_Main.this.frame.setLocationRelativeTo( null ); // Fenster zentriert

//			this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JMo_Swing_Main.this.frame.setDefaultCloseOperation( WindowConstants.DISPOSE_ON_CLOSE );

//			this.frame.getContentPane().setBackground(Color.green);

//			this.frame.pack();
			JMo_Swing_Main.this.frame.setVisible( true );


			// Maximize if no size is set!
			final Dimension sizeDim = this.frame.getSize();
			if( !this.frame.isMinimumSizeSet() && !this.frame.isPreferredSizeSet() && !this.frame.isMaximumSizeSet() && sizeDim.height == 0 && sizeDim.width == 0 )
				JMo_Swing_Main.this.frame.setExtendedState( JMo_Swing_Main.this.frame.getExtendedState() | Frame.MAXIMIZED_BOTH );

			JMo_Swing_Main.this.eventRun( cr, "@ready", JMo_Swing_Main.this ); // TODO Maybe not correct here
		} );

		cr.getApp().doAtCalledExit( () -> {
			this.cleanUp( cr );
		} );


		KeyboardFocusManager.getCurrentKeyboardFocusManager()
			.addKeyEventDispatcher( e -> {
				if( e.getID() != KeyEvent.KEY_RELEASED )
					return false;

				if( e.getKeyCode() == 10 && e.getModifiersEx() == 0 )
					JMo_Swing_Main.this.eventRun( cr, "@exec", JMo_Swing_Main.this ); // TODO Main.this?!?
				else {
//					if(JMo_Swing_Main.this.event)	if has event handlers ...
					final SimpleList<I_Object> al = new SimpleList<>( 2 );
//					al.add(new Int(e.getKeyCode()));
					al.add( JMo_Char.createSimple( e.getKeyChar() ) );
					al.add( new JMo_Int( e.getModifiersEx() ) );
					JMo_Swing_Main.this.eventRun( cr, "@key", new JMo_List( al ) );
				}

				return false; // All other KeyEvent-Handler will be executed
			} );
	}

	private String iSearchLook( final String search ) {
		final LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
		for( final LookAndFeelInfo info : lafs )
			if( info.getName().contains( search ) )
				return info.getClassName();
		for( final LookAndFeelInfo info : lafs )
			if( info.getClassName().contains( search ) )
				return info.getClassName();
		return null;
	}

	private void iSetLook( final String s ) {

		try {
			final String name = this.iSearchLook( s );
			if( name == null )
				Err.invalid( "Look not found", s, name );
			UIManager.setLookAndFeel( name );
		}
		catch( final Exception ex ) {
			Err.exit( ex );
		}

		// Turn off metal's use bold fonts
//      UIManager.put("swing.boldMetal", Boolean.FALSE);
	}

	/**
	 * °allLooks()List # Get a List with all available LookAndFeels.
	 */
	private JMo_List mAllLooks( final CallRuntime cr ) {
		cr.argsNone();
		final LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
		final SimpleList<I_Object> al = new SimpleList<>( lafs.length );

		for( final LookAndFeelInfo info : lafs )
			al.add( new JMo_Str( info.getName() ) );
		return new JMo_List( al );
	}

	/**
	 * °getLook()Str # Return the current 'Look and Feel'.
	 */
	private JMo_Str mGetLook( final CallRuntime cr ) {
		cr.argsNone();
		final String currentLook = UIManager.getLookAndFeel().getName(); // getID()
		return new JMo_Str( currentLook );
	}

	/**
	 * °setLook(Str name)Same # Set a 'Look and Feel'.
	 */
	private void mSetLook( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		this.iSetLook( arg.rawString() );
		this.frame.repaint();
	}

}
