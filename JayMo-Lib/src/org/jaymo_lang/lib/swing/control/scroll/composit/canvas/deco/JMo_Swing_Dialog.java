/*******************************************************************************
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco;

import java.awt.Component;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JDialog;
import javax.swing.WindowConstants;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Java;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 11.01.2022
 */
public class JMo_Swing_Dialog extends A_Swing_Window {

	private JDialog dialog = null;


	public JMo_Swing_Dialog( final Call parent ) {
		super( parent );
	}

	public JMo_Swing_Dialog( final Call parent, final Call title ) {
		super( parent, title );
	}

	public JMo_Swing_Dialog( final Call parent, final Call dx, final Call dy ) {
		super( parent, dx, dy );
	}

	/**
	 * ! Object of a single Swing-Dialog
	 * + Swing_Dialog(Swing_Window parent)
	 * + Swing_Dialog(Swing_Window parent, Str title)
	 * + Swing_Dialog(Swing_Window parent, IntNumber dx, IntNumber dy)
	 * + Swing_Dialog(Swing_Window parent, IntNumber dx, IntNumber dy, Str title)
	 */
	public JMo_Swing_Dialog( final Call parent, final Call dx, final Call dy, final Call title ) {
		super( parent, dx, dy, title );
	}

	public Component getSwing() {
		return this.dialog;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.dialog.addWindowListener( new WindowListener() {

			public void windowActivated( final WindowEvent e ) {}

			public void windowClosed( final WindowEvent e ) {
				JMo_Swing_Dialog.this.setState( STATE.CLOSED );
				JMo_Swing_Dialog.this.eventRun( cr, "@close", Nil.NIL );
			}

			public void windowClosing( final WindowEvent e ) {}

			public void windowDeactivated( final WindowEvent e ) {}

			public void windowDeiconified( final WindowEvent e ) {}

			public void windowIconified( final WindowEvent e ) {}

			public void windowOpened( final WindowEvent e ) {}

		} );
	}

	/**
	 * °@close # On dialog close
	 */
	@Override
	public boolean validateEvent( final String event ) {

		switch( event ) {
			case "@close":
				return true;
		}
		return false;
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °dispose ^ close
	 * °close()Nil # Close this frame.
	 * °+ ^ add
	 * °add(Swing_Object obj)Same # Add a object to this frame.
	 * °setTitle(Str name)Same # Set the title of this frame.
	 */
	@Override
	protected final I_Object callMethod2( final CallRuntime cr, final String method ) {
		if( this.getState() == STATE.CLOSED )
			throw new RuntimeError( cr, "Illegal Frame-Access", "This Dialog is already closed" );

		switch( method ) {
			case "show":
				this.mShow( cr, true );
				return this;
			case "hide":
				this.mShow( cr, false );
				return this;

			default:
				final I_Object res = Lib_Java.mExec( cr, this.dialog, this.dialog.getClass(), method, this ); //cr.parsFlex(null, this, 0)	//TODO prüfen!!!
				if( res != null && res == Nil.NIL )
					return this;

				return null;
		}
	}

	@Override
	protected Window pCreateFrame( final Frame parent ) {
		Err.ifNull( parent );
		return this.dialog = new JDialog( parent, true ); // Dialog is always modal!
	}

	@Override
	protected String pGetTitle() {
		return this.dialog.getTitle();
	}

	@Override
	protected void pReplace( final Component swing ) {
//		final I_Swing_Object[] addObjs = cr.argsVar( this, 1, I_Swing_Object.class );
//	this.frame.add(b.gSwing());
//	for( final I_Swing_Object so : addObjs )
		this.dialog.getContentPane().add( swing );
	}

	@Override
	protected void pSetTitle( final String title ) {
		this.dialog.setTitle( title );
	}

	/**
	 * °show()Same # Show this window.
	 * °hide()Same # Hide this window.
	 */
	private void mShow( final CallRuntime cr, final boolean show ) {
		cr.argsNone();

		if( show && this.getState() != STATE.RUNNING ) {
			this.dialog.setLocationRelativeTo( null ); // Center window
			this.dialog.setDefaultCloseOperation( WindowConstants.DISPOSE_ON_CLOSE );
		}

		this.setState( STATE.RUNNING );

//		SwingUtilities.invokeLater(() -> {	// This conflicts with Modal!!!
		this.dialog.setVisible( show );
//		}
	}

}
