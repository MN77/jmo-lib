/*******************************************************************************
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.control.A_JG_LabelI;
import org.jaymo_lang.lib.gui.control.A_JG_LabelM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.position.Lib_Position;
import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;
import de.mn77.base.data.constant.position.POSITION_V;
import de.mn77.lib.graphic.I_ImageX;
import de.mn77.lib.graphic.MImageX;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_Swing_Label extends A_Swing_JComponent {

	private JLabel              label;
	private final ArgCallBuffer arg0;
	private final ArgCallBuffer arg1;
	private final ArgCallBuffer arg2;

	/**
	 * °setText(Atomic a)Same # Set the text of this label.
	 * °getText()Str # Returns the text of this label.
	 * °setImage(Image img)Same # Set the image of this label.
	 * °getImage()Image # Returns the image of this label.
	 */
	private final A_JG_LabelM methods = new A_JG_LabelM() {

		@Override
		protected I_Object getImage() {
			final Icon icon = JMo_Swing_Label.this.label.getIcon();
			final I_ImageX image = new MImageX(icon); //TODO Testen!!!
			return new JMo_Image(image);
		}

		@Override
		protected String getText() {
			return JMo_Swing_Label.this.label.getText();
		}

		@Override
		protected void setImage( JMo_Image image ) {
			if(image == null)
				JMo_Swing_Label.this.label.setIcon( null );
			else {
				final Icon icon = image.internalGet().getIcon();
				JMo_Swing_Label.this.label.setIcon( icon );
			}
		}

		@Override
		protected void setText( String text ) {
			JMo_Swing_Label.this.label.setText( text );
		}
	};

	private final A_JG_LabelI inits = new A_JG_LabelI() {
		@Override
		protected void arg0str( JMo_Str s ) {
			JMo_Swing_Label.this.label.setText( s.rawString()) ;
		}

		@Override
		protected void arg0image( JMo_Image img ) {
			final I_ImageX imageData = img.internalGet();
			final Icon icon = imageData.getIcon();
			JMo_Swing_Label.this.label.setIcon( icon );
		}

		@Override
		protected void arg1pos( CallRuntime cr, POSITION pos ) {
			if( pos == POSITION.LEFT )
				JMo_Swing_Label.this.label.setAlignmentX( Component.LEFT_ALIGNMENT );
			else if( pos == POSITION.CENTER )
				JMo_Swing_Label.this.label.setAlignmentX( Component.CENTER_ALIGNMENT );
			else if( pos == POSITION.RIGHT )
				JMo_Swing_Label.this.label.setAlignmentX( Component.RIGHT_ALIGNMENT );
			else
				throw new RuntimeError( cr, "Invalid position", "Allowed are left, center right, but got: " + pos );
		}

		@Override
		protected void arg2wrap( boolean wrap ) {
//			if( wrap ) {}
			// This does nothing here and will be ignored!
			// This is only useful for SWT
		}
	};


	/**
	 * +Swing_Label()
	 * +Swing_Label(Str|Image content)
	 * +Swing_Label(Str|Image content, MagicPosition align)
	 * +Swing_Label(Str|Image content, MagicPosition align, Bool wrap)
	 */
	public JMo_Swing_Label() {
		this.arg0 = null;
		this.arg1 = null;
		this.arg2 = null;
	}

	public JMo_Swing_Label( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
		this.arg1 = null;
		this.arg2 = null;
	}

	public JMo_Swing_Label( final Call arg, final Call align ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
		this.arg1 = new ArgCallBuffer( 1, align );
		this.arg2 = null;
	}

	public JMo_Swing_Label( final Call arg, final Call align, final Call wrap ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
		this.arg1 = new ArgCallBuffer( 1, align );
		this.arg2 = new ArgCallBuffer( 2, wrap );
	}


	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		final I_Object result = this.methods.call( cr, method, this );
		if(result !=null)
			return result;

		switch( method ) {
			case "setAlign":
				this.mSetAlign( cr );
				return this;
			case "getAlign":
				cr.argsNone();
				return this.mGetAlign(cr);

//			case "@select":
//				//TO DO Check args
//				this.eventAddBlock("@select", c.gBlock());
//				return new Result_Obj(this, true);

			default:
				return null;
		}
	}

	public JLabel getSwing() {
		return this.label;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.label = new JLabel();
		this.inits.init(cr, this, this.arg0, this.arg1, this.arg2);

//		this.label.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				this.eventRun("@select");
//			}
//		});
	}

	//-----------------------

	@Override
	public boolean validateEvent2( final String event ) {
		return this.inits.validateEvent( event );
	}

	/**
	 * °setAlign(MagicPosition align)Same # Sets the alignment of the text.
	 */
	private void mSetAlign( final CallRuntime cr ) {
		final MagicPosition opos = (MagicPosition)cr.args( this, MagicPosition.class )[0];
		final POSITION pos = opos.get();

		final POSITION_H posh = Lib_Position.getHorizontal( pos );
		final int sposx = posh == POSITION.LEFT ? SwingConstants.LEFT : posh == POSITION.RIGHT ? SwingConstants.RIGHT : posh == POSITION.CENTER ? SwingConstants.CENTER : -1;
		if( sposx < 0 )
			throw new RuntimeError( cr, "Invalid position", "Only left, center and right are allowed. Got: " + posh );

		final POSITION_V posv = Lib_Position.getVertical( pos );
		final int sposy = posv == POSITION.TOP ? SwingConstants.TOP : posv == POSITION.BOTTOM ? SwingConstants.BOTTOM : posv == POSITION.CENTER ? SwingConstants.CENTER : -1;
		if( sposy < 0 )
			throw new RuntimeError( cr, "Invalid position", "Only top, center and bottom are allowed. Got: " + posv );

		this.label.setHorizontalAlignment( sposx );
		this.label.setVerticalAlignment( sposy );
	}

	/**
	 * °getAlign()MagicPosition # Returns the alignment of the content.
	 */
	private MagicPosition mGetAlign( final CallRuntime cr ) {
		cr.argsNone();
		final int alignH = this.label.getHorizontalAlignment();
		final int alignV = this.label.getVerticalAlignment();

		final POSITION_H posH = alignH == SwingConstants.LEFT
			? POSITION_H.LEFT
			: alignH == SwingConstants.RIGHT
				? POSITION_H.RIGHT
				: POSITION_H.CENTER; // TODO Leading/Trailing

		final POSITION_V posV = alignV == SwingConstants.TOP
			? POSITION_V.TOP
			: alignV == SwingConstants.BOTTOM
				? POSITION_V.BOTTOM
				: POSITION_V.CENTER;

		final POSITION pos = Lib_Position.combine( posH, posV );
		return new MagicPosition(pos);
	}

}
