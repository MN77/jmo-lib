/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.SwingConstants;

import org.jaymo_lang.lib.gui.control.A_JG_SliderI;
import org.jaymo_lang.lib.gui.control.A_JG_SliderM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.AXIS;


/**
 * @author Michael Nitsche
 * @created 17.10.2020
 */
public class JMo_Swing_Slider extends A_Swing_JComponent {

	private JScrollBar          slider;
	private final ArgCallBuffer arg0;

	/**
	 * °setRange(Range range)Same # Set minimum and maximum
	 * °setRange(Int min, Int max)Same # Set minimum and maximum
	 */
	private final A_JG_SliderM methods = new A_JG_SliderM() {

		@Override
		protected void setRange( final int min, final int max ) {
			JMo_Swing_Slider.this.slider.setMinimum( min );
			JMo_Swing_Slider.this.slider.setMaximum( max );
			JMo_Swing_Slider.this.slider.setValue( min );
		}

		@Override
		protected void setValue( final int value ) {
			JMo_Swing_Slider.this.slider.setValue( value );
		}

		@Override
		protected int getMinimum() {
			return JMo_Swing_Slider.this.slider.getMinimum();
		}

		@Override
		protected int getMaximum() {
			return JMo_Swing_Slider.this.slider.getMaximum();
		}

		@Override
		protected int getValue() {
			return JMo_Swing_Slider.this.slider.getValue();
		}
	};

	private final A_JG_SliderI inits = new A_JG_SliderI() {

		@Override
		protected void arg0align( AXIS align ) {
			final int arg = align == AXIS.X
				? SwingConstants.HORIZONTAL
				: SwingConstants.VERTICAL;

			JMo_Swing_Slider.this.slider = new JScrollBar( arg );
		}

		@Override
		protected void atChange( CallRuntime cr, String ev ) {
			JMo_Swing_Slider.this.slider.addPropertyChangeListener( e -> JMo_Swing_Slider.this.eventRun( cr, ev, JMo_Swing_Slider.this ) );
		}
	};

	/**
	 * +Swing_Scale(MagicAxis xy)
	 * TODO evtl. Scale(MagicAxis, min, max)
	 */
	public JMo_Swing_Slider( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
	}


	/**
	 * °getValue()Int # Returns the value of this slider.
	 * °setValue(IntNumber num)Same # Set the value of this slider.
	 * °setMin ^ setMinimum
	 * °setMinimum(IntNumber num)Same # Set the minimum value.
	 * °setMax ^ setMaximum
	 * °setMaximum(IntNumber num)Same # Set the maximum value.
	 */
	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JComponent getSwing() {
		return this.slider;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.inits.init(cr, this, this.arg0);
	}

//	@Override
//	protected String resolveDefault() {
//		return "@changed";
//	}

	/**
	 * °@change # Executed on a change.
	 */
	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

}
