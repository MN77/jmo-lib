/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import javax.swing.Icon;
import javax.swing.JTabbedPane;

import org.jaymo_lang.lib.gui.control.A_JG_TabbedPaneI;
import org.jaymo_lang.lib.gui.control.A_JG_TabbedPaneM;
import org.jaymo_lang.lib.swing.A_Swing_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 03.05.2021
 */
public class JMo_Swing_TabbedPane extends A_Swing_JComponent {

	private JTabbedPane pane;

	private final A_JG_TabbedPaneM methods = new A_JG_TabbedPaneM() {

		@Override
		protected void addTab( CallRuntime cr, String title, Icon icon, I_Object content, String tooltip ) {
			final A_Swing_Object contentSwing = cr.argType( content, A_Swing_Object.class );
			JMo_Swing_TabbedPane.this.pane.addTab( title, icon, contentSwing.getSwing(), tooltip );
//			this.pane.setMnemonicAt(0, KeyEvent.VK_1);
		}
	};

	private final A_JG_TabbedPaneI inits = new A_JG_TabbedPaneI() {
	};


	public JMo_Swing_TabbedPane() {}

	/**
	 * °add(Str title, Object content)Same # Add an element to a new tab.
	 * °add(Str title, Object content, Image icon)Same # Add an element to a new tab and set the tab icon.
	 * °add(Str title, Object content, Image icon, Atomic key)Same # Add an element to a new tab, set the tab icon and link a key with this tab.
	 */
	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JTabbedPane getSwing() {
		return this.pane;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.pane = new JTabbedPane();
		this.inits.init(cr, null);
//		this.pane.addActionListener(e -> JMo_Swing_TabbedPane.this.eventRun(cr, "@select", this));
	}

//	@Override
//	protected String resolveDefault() {
//		return "@change";
//	}

	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

}
