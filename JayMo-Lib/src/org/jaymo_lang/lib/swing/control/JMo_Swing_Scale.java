/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import javax.swing.JComponent;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

import org.jaymo_lang.lib.gui.control.A_JG_ScaleI;
import org.jaymo_lang.lib.gui.control.A_JG_ScaleM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.AXIS;


/**
 * @author Michael Nitsche
 * @created 17.10.2020
 */
public class JMo_Swing_Scale extends A_Swing_JComponent {

	private JSlider             slider;
	private final ArgCallBuffer arg0;

	/**
	 * °setRange(Range range)Same # Set minimum and maximum
	 * °setRange(Int min, Int max)Same # Set minimum and maximum
	 * °getValue()Int # Returns the value of this slider.
	 * °setValue(IntNumber num)Same # Set the value of this slider.
	 * °setMin ^ setMinimum
	 * °setMinimum(IntNumber num)Same # Set the minimum value.
	 * °setMax ^ setMaximum
	 * °setMaximum(IntNumber num)Same # Set the maximum value.
	 */
	private final A_JG_ScaleM methods = new A_JG_ScaleM() {

		@Override
		protected void setRange( final int min, final int max ) {
			JMo_Swing_Scale.this.slider.setMinimum( min );
			JMo_Swing_Scale.this.slider.setMaximum( max );
			JMo_Swing_Scale.this.slider.setValue( min );
		}

		@Override
		protected void setValue( final int value ) {
			JMo_Swing_Scale.this.slider.setValue( value );
		}

		@Override
		protected int getMinimum() {
			return JMo_Swing_Scale.this.slider.getMinimum();
		}

		@Override
		protected int getMaximum() {
			return JMo_Swing_Scale.this.slider.getMaximum();
		}

		@Override
		protected int getValue() {
			return JMo_Swing_Scale.this.slider.getValue();
		}
	};

	private final A_JG_ScaleI inits = new A_JG_ScaleI() {

		@Override
		protected void arg0align( AXIS align ) {
			final int arg = align == AXIS.X
				? SwingConstants.HORIZONTAL
				: SwingConstants.VERTICAL;

			JMo_Swing_Scale.this.slider = new JSlider( arg );
		}

		@Override
		protected void atChange( CallRuntime cr, String ev ) {
			JMo_Swing_Scale.this.slider.addChangeListener( e -> JMo_Swing_Scale.this.eventRun( cr, ev, JMo_Swing_Scale.this ) );
		}
	};

	/**
	 * +Swing_Slider(MagicAxis xy)
	 * TODO evtl. Slider(MagicAxis, min, max)
	 */
	public JMo_Swing_Scale( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
	}


	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JComponent getSwing() {
		return this.slider;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.inits.init(cr, this, this.arg0);
	}

//	@Override
//	protected String resolveDefault() {
//		return "@changed";
//	}

	/**
	 * °@change # Executed on a change.
	 */
	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

}
