/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import org.jaymo_lang.lib.gui.control.A_JG_ProgressBarI;
import org.jaymo_lang.lib.gui.control.A_JG_ProgressBarM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.AXIS;


/**
 * @author Michael Nitsche
 * @created 17.10.2020
 */
public class JMo_Swing_ProgressBar extends A_Swing_JComponent {

	private JProgressBar        bar;
	private final ArgCallBuffer arg0;


	/*
	 * °setRange(Range range)Same # Set minimum and maximum
	 * °setRange(Int min, Int max)Same # Set minimum and maximum
	 * °setValue(IntNumber num)Same # Set the value of the progress bar.
	 * °getMin()Int # Returns the current value.
	 * °getMax()Int # Returns the current value.
	 * °getValue()Int # Returns the current value.
	 * °getRange()Range # Returns the current range from min to max.
	 * °next ^ inc
	 * °inc()Same # Push the value one step forward
	 * °hasNext()Bool # Returns true, if value lower than max.
	 */
	private final A_JG_ProgressBarM methods = new A_JG_ProgressBarM() {

		@Override
		protected void setRange( final int min, final int max ) {
			JMo_Swing_ProgressBar.this.bar.setMinimum( min );
			JMo_Swing_ProgressBar.this.bar.setMaximum( max );
			JMo_Swing_ProgressBar.this.bar.setValue( min );
		}

		@Override
		protected void setValue( int value ) {
			JMo_Swing_ProgressBar.this.bar.setValue( value );
		}

		@Override
		protected int getMinimum() {
			return JMo_Swing_ProgressBar.this.bar.getMinimum();
		}

		@Override
		protected int getMaximum() {
			return JMo_Swing_ProgressBar.this.bar.getMaximum();
		}

		@Override
		protected int getValue() {
			return JMo_Swing_ProgressBar.this.bar.getValue();
		}
	};

	private final A_JG_ProgressBarI inits = new A_JG_ProgressBarI() {
		@Override
		protected void arg0align( AXIS align ) {
			final int arg = align == AXIS.X
				? SwingConstants.HORIZONTAL
				: SwingConstants.VERTICAL;
			JMo_Swing_ProgressBar.this.bar = new JProgressBar( arg );
		}
	};


	/**
	 * +Swing_ProgressBar(MagicAxis xy)
	 *
	 * @implNote: evtl. ProgressBar(MagicAxis, min, max)
	 */
	public JMo_Swing_ProgressBar( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
	}

	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JComponent getSwing() {
		return this.bar;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.inits.init(cr, this, this.arg0);
	}

	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

}
