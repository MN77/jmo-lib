/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import javax.swing.text.JTextComponent;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 06.03.2021
 */
public abstract class A_Swing_JTextComponent extends A_Swing_JComponent {

	/**
	 * °set ^ setText
	 * °setText(Str text)Same # Set the text of this component.
	 * °get ^ getText
	 * °getText()Str # Get the text of this component.
	 * °setEditable(Bool b)Same # Make this component editable or read only.
	 */
	@Override
	public final I_Object callMethod2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "set":
			case "setText":
				final JMo_Str s = (JMo_Str)cr.args( this, JMo_Str.class )[0];
				//Lib_Konvert
				//	      button.setForeground(Color.blue);
				((JTextComponent)this.getSwing()).setText( s.rawString() );
				return this;

			case "get":
			case "getText":
				cr.argsNone();
				final String text = ((JTextComponent)this.getSwing()).getText();
				return new JMo_Str( text );

			case "setEditable": // TextComponent
				final boolean b = Lib_Convert.toBoolean( cr, cr.args( this, JMo_Bool.class )[0] );
				((JTextComponent)this.getSwing()).setEditable( b );
				return this;

			default:
				return this.callMethod3( cr, method );
		}
	}

	public abstract I_Object callMethod3( final CallRuntime cr, String method );

}
