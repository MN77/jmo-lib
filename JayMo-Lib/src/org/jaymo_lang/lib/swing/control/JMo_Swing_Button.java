/*******************************************************************************
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import javax.swing.Icon;
import javax.swing.JButton;

import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.control.A_JG_ButtonI;
import org.jaymo_lang.lib.gui.control.A_JG_ButtonM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.error.Err;

/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_Swing_Button extends A_Swing_JComponent {

	private JButton       button;
	private ArgCallBuffer arg0;

	private final A_JG_ButtonM methods = new A_JG_ButtonM() {
		@Override
		public I_Object getImage() {
			throw Err.todo(  );
		}

		@Override
		public String getText() {
			return JMo_Swing_Button.this.button.getText();
		}

		@Override
		public void setImage( JMo_Image image ) {
			final Icon icon = image == null
				? null
				: image.internalGet().getIcon();
			JMo_Swing_Button.this.button.setIcon( icon );
		}

		@Override
		public void setText( String text ) {
			JMo_Swing_Button.this.button.setText( text );
		}
	};

	private final A_JG_ButtonI inits = new A_JG_ButtonI() {
		@Override
		protected void atSelect(CallRuntime cr, String ev) {
			JMo_Swing_Button.this.button.addActionListener( e -> JMo_Swing_Button.this.eventRun( cr, ev, JMo_Swing_Button.this ) );
		}

		@Override
		protected void arg0str( JMo_Str s ) {
			JMo_Swing_Button.this.button.setText( s.rawString() );
		}

		@Override
		protected void arg0image( JMo_Image s ) {
			final Icon icon = s.internalGet().getIcon();
			JMo_Swing_Button.this.button.setIcon( icon );
		}
	};


	public JMo_Swing_Button() {
		this.arg0 = null;
	}

	/**
	 * +Swing_Button()
	 * +Swing_Button(Str text)
	 * +Swing_Button(Image img)
	 */
	public JMo_Swing_Button( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
	}


	/**
	 * °setText(Atomic a)Same # Set the text of this button.
	 * °getText()Str # Returns the text of this button.
	 * °setImage(Image img)Same # Set the icon image of this button.
	 */
	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JButton getSwing() {
		return this.button;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.button = new JButton();
		this.inits.init(cr, this, this.arg0);
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {

		switch( type ) {
			case NESTED:
			case IDENT:
				return this.getTypeName();
			default:
				final String text = this.button == null ? "" : this.button.getText();
				return this.toString() + "(\"" + text + "\")";
		}
	}

	/**
	 * °@select # Executed on selection.
	 */
	@Override
	public boolean validateEvent2( final String event ) {
		return this.inits.validateEvent( event );
	}

//	@Override
//	protected String resolveDefault() {
//		return "@select";
//	}

}
