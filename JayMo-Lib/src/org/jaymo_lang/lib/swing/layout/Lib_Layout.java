/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.layout;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 09.01.2021
 */
public class Lib_Layout {

	/**
	 * int[4] = top, right, bottom, left
	 */
	public static int[] border( final CallRuntime cr, final I_Object[] args ) {
		int top = 0;
		int left = 0;
		int right = 0;
		int bottom = 0;

		switch( args.length ) {
			case 1:
				top = Lib_Convert.toInt( cr, cr.argType( args[0], I_IntNumber.class ) );
				left = top;
				right = top;
				bottom = top;
				break;
			case 2:
				top = Lib_Convert.toInt( cr, cr.argType( args[0], I_IntNumber.class ) );
				left = Lib_Convert.toInt( cr, cr.argType( args[1], I_IntNumber.class ) );
				right = left;
				bottom = top;
				break;
			case 3:
				top = Lib_Convert.toInt( cr, cr.argType( args[0], I_IntNumber.class ) );
				left = Lib_Convert.toInt( cr, cr.argType( args[1], I_IntNumber.class ) );
				right = left;
				bottom = Lib_Convert.toInt( cr, cr.argType( args[2], I_IntNumber.class ) );
				break;
			case 4:
				top = Lib_Convert.toInt( cr, cr.argType( args[0], I_IntNumber.class ) );
				right = Lib_Convert.toInt( cr, cr.argType( args[1], I_IntNumber.class ) );
				bottom = Lib_Convert.toInt( cr, cr.argType( args[2], I_IntNumber.class ) );
				left = Lib_Convert.toInt( cr, cr.argType( args[3], I_IntNumber.class ) );
				break;
		}

		return new int[]{ top, right, bottom, left };
	}

}
