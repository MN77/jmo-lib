/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.type;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.VarArgsCallBuffer;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;


/**
 * Represents a Uniform Resource Identifier (URI) reference.
 *
 * @author Michael Nitsche
 * @created 26.11.2022
 * @implNote
 *           https://de.wikipedia.org/wiki/Uniform_Resource_Identifier
 *           TODO: Set Nil to delete a specific part
 */
public class JMo_Uri extends A_Immutable {

	private final VarArgsCallBuffer args;
	private URI                     uri;


	/*
	 * +Uri(Str uri)
	 * +Uri(Str scheme, Str host, Str path, Str fragment)
	 * +Uri(Str scheme, Str authority, Str path, Str query, Str fragment)
	 * +Uri(Str scheme, Str userInfo, Str host, IntNumber port, Str path, Str query, Str fragment)
	 */
	public JMo_Uri( final Call... ca ) {
		this.args = new VarArgsCallBuffer( ca );
	}

	public JMo_Uri( final URI newURI ) {
		this.uri = newURI;
		this.args = null;
	}

	@Override
	public boolean equals( final Object other ) {
		return other instanceof JMo_Uri
			? ((JMo_Uri)other).uri.equals( this.uri )
			: false;
	}

	@Override
	public boolean equalsLazy( final Object other ) {
		return this.equals( other );
	}

	@Override
	public void init( final CallRuntime cr ) {
		if( this.args != null )
			try {
				final I_Object[] args = this.args.init( cr, this );

				switch( args.length ) {
					case 1:
						final String s1 = cr.argType( args[0], JMo_Str.class ).rawString();
						this.uri = new URI( s1 );
						break;
					case 4:
						final String s4a = cr.argType( args[0], JMo_Str.class ).rawString();
						final String s4b = cr.argType( args[1], JMo_Str.class ).rawString();
						final String s4c = cr.argType( args[2], JMo_Str.class ).rawString();
						final String s4d = cr.argType( args[3], JMo_Str.class ).rawString();
						this.uri = new URI( s4a, s4b, s4c, s4d );
						break;
					case 5:
						final String s5a = cr.argType( args[0], JMo_Str.class ).rawString();
						final String s5b = cr.argType( args[1], JMo_Str.class ).rawString();
						final String s5c = cr.argType( args[2], JMo_Str.class ).rawString();
						final String s5d = cr.argType( args[3], JMo_Str.class ).rawString();
						final String s5e = cr.argType( args[4], JMo_Str.class ).rawString();
						this.uri = new URI( s5a, s5b, s5c, s5d, s5e );
						break;
					case 7:
						final String s7a = cr.argType( args[0], JMo_Str.class ).rawString();
						final String s7b = cr.argType( args[1], JMo_Str.class ).rawString();
						final String s7c = cr.argType( args[2], JMo_Str.class ).rawString();
						final int s7d = Lib_Convert.toInt( cr, cr.argType( args[3], I_IntNumber.class ) );
						final String s7e = cr.argType( args[4], JMo_Str.class ).rawString();
						final String s7f = cr.argType( args[5], JMo_Str.class ).rawString();
						final String s7g = cr.argType( args[6], JMo_Str.class ).rawString();
						this.uri = new URI( s7a, s7b, s7c, s7d, s7e, s7f, s7g );
						break;
					default:
						throw new CodeError( cr, "Invalid amount of arguments for 'Uri'", "Got " + args.length + ", allowed are 1,4,5,7" );
				}
			}
			catch( final URISyntaxException e ) {
				throw new RuntimeError( cr, "Invalid URI", e.getMessage() );
			}
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		final StringBuilder sb = new StringBuilder();
		sb.append( "Uri" );

		switch( type ) {
			case REGULAR:
			case DESCRIBE:
				sb.append( "(\"" );
				sb.append( this.uri.toString() );
				sb.append( "\")" );
				break;
			case NESTED:
			case IDENT:
		}

		return sb.toString();
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "toFile":
				return this.mToFile( cr );
//			case "toASCIIString":
//			case "toUrl":

			case "getScheme":
				return this.mGetStr( cr, this.uri.getScheme() );
			case "getAuthority":
				return this.mGetStr( cr, this.uri.getAuthority() );
			case "getPath":
				return this.mGetStr( cr, this.uri.getPath() );
			case "getQuery":
				return this.mGetStr( cr, this.uri.getQuery() );
			case "getFragment":
				return this.mGetStr( cr, this.uri.getFragment() );
			case "getHost":
				return this.mGetStr( cr, this.uri.getHost() );
			case "getPort":
				return this.mGetInt( cr, this.uri.getPort() );
			case "getUserInfo":
				return this.mGetStr( cr, this.uri.getUserInfo() );

			case "setScheme":
				return this.mSetScheme( cr );
			case "setAuthority":
				return this.mSetAuthority( cr );
			case "setPath":
				return this.mSetPath( cr );
			case "setQuery":
				return this.mSetQuery( cr );
			case "setFragment":
				return this.mSetFragment( cr );
			case "setHost":
				return this.mSetHost( cr );
			case "setPort":
				return this.mSetPort( cr );
			case "setUserInfo":
				return this.mSetUserInfo( cr );
		}

		return null;
	}

	/*
	 * °getPort()Int? # Returns the port of this URI.
	 */
	private I_Object mGetInt( final CallRuntime cr, final int i ) {
		cr.argsNone();
		return i < 0
			? Nil.NIL
			: new JMo_Int( i );
	}

	/*
	 * °getScheme()Str? # Returns the scheme of this URI.
	 * °getAuthority()Str? # Returns the authority of this URI.
	 * °getPath()Str? # Returns the path of this URI.
	 * °getQuery()Str? # Returns the query of this URI.
	 * °getFragment()Str? # Returns the fragment of this URI.
	 * °getHost()Str? # Returns the host of this URI.
	 * °getPort()Str? # Returns the port of this URI.
	 * °getUserInfo()Str? # Returns the user info of this URI.
	 */
	private I_Object mGetStr( final CallRuntime cr, final String s ) {
		cr.argsNone();
		return s == null
			? Nil.NIL
			: new JMo_Str( s );
	}

	/*
	 * °setAuthority(Str s)Uri # Create a new URI where this authority is set.
	 */
	private I_Object mSetAuthority( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];

		try {
			final URI newURI = new URI( this.uri.getScheme(), arg.rawString(), this.uri.getRawPath(), this.uri.getRawQuery(), this.uri.getRawFragment() );
			return new JMo_Uri( newURI );
		}
		catch( final URISyntaxException e ) {
			throw new RuntimeError( cr, "Invalid URI syntax", e.getMessage() );
		}
	}

	/*
	 * °setFragment(Str s)Uri # Create a new URI where this fragment is set.
	 */
	private I_Object mSetFragment( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];

		try {
			final URI newURI = new URI( this.uri.getScheme(), this.uri.getRawUserInfo(), this.uri.getHost(), this.uri.getPort(), this.uri.getRawPath(), this.uri.getRawQuery(), arg.rawString() );
			return new JMo_Uri( newURI );
		}
		catch( final URISyntaxException e ) {
			throw new RuntimeError( cr, "Invalid URI syntax", e.getMessage() );
		}
	}

	/*
	 * °setHost(Str s)Uri # Create a new URI where this host is set.
	 */
	private I_Object mSetHost( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];

		try {
			final URI newURI = new URI( this.uri.getScheme(), this.uri.getRawUserInfo(), arg.rawString(), this.uri.getPort(), this.uri.getRawPath(), this.uri.getRawQuery(), this.uri.getRawFragment() );
			return new JMo_Uri( newURI );
		}
		catch( final URISyntaxException e ) {
			throw new RuntimeError( cr, "Invalid URI syntax", e.getMessage() );
		}
	}

	/*
	 * °setPath(Str s)Uri # Create a new URI where this path is set.
	 */
	private I_Object mSetPath( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];

		try {
			final URI newURI = new URI( this.uri.getScheme(), this.uri.getRawUserInfo(), this.uri.getHost(), this.uri.getPort(), arg.rawString(), this.uri.getRawQuery(), this.uri.getRawFragment() );
			return new JMo_Uri( newURI );
		}
		catch( final URISyntaxException e ) {
			throw new RuntimeError( cr, "Invalid URI syntax", e.getMessage() );
		}
	}

	/*
	 * °setPort(IntNumber port)Uri # Create a new URI where this port is set.
	 */
	private I_Object mSetPort( final CallRuntime cr ) {
		final I_IntNumber arg = (I_IntNumber)cr.args( this, I_IntNumber.class )[0];
		final int port = Lib_Convert.toInt( cr, arg );
		Lib_Error.ifNotBetween( cr, 0, 65535, port, "Port" );

		try {
			final URI newURI = new URI( this.uri.getScheme(), this.uri.getRawUserInfo(), this.uri.getHost(), port, this.uri.getRawPath(), this.uri.getRawQuery(), this.uri.getRawFragment() );
			return new JMo_Uri( newURI );
		}
		catch( final URISyntaxException e ) {
			throw new RuntimeError( cr, "Invalid URI syntax", e.getMessage() );
		}
	}

	/*
	 * °setQuery(Str s)Uri # Create a new URI where this query is set.
	 */
	private I_Object mSetQuery( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];

		try {
			final URI newURI = new URI( this.uri.getScheme(), this.uri.getRawUserInfo(), this.uri.getHost(), this.uri.getPort(), this.uri.getRawPath(), arg.rawString(), this.uri.getRawFragment() );
			return new JMo_Uri( newURI );
		}
		catch( final URISyntaxException e ) {
			throw new RuntimeError( cr, "Invalid URI syntax", e.getMessage() );
		}
	}

	/*
	 * °setScheme(Str s)Uri # Create a new URI where this scheme is set.
	 */
	private JMo_Uri mSetScheme( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];

		try {
			final URI newURI = new URI( arg.rawString(), this.uri.getRawUserInfo(), this.uri.getHost(), this.uri.getPort(), this.uri.getRawPath(), this.uri.getRawQuery(), this.uri.getRawFragment() );
			return new JMo_Uri( newURI );
		}
		catch( final URISyntaxException e ) {
			throw new RuntimeError( cr, "Invalid URI syntax", e.getMessage() );
		}
	}

	/**
	 * °setUserInfo(Str s)Uri # Create a new URI where this user info is set.
	 * °setUserInfo(Str user, Str pass)Uri # Create a new URI where this user info is set.
	 */
	private I_Object mSetUserInfo( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final String arg = args.length == 1
			? cr.argType( args[0], JMo_Str.class ).rawString()
			: cr.argType( args[0], JMo_Str.class ).rawString() + ':' + cr.argType( args[1], JMo_Str.class ).rawString();

		try {
			final URI newURI = new URI( this.uri.getScheme(), arg, this.uri.getHost(), this.uri.getPort(), this.uri.getRawPath(), this.uri.getRawQuery(), this.uri.getRawFragment() );
			return new JMo_Uri( newURI );
		}
		catch( final URISyntaxException e ) {
			throw new RuntimeError( cr, "Invalid URI syntax", e.getMessage() );
		}
	}

	/*
	 * °toFile()File # Create a File object from this URI.
	 */
	private I_Object mToFile( final CallRuntime cr ) {
		cr.argsNone();

		try {
			final File f = new File( this.uri );
			return new JMo_File( f );
		}
		catch( final IllegalArgumentException e ) {
			throw new RuntimeError( cr, "Illegal convertion from URI to File", e.getMessage() );
		}
	}

}
