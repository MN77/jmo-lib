/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.fuzzy;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.lib.fuzzy.Fuzzy;


/**
 * @author Michael Nitsche
 * @created 14.03.2021
 */
public class JMo_Fuzzy extends A_ObjectSimple {

	private final Fuzzy fuzzy = new Fuzzy();


	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "calc":
				return this.iCalculate( cr, false );
			case "calcLimited":
				return this.iCalculate( cr, true );
		}
		return null;
	}

	/**
	 * °calc(Str value1, Str value2)Dec # Compares to strings.
	 * °calcLimited(Str value1, Str value2)Dec # Compares to strings, but only up to the length of the first string.
	 */
	private I_Object iCalculate( final CallRuntime cr, final boolean limitToS1 ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class );
		final String s1 = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String s2 = Lib_Convert.toStr( cr, args[1] ).rawString();
		return JMo_Dec.valueOf( cr, this.fuzzy.compare( s1, s2, limitToS1 ) );
	}

}
