/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control;

import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 14.05.2024
 */
public abstract class A_JG_TextAreaM implements I_JG_Methods {

	@Override
	public final I_Object call( final CallRuntime cr, final String method, final I_Object obj ) {

		switch( method ) {
			case "set":
			case "setText":
				final JMo_Str s = (JMo_Str)cr.args( obj, JMo_Str.class )[0];
				this.setText( s.rawString() );
				return obj;

			case "get":
			case "getText":
//			case "text":
				cr.argsNone();
				return new JMo_Str( this.getText() );

			case "append":
				final JMo_Str s2 = cr.arg( obj, JMo_Str.class );
				this.append( s2.rawString() );
				return obj;

			case "setWrap":
			case "setWrapLines":
				JMo_Bool doWrap = cr.arg( obj, JMo_Bool.class );
				this.setWrapLines( doWrap.rawBoolean() );
				return obj;

//			case "@select":
//				//TODO Check args
//				this.eventAddBlock("@select", c.gBlock());
//				return new Result_Obj(this, true);
		}

		return null;
	}

	protected abstract void append( String text );
	protected abstract String getText();
	protected abstract void setText( String text );
	protected abstract void setWrapLines( boolean enabled );

}
