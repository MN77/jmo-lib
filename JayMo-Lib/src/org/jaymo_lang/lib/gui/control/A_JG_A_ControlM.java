/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.lib.font.JMo_Font;
import org.jaymo_lang.lib.graphic.A_ColorModel;
import org.jaymo_lang.lib.graphic.JMo_Color;
import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 14.11.2023
 */
public abstract class A_JG_A_ControlM implements I_JG_Methods {

	protected abstract void border();
	protected abstract void focus();
	protected abstract int[] getBackground();
	protected abstract int[] getForeground();
	protected abstract boolean hasFocus();
	protected abstract boolean isEnabled();
	protected abstract boolean isMouseOver();
	protected abstract boolean isVisible();
	protected abstract void setBackgroundNone( CallRuntime cr);
	protected abstract void setBackgroundColor( CallRuntime cr, A_ColorModel color );
	protected abstract void setBackgroundRGB( CallRuntime cr, int[] rgb );
	protected abstract void setEnabled( boolean b );
	protected abstract void setFont( CallRuntime cr, JMo_Font font );
	protected abstract void setForegroundNone( CallRuntime cr);
	protected abstract void setForegroundColor( CallRuntime cr, A_ColorModel color );
	protected abstract void setForegroundRGB( CallRuntime cr, int[] rgb );
	protected abstract void setToolTip( CallRuntime cr, String toolTip );
	protected abstract void setVisible( boolean b );


	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object obj ) {
		switch( method ) {
			case "setEnabled":
				final boolean enable = cr.arg( obj, JMo_Bool.class ).rawBoolean();
				this.setEnabled( enable );
				return obj;
			case "setVisible":
				final boolean visible = cr.arg( obj, JMo_Bool.class ).rawBoolean();
				this.setVisible( visible );
				return obj;
			case "focus":
				cr.argsNone();
				this.focus();
				return obj;
			case "hasFocus":
				cr.argsNone();
				return JMo_Bool.getObject( this.hasFocus() );

			case "border":
				cr.argsNone();
				this.border();
				return obj;
			case "setToolTip":
			case "setTip":
				final String toolTip = cr.arg( obj, JMo_Str.class ).rawString();
				this.setToolTip( cr, toolTip );
				return obj;

			case "isEnabled":
				cr.argsNone();
				return JMo_Bool.getObject( this.isEnabled() );
			case "isVisible":
				cr.argsNone();
				return JMo_Bool.getObject( this.isVisible() );
			case "setBackground":
//				final I_Object[] argsBg = cr.argsFlex( obj, 1, 3 ); // Nil, Color, RGB
				switch(cr.argCount()) {
					case 1:
						final I_Object arg = cr.argExt( obj, A_ColorModel.class, Nil.class );
						if(arg == Nil.NIL)
							this.setBackgroundNone( cr );
						else
							this.setBackgroundColor( cr, (A_ColorModel)arg );
						break;
					case 3:
						final I_Object[] args = cr.args( obj, JMo_Int.class, JMo_Int.class, JMo_Int.class );
						final int[] rgb = new int[]{ ((JMo_Int)args[0]).rawInt( cr ), ((JMo_Int)args[1]).rawInt( cr ), ((JMo_Int)args[2]).rawInt( cr ) };
						this.setBackgroundRGB( cr, rgb );
						break;
					default:
						throw new CodeError( cr, "Invalid amount of arguments", "Need 1 or 3, but got " + cr.argCount() + " for: " + method ); // TODO Zentral in Lib_Error?
				}
				return obj;
			case "setForeground":
//				final I_Object[] argsFg = cr.argsFlex( obj, 1, 3 );
				switch(cr.argCount()) {
					case 1:
						final I_Object arg = cr.argsExt( obj, new Class[]{ A_ColorModel.class, Nil.class } )[0];
						if(arg == Nil.NIL)
							this.setForegroundNone( cr );
						else
							this.setForegroundColor( cr, (A_ColorModel)arg );
						break;
					case 3:
						final I_Object[] args = cr.args( obj, JMo_Int.class, JMo_Int.class, JMo_Int.class );
						final int[] rgb = new int[]{ ((JMo_Int)args[0]).rawInt( cr ), ((JMo_Int)args[1]).rawInt( cr ), ((JMo_Int)args[2]).rawInt( cr ) };
						this.setForegroundRGB( cr, rgb );
						break;
					default:
						throw new CodeError( cr, "Invalid amount of arguments", "Need 1 or 3, but got " + cr.argCount() + " for: " + method ); // TODO Zentral in Lib_Error?
				}
				return obj;
			case "getBackground":
				cr.argsNone();
				final int[] bgRGB = this.getBackground();
				return new JMo_Color( bgRGB[0], bgRGB[1], bgRGB[2] );
			case "getForeground":
				cr.argsNone();
				final int[] fgRGB = this.getForeground();
				return new JMo_Color( fgRGB[0], fgRGB[1], fgRGB[2] );
			case "isMouseOver":
				cr.argsNone();
				return JMo_Bool.getObject( this.isMouseOver() );
			case "setFont":
				final JMo_Font font = cr.arg( obj, JMo_Font.class );
				this.setFont( cr, font );
				return obj;
		}

		return null;
	}

}
