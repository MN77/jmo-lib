/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control;

import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.I_JG_Init;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 14.05.2024
 */
public abstract class A_JG_ToggleButtonI implements I_JG_Init {

	@Override
	public final void init( final CallRuntime cr, final I_Object that, final ArgCallBuffer... args ) {
		Err.ifNot( args.length == 2 );

		if( args[0] != null ) {
			final I_Object arg = args[0].initExt( cr, that, JMo_Str.class, JMo_Image.class );

			if( arg instanceof JMo_Str ) {
				final JMo_Str s = Lib_Convert.toStr( cr, arg );
				this.arg0str( s );
			}
			else {
				this.argImageNorm( (JMo_Image)arg );

//				I_ImageX pressed = image.copy();
//				pressed.brightness(30);

				if( args[1] != null ) {
					final JMo_Image arg2 = (JMo_Image)args[1].initExt( cr, that, JMo_Image.class );
					this.argImagePress( arg2 );
				}
			}
		}

		this.atSelect( cr, "@select" );
	}

	public final boolean validateEvent( final String event ) {

		switch( event ) {
			case "@select":
				return true;
			default:
				return false;
		}
	}

	protected abstract void arg0str( JMo_Str s );
	protected abstract void argImageNorm( JMo_Image img );
	protected abstract void argImagePress( JMo_Image img );
	protected abstract void atSelect( CallRuntime cr, String event );

}
