/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control;

import javax.swing.Icon;

import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 14.05.2024
 */
public abstract class A_JG_TabbedPaneM implements I_JG_Methods {

	@Override
	public final I_Object call( final CallRuntime cr, final String method, final I_Object obj ) {

		switch( method ) {
			case "add":
				final I_Object[] args = cr.argsFlex( obj, 2, 4 ); // title, content, icon, tooltip // TODO 5=key
				final String title = Lib_Convert.toStr( cr, args[0] ).rawString();
//				final A_Swing_Object content = cr.argType( args[1], A_Swing_Object.class );
				final I_Object content = args[1];

				Icon icon = null;

				if( args.length >= 3 ) {
					final I_Object argImage = cr.argTypeExt( args[2], new Class<?>[]{ JMo_Image.class, Nil.class } );
					if( argImage != Nil.NIL )
						icon = ((JMo_Image)argImage).internalGet().getIcon();
				}

				String tooltip = null;
				if( args.length >= 4 )
					tooltip = Lib_Convert.toStr( cr, cr.argType( args[3], I_Atomic.class ) ).rawString();

				this.addTab( cr, title, icon, content, tooltip );
				return obj; // TODO Maybe return handle to the Tab-Object
		}

		return null;
	}


	protected abstract void addTab( CallRuntime cr, String title, Icon icon, I_Object content, String tooltip );

}
