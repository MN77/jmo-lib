/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll;

import org.jaymo_lang.lib.gui.I_JG_Init;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;

/**
 * @author	Michael Nitsche
 * @created	24.10.2023
 */
public abstract class A_JG_TextFieldI implements I_JG_Init {

	protected abstract void arg0str(JMo_Str s);

	protected abstract void atChange( CallRuntime cr, String event );
	protected abstract void atExec( CallRuntime cr, String event );


	@Override
	public final void init( CallRuntime cr, I_Object that, ArgCallBuffer... args) {
		Err.ifNot( args.length == 1 );

		if( args[0] != null ) {
			final JMo_Str oStr = args[0].init( cr, that, JMo_Str.class );
			this.arg0str(oStr);
		}

		this.atChange(cr, "@change");
		this.atExec(cr, "@exec");
	}

	public final boolean validateEvent( final String event ) {

		switch( event ) {
			case "@change":
			case "@exec":
				return true;
			default:
//				return this.inits.validateEvent( event );
				return false;
		}
	}

}
