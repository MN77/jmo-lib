/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll.composit;

import java.util.List;

import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 25.12.2023
 */
public abstract class A_JG_ComboBoxM implements I_JG_Methods {

	public abstract String getValue();
	public abstract void setItems( String[] items );
	public abstract void clear();
	public abstract void setValue( String s );
	public abstract void add( String[] items );


	/**
	 * °get ^ getValue
	 * °getValue()Str # Returns the current value
	 * °setItems(Str items...)Same
	 * °clear()Same
	 * °set ^ setValeu
	 * °setValue(Str s)Same
	 * °append ^ add
	 * °add(Str items...)Same # Add one or more strings.
	 * °addAll(List items)Same
	 */
	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object obj ) {
		switch( method ) {
			case "get":
			case "getValue":
				cr.argsNone();
				return new JMo_Str(this.getValue());

			case "setItems":
				final JMo_Str[] args = (JMo_Str[])cr.argsVar( obj, 0, JMo_Str.class );
				String[] setItems = this.iToStringArray(cr, args);
				this.setItems( setItems );
				return obj;

			case "clear":
				cr.argsNone();
				this.clear();
				return obj;

			case "set":
			case "setValue":
				JMo_Str setValueArg = cr.arg( obj, JMo_Str.class );
				this.setValue( setValueArg.rawString() );
				return obj;

			case "append":
			case "add":
				final JMo_Str[] addArgs = (JMo_Str[])cr.argsVar( obj, 0, JMo_Str.class );
				String[] addItems = this.iToStringArray(cr, addArgs);
				this.add( addItems );
				return obj;

			case "addAll":
				final JMo_List addAllArg = cr.arg( obj, JMo_List.class );
				String[] addAllItems = this.iToStringArray(cr, addAllArg.getInternalCollection());
				this.add( addAllItems );
				return obj;

			default:
				return null;
		}
	}

	private String[] iToStringArray( CallRuntime cr, JMo_Str[] args ) {
		final int len = args.length;
		final String[] items = new String[len];

		for( int i=0; i<len; i++ )
//			items[i] = cr.argType( args[i], JMo_Str.class ).rawString();
			items[i] = args[i].rawString();

		return items;
	}

	private String[] iToStringArray( CallRuntime cr, List<I_Object> args ) {
		final int len = args.size();
		final String[] items = new String[len];

		for( int i=0; i<len; i++ )
			items[i] = cr.argType( args.get( i ), JMo_Str.class ).rawString();

		return items;
	}

}
