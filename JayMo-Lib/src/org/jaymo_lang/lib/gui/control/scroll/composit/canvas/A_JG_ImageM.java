/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll.composit.canvas;

import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.position.POSITION;


/**
 * @author Michael Nitsche
 * @created 25.11.2023
 */
public abstract class A_JG_ImageM implements I_JG_Methods {

	public abstract JMo_Image getImage();
	public abstract POSITION getAlign();
	public abstract void setImage( JMo_Image image );
	public abstract void setAlign( POSITION align );

	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object obj ) {
		switch( method ) {
			case "setImage":
				final JMo_Image img = (JMo_Image)cr.args( obj, JMo_Image.class )[0];
				this.setImage( img );
				return obj;

			case "setAlign":
				final MagicPosition opos = (MagicPosition)cr.args( obj, MagicPosition.class )[0];
				final POSITION pos = opos.get();
				this.setAlign(pos);
				return obj;

//			case "setTip":
//				this.tip(cr);
//				return this;

//			case "@select":
//				//TO DO Check args
//				this.eventAddBlock("@select", c.gBlock());
//				return new Result_Obj(this, true);

			default:
				return null;
		}
	}

}
