/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll;

import org.jaymo_lang.lib.gui.I_JG_Init;
import org.jaymo_lang.lib.gui.JG_Events;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;

/**
 * @author	Michael Nitsche
 * @created	25.11.2023
 */
public abstract class A_JG_ListI implements I_JG_Init {

	protected abstract void arg0list(CallRuntime cr, JMo_List list);
	protected abstract void atSelect( CallRuntime cr, String event );


	@Override
	public final void init( CallRuntime cr, I_Object that, ArgCallBuffer... args) {
		Err.ifNot( args.length == 1 );

		if( args[0] != null ) {
			final JMo_List list = args[0].init( cr, that, JMo_List.class );
			this.arg0list(cr, list);
		}

		// Events
		this.atSelect(cr, JG_Events.SELECT);
	}

	public final boolean validateEvent( String event ) {
		return event.equals( JG_Events.SELECT );
	}

}
