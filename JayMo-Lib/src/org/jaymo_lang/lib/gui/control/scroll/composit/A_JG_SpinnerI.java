/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll.composit;

import org.jaymo_lang.lib.gui.I_JG_Init;
import org.jaymo_lang.lib.gui.JG_Events;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;

/**
 * @author	Michael Nitsche
 * @created	24.11.2023
 */
public abstract class A_JG_SpinnerI implements I_JG_Init {

	protected abstract void argsMinMax( int min, int max );
	protected abstract void atChange( CallRuntime cr, String event );


	@Override
	public final void init( CallRuntime cr, I_Object that, ArgCallBuffer... args) {
		Err.ifNot( args.length, 2 );

		if( args[0] != null || args[1] != null ) {
			Err.ifNull( args[0], args[1] );
			final JMo_Int oMin = args[0].init( cr, that, JMo_Int.class );
			final JMo_Int oMax = args[1].init( cr, that, JMo_Int.class );
			this.argsMinMax( oMin.rawInt( cr ), oMax.rawInt(cr) );
		}

		this.atChange( cr, JG_Events.CHANGE);
	}

	public final boolean validateEvent( String event ) {
		return event.equals( JG_Events.CHANGE );
	}

}
