/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll;

import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 25.11.2023
 */
public abstract class A_JG_ListM implements I_JG_Methods {

	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object obj ) {
		switch( method ) {
			case "add":
				final JMo_Str[] items = (JMo_Str[])cr.argsVar( obj, 1, JMo_Str.class );
				this.add(items);
				return obj;

			case "set":
//			case "setData":
			case "setItems":
				final JMo_List list = (JMo_List)cr.args( obj, JMo_List.class )[0];
				this.setItems( cr, list );
				return obj;

			case "getSelection":
				cr.argsNone();
				return this.getSelection();

//			case "setSelection": TODO

			default:
				return null;
		}
	}

	public abstract void add(JMo_Str[] items);
	public abstract JMo_List getSelection();
	public abstract void setItems( CallRuntime cr, JMo_List items );

}
