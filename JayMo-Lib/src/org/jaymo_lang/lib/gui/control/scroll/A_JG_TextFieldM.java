/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.constant.position.Lib_Position;
import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;


/**
 * @author Michael Nitsche
 * @created 01.08.2023
 */
public abstract class A_JG_TextFieldM implements I_JG_Methods {

	protected abstract void setAlign( POSITION_H align );
	protected abstract void setEditable( boolean b );
	protected abstract String getText();
	protected abstract void setText( String s );


	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object obj ) {
		switch( method ) {
			case "setAlign":
				final MagicPosition mPos = cr.arg( obj, MagicPosition.class );
				final POSITION pos = mPos.get();
				final POSITION_H posH = Lib_Position.getHorizontal( pos );
				if( posH == null )
					throw new RuntimeError( cr, "Invalid position", "Only left, center or right are allowed" );
				this.setAlign(posH);
				return obj;

			case "setEditable":
				final JMo_Bool bObj = cr.arg( obj, JMo_Bool.class );
				final boolean b = Lib_Convert.toBoolean( cr, bObj );
				this.setEditable(b);
				return obj;

			case "setText":
				final JMo_Str sObj = cr.arg( obj, JMo_Str.class );
				final String s = Lib_Convert.toStr( cr, sObj ).rawString();
				this.setText(s);
				return obj;

			case "getText":
				cr.argsNone();
				return new JMo_Str( this.getText() );
		}

		return null;
	}

}
