/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.lib.gui.I_JG_Init;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.con.MagicAxis;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.AXIS;
import de.mn77.base.error.Err;

/**
 * @author	Michael Nitsche
 * @created	27.10.2023
 */
public abstract class A_JG_SliderI implements I_JG_Init {

	protected abstract void arg0align(AXIS align);

	protected abstract void atChange( CallRuntime cr, String event );


	@Override
	public final void init( CallRuntime cr, I_Object that, ArgCallBuffer... args) {
		Err.ifNot( args.length == 1 );

		final AXIS axis = args[0].init( cr, that, MagicAxis.class ).get();
		if( axis == AXIS.Z )
			throw new RuntimeError( cr, "Invalid axis", "Allowed are only horizontal and vertical, but got: " + axis );

		this.arg0align(axis);
		this.atChange( cr, "@change" );
	}

	public final boolean validateEvent( String event ) {
		return event.equals( "@change" );
	}

}
