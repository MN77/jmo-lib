/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.json;

import java.text.ParseException;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Java;

import de.mn77.lib.json.Lib_JsonDeserialize;
import de.mn77.lib.json.Lib_JsonSerialize;


/**
 * @author Michael Nitsche
 * @created 15.03.2021
 */
public class JMo_Json extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "deserialize":
			case "decode":
			case "parse":
				return this.mParse( cr );
			case "object":
				return this.mCreate( cr ); //TODO ?!?
			case "serialize":
			case "encode":
				return this.mSerialize( cr );
		}
		return null;
	}

	/**
	 * °object()JsonObject # Create a new JSON-Object.
	 */
	private I_Object mCreate( final CallRuntime cr ) {
		cr.argsNone();
		return new JMo_JsonObject();
	}

	/**
	 * °parse ^ deserialize
	 * °decode ^ deserialize
	 * °deserialize(Str s)Object # Deserialize a object.
	 */
	private I_Object mParse( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Str.class )[0];
		final String s = Lib_Convert.toStr( cr, arg ).rawString();

		try {
			final Object obj = Lib_JsonDeserialize.parse( s );
			return Lib_Java.javaToJmo( obj ); // TODO go deep
		}
		catch( final ParseException e ) {
			throw new RuntimeError( cr, "JSON parse error", e.getMessage() );
		}
	}

	/**
	 * °encode ^ serialize
	 * °serialize(Object o)Str # Serialize a object.
	 */
	private JMo_Str mSerialize( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_Object.class )[0];

		try {
			final Object obj = Lib_Java.jmoToJava( cr, arg, null );
			return new JMo_Str( Lib_JsonSerialize.encode( obj ) );
		}
		catch( final RuntimeError e ) {
			throw new RuntimeError( cr, "Conversion to JSON failed", "Object from type <" + arg.getTypeName() + "> could not be converted to JSON." );
		}
	}

}
