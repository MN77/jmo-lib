/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.json;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Java;

import de.mn77.lib.json.JsonObject;


/**
 * @author Michael Nitsche
 * @created 15.03.2021
 */
public class JMo_JsonObject extends A_ObjectSimple {

	private final JsonObject jo = new JsonObject();


	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		return this.jo.toString();
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "add":
				return this.add( cr );
		}
		return null;
	}

	/**
	 * °add(Str key, Object obj)Same # Add an object.
	 */
	private JMo_JsonObject add( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, I_Object.class );
		this.jo.add( ((JMo_Str)args[0]).rawString(), Lib_Java.jmoToJava( cr, args[1], null ) );
		return this;
	}

}
