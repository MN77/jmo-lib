/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.json;

import java.util.HashMap;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.util.Lib_Java;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 16.08.2020
 */
public class Util_Json {

	private Util_Json() {}

	
	/**
	 * @param o
	 *            could be null
	 */
	public static I_Object jsonToJMo( final Object o ) {
		return Lib_Java.javaToJmo( o );
	}

	public static I_Object jsonToMap( final HashMap<? extends String, ? extends Object> map, final String... only ) {
		Err.ifNull( map );
		final int size = map.keySet().size();

		final SimpleList<I_Object> keys = new SimpleList<>( size );
		final SimpleList<I_Object> objs = new SimpleList<>( size );

		for( final Object key : map.keySet() )
			if( Lib_Array.contains( only, key ) ) {
				objs.add( Util_Json.jsonToJMo( map.get( key ) ) );
//				key = BL_JSON.iReplace( key );
				keys.add( new JMo_Str( "" + key ) );
			}
		return new JMo_Map( keys, objs );
	}

	public static String jsonToString( final HashMap<? extends String, ? extends Object> map, final String key ) {
		Err.ifNull( map );
		return map.get( key ).toString();
	}

}
