/*******************************************************************************
 * Copyright (C) 2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.ollama;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.lib.json.Json;


/**
 * @author Michael Nitsche
 * @created 07.02.2025
 */
public class U_Ollama {

	public static String defaultModel( final CallRuntime cr, final String host, final int port ) {
		final String response = U_Ollama.fetch( cr, host, port, "api/tags", null ).data();

		try {
			final HashMap<?, ?> map = (HashMap<?, ?>)Json.parse( response );
			final ArrayList<?> list = (ArrayList<?>)map.get( "models" );

			if( list.size() == 0 )
				throw new RuntimeError( cr, "No model available", "" );

			final HashMap<?, ?> firstItem = (HashMap<?, ?>)list.get( 0 );
			return (String)firstItem.get( "name" ); // "model"
		}
		catch( final ParseException e ) {
			throw new ExternalError( cr, "Response parse error", e.getMessage() );
		}
	}

	public static R_OllamaResult fetch( final CallRuntime cr, final String host, final int port, final String target, final String request ) {

		try {
			final StringBuilder sb = new StringBuilder();
			sb.append( "http://" );
			sb.append( host );
			sb.append( ':' );
			sb.append( port );
			sb.append( '/' );
			sb.append( target );

//			MOut.print(sb.toString());
//			String url = "http://localhost:11434/api/generate";

			final URL url = new URL( sb.toString() );
			final HttpURLConnection con = (HttpURLConnection)url.openConnection();

			if( request != null ) {
				// Set request method to POST
				con.setRequestMethod( "POST" );

				// Set request headers
				con.setRequestProperty( "Content-Type", "application/json; charset=UTF-8" );

				// Enable output stream for sending the request body
				con.setDoOutput( true );

				// Send request body
				try( OutputStream os = con.getOutputStream() ) {
					final byte[] input = request.getBytes( "utf-8" );
					os.write( input, 0, input.length );
				}
			}

			final int responseCode = con.getResponseCode();

			if( responseCode == HttpURLConnection.HTTP_OK )
				try( BufferedReader in = new BufferedReader( new InputStreamReader( con.getInputStream() ) ) ) {
					final StringBuilder response = new StringBuilder();

					String inputLine;
					while( (inputLine = in.readLine()) != null )
						response.append( inputLine );

					return new R_OllamaResult( responseCode, response.toString() );
				}
			else
				return new R_OllamaResult( responseCode, "Request failed" );
		}
		catch( final MalformedURLException e ) {
			throw new RuntimeError( cr, "Malformed URL", e.getMessage() );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "Connection error", e.getMessage() );
		}
	}

}
