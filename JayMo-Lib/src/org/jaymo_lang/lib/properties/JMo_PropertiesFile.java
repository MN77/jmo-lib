/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.properties;

import java.io.File;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err_FileSys;
import de.mn77.lib.config.PropertiesFile;


/**
 * @author Michael Nitsche
 * @created 04.05.2022
 */
public class JMo_PropertiesFile extends A_ObjectSimple {

	private PropertiesFile      properties;
	private final ArgCallBuffer argTitle;
	private final ArgCallBuffer argFile;


	/**
	 * +PropertiesFile(Str title, File file)
	 * +PropertiesFile(Str title, Str file)
	 */
	public JMo_PropertiesFile( final Call title, final Call file ) {
		this.argTitle = new ArgCallBuffer( 0, title );
		this.argFile = new ArgCallBuffer( 1, file );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final JMo_Str title = this.argTitle.init( cr, this, JMo_Str.class );
		final I_Object file = this.argFile.initExt( cr, this, JMo_Str.class, JMo_File.class );

		final String titleString = Lib_Convert.toStr( cr, title ).rawString();

		File f = null;

		if( file instanceof JMo_Str ) {
			final String fileString = Lib_Convert.toStr( cr, file ).rawString();
			f = new File( fileString );
		}
		else
			f = ((JMo_File)file).getInternalFile();

		this.properties = new PropertiesFile( titleString, f );
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "read":
				return this.mRead( cr );
			case "write":
				return this.mWrite( cr );
			case "close":
				return this.mClose( cr );

			case "removeKey":
				return this.mRemoveKey( cr );

			case "getBool":
				return this.mGetBool( cr );
			case "getInt":
				return this.mGetInt( cr );
			case "getStr":
				return this.mGetStr( cr );

			case "setBool":
				return this.mSetBool( cr );
			case "setInt":
				return this.mSetInt( cr );
			case "setStr":
				return this.mSetStr( cr );
		}

		return null;
	}

	/**
	 * °close()Nil # Write and close the properties file.
	 */
	private I_Object mClose( final CallRuntime cr ) {
		cr.argsNone();

		try {
			this.properties.close();
		}
		catch( final Err_FileSys e ) {
			throw new ExternalError( cr, "Properties file write error", e.getMessage() );
		}
		return this;
	}

	/**
	 * °getBool(Str key)Bool # Return the assigned value of the given key.
	 * °getBool(Str key, Bool default)Bool # Return the assigned value of the given key or the default value if absent.
	 */
	private JMo_Bool mGetBool( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final String key = Lib_Convert.toStr( cr, args[0] ).rawString();

		if( args.length == 1 ) {
			final Boolean value = this.properties.getBoolean( key );
			if( value == null )
				throw new RuntimeError( cr, "Invalid key", "Key not known: " + key );
			return JMo_Bool.getObject( value );
		}
		else {
			final boolean defaultValue = Lib_Convert.toBoolean( cr, args[1] );
			return JMo_Bool.getObject( this.properties.getBooleanOrDefault( key, defaultValue ) );
		}
	}

	/**
	 * °getInt(Str key)Int # Return the assigned value of the given key.
	 * °getInt(Str key, Int default)Int # Return the assigned value of the given key or the default value if absent.
	 */
	private JMo_Int mGetInt( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final String key = Lib_Convert.toStr( cr, args[0] ).rawString();

		if( args.length == 1 ) {
			final Integer value = this.properties.getInteger( key );
			if( value == null )
				throw new RuntimeError( cr, "Invalid key", "Key not known: " + key );
			return new JMo_Int( value );
		}
		else {
			final int defaultValue = Lib_Convert.toInt( cr, args[1] );
			return new JMo_Int( this.properties.getIntegerOrDefault( key, defaultValue ) );
		}
	}

	/**
	 * °getStr(Str key)Str # Return the assigned value of the given key.
	 * °getStr(Str key, Str default)Str # Return the assigned value of the given key or the default value if absent.
	 */
	private JMo_Str mGetStr( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final String key = Lib_Convert.toStr( cr, args[0] ).rawString();

		if( args.length == 1 ) {
			final String value = this.properties.getString( key );
			if( value == null )
				throw new RuntimeError( cr, "Invalid key", "Key not known: " + key );
			return new JMo_Str( value );
		}
		else {
			final String defaultValue = Lib_Convert.toStr( cr, args[1] ).rawString();
			return new JMo_Str( this.properties.getStringOrDefault( key, defaultValue ) );
		}
	}

	/**
	 * °read()Same # Read and parse the properties file.
	 */
	private I_Object mRead( final CallRuntime cr ) {
		cr.argsNone();

		try {
			this.properties.read();
		}
		catch( final Err_FileSys e ) {
			throw new ExternalError( cr, "Properties file read error", e.getMessage() );
		}
		return this;
	}

	/**
	 * °removeKey(Str key)Same # Remove a key.
	 */
	private I_Object mRemoveKey( final CallRuntime cr ) {
		final JMo_Str key = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		final String keyString = Lib_Convert.toStr( cr, key ).rawString();
		this.properties.removeKey( keyString );
		return this;
	}

	/**
	 * °setBool(Str key, Bool value)Same # Add key if absent and set the value for it.
	 */
	private I_Object mSetBool( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Bool.class );
		final String key = Lib_Convert.toStr( cr, args[0] ).rawString();
		final boolean value = Lib_Convert.toBoolean( cr, args[1] );
		this.properties.setBoolean( key, value );
		return this;
	}

	/**
	 * °setInt(Str key, Int value)Same # Add key if absent and set the value for it.
	 */
	private I_Object mSetInt( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Int.class );
		final String key = Lib_Convert.toStr( cr, args[0] ).rawString();
		final int value = Lib_Convert.toInt( cr, args[1] );
		this.properties.setInteger( key, value );
		return this;
	}

	/**
	 * °setStr(Str key, Str value)Same # Add key if absent and set the value for it.
	 */
	private I_Object mSetStr( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String value = Lib_Convert.toStr( cr, args[1] ).rawString();
		this.properties.setString( key, value );
		return this;
	}

	/**
	 * °write()Same # Write settings to the properties file.
	 */
	private I_Object mWrite( final CallRuntime cr ) {
		cr.argsNone();

		try {
			this.properties.write();
		}
		catch( final Err_FileSys e ) {
			throw new ExternalError( cr, "Properties file write error", e.getMessage() );
		}
		return this;
	}

}
