/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.tcp;

import java.io.IOException;

import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.JMo_Error;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.lib.tcp.TCP_Connection;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_TcpConnection extends A_EventObject implements I_Object {

	private final TCP_Connection connection;


	public JMo_TcpConnection( final TCP_Connection con ) {
		this.connection = con;
	}

	@Override
	public I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
//		switch(cr.getMethod()) {
//			case "@error":
//				eventAddHandler("@error", cr.call);
//				return new Result_Obj(this, true);
//
//			default:
//				return null;
//		}
	}

	@Override
	public void init( final CallRuntime cr ) {}

	/**
	 * °@timeout # Executed when connection timed out.
	 * °@error # If a error occures, this event will be executed.
	 */
	@Override
	public boolean validateEvent( final String event ) {

		switch( event ) {
			case "@error":
			case "@timeout":
				return true;

			default:
				return false;
		}
	}

	@Override
	protected I_Object callMethod( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "send":
				return this.send( cr );

			case "recieveStr":
				return this.mRecieveStr( cr );
			case "recieveInt":
				return this.mRecieveInt( cr );

			case "close":
				return this.close( cr );

			default:
				return null;
		}
	}

	/**
	 * °close()Nil # Close the connection.
	 */
	private I_Object close( final CallRuntime cr ) {
		cr.argsNone();
		this.connection.close();
		return Nil.NIL;
	}

	/**
	 * °recieveInt()Int? # Wait for a incoming integer number.
	 */
	private I_Object mRecieveInt( final CallRuntime cr ) {
		cr.argsNone();

		try {
			final int i = this.connection.recieveInt();
			return new JMo_Int( i );
		}
		catch( final IOException e ) {
			this.eventRun( cr, "@error", new JMo_Error( e ) );
			return Nil.NIL;
		}
	}

	/**
	 * °recieveStr()Str? # Wait for a incoming string.
	 */
	private I_Object mRecieveStr( final CallRuntime cr ) {
		cr.argsNone();

		try {
			final String s = this.connection.recieveString();
			return new JMo_Str( s );
		}
		catch( final IOException e ) {
			this.eventRun( cr, "@error", new JMo_Error( e ) );
			return Nil.NIL;
		}
	}

	/**
	 * °send(Atomic value)Same # Send the value.
	 * TODO Dec, Long, ?!?
	 */
	private I_Object send( final CallRuntime cr ) {
		final I_Object oa = cr.args( this, I_Atomic.class )[0];

		try {

			if( oa instanceof I_IntNumber && !(oa instanceof JMo_Long) ) {
				final int value = Lib_Convert.toInt( cr, oa );
				this.connection.send( value );
			}
			else {
				final String value = Lib_Convert.toStr( cr, oa ).rawString();
				this.connection.send( value );
			}
		}
		catch( final IOException e ) {
			this.eventRun( cr, "@error", new JMo_Error( e ) );
		}
		return this;
	}

}
