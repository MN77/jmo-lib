/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.tcp;

import java.io.IOException;
import java.net.SocketTimeoutException;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.JMo_Error;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;
import de.mn77.lib.tcp.TCP_Client;
import de.mn77.lib.tcp.TCP_Connection;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_TcpClient extends A_EventObject {

	private TCP_Client          client;
	private final ArgCallBuffer par_address, par_port;


	public JMo_TcpClient( final Call address, final Call port ) {
		this.par_address = new ArgCallBuffer( 0, address );
		this.par_port = new ArgCallBuffer( 1, port );
	}

	@Override
	public I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	@Override
	public void init( final CallRuntime cr ) {
		final String address = Lib_Convert.toStr( cr, this.par_address.init( cr, this, JMo_Str.class ) ).rawString();
		final int port = Lib_Convert.toInt( cr, this.par_port.init( cr, this, I_IntNumber.class ) );

		try {
			this.client = new TCP_Client( address, port );
		}
		catch( final IOException e ) {
			Err.exit( e ); //TODO
		}


//		this.client.eVerbunden(verb -> {
//			final TcpConnection con = new TcpConnection(verb);
//			this.eventRun(cr, "@connect", con);
//		});
		this.client.onTimeOut( ( final SocketTimeoutException ste ) -> {
			final JMo_Error e = new JMo_Error( ste );
			JMo_TcpClient.this.eventRun( cr, "@timeout", e );
		} );
		this.client.onError( ( final IOException ioe ) -> {
			final JMo_Error e = new JMo_Error( ioe );
			JMo_TcpClient.this.eventRun( cr, "@error", e );
		} );
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		return type == STYPE.NESTED
			? super.toString()
			: super.toString() + "(\"" + this.par_address.get() + "\":" + this.par_port.get() + ")";//+this.nr+p;
	}

	/**
	 * °@timeout # Executed when connection timed out.
	 * °@error # If a error occures, this event will be executed.
	 */
	@Override
	public boolean validateEvent( final String event ) {

		switch( event ) {
//			case "@connect":
			case "@timeout":
			case "@error":
				return true;

			default:
				return false;
		}
	}

	/**
	 * °connect()TcpConnection? # Try to connect and return either a connection or nil.
	 */
	@Override
	protected I_Object callMethod( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "connect":
				cr.argsNone();
//				final I_Object o = cr.args(this, I_IntNumber.class)[0];
//				final int timeout = Lib_Convert.getIntValue(cr, o); //TODO Timeout!!!
				final TCP_Connection con = this.client.connect();
				return con == null ? Nil.NIL : new JMo_TcpConnection( con );

			default:
				return null;
		}
	}

}
