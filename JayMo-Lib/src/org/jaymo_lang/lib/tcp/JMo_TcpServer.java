/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.tcp;

import java.io.IOException;
import java.net.SocketTimeoutException;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.JMo_Error;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.lib.tcp.TCP_Server;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_TcpServer extends A_EventObject {

	private TCP_Server          server;
	private final ArgCallBuffer par_port;


	public JMo_TcpServer( final Call port ) {
		this.par_port = new ArgCallBuffer( 0, port );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final int p = Lib_Convert.toInt( cr, this.par_port.init( cr, this, I_IntNumber.class ) );

		try {
			this.server = new TCP_Server( p );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "TCP-Network error", e.getMessage() );
		}


		this.server.onConnect( verb -> {
			final JMo_TcpConnection con = new JMo_TcpConnection( verb );
			JMo_TcpServer.this.eventRun( cr, "@connect", con );
		} );
		this.server.onTimeOut( ( final SocketTimeoutException ste ) -> {
			final JMo_Error e = new JMo_Error( ste );
			JMo_TcpServer.this.eventRun( cr, "@timeout", e );
		} );
		this.server.onError( ( final IOException ioe ) -> {
			final JMo_Error e = new JMo_Error( ioe );
			JMo_TcpServer.this.eventRun( cr, "@error", e );
		} );
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		return super.toString() + "(" + this.par_port.get().toString( cr, STYPE.IDENT ) + ")";
	}

	/**
	 * °@connect # Executed on incomming connection.
	 * °@timeout # Executed when connection timed out.
	 * °@error # If a error occures, this event will be executed.
	 */
	@Override
	public boolean validateEvent( final String event ) {

		switch( event ) {
			case "@connect":
			case "@timeout":
			case "@error":
				return true;

			default:
				return false;
		}
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

//	@Override
//	protected String resolveDefault() {
//		return "@connect";
//	}

	/**
	 * °wait(IntNumber num)Nil # Wait for an incoming connection.
	 * °close()Nil # Close this server.
	 */
	@Override
	protected I_Object callMethod( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "wait":
				final I_IntNumber arg = (I_IntNumber)cr.args( this, I_IntNumber.class )[0];
				final int timeout = Lib_Convert.toInt( cr, arg );
				this.server.waitForConnection( timeout * 1000 );
				return Nil.NIL;
			case "close":
				cr.argsNone();
				this.server.close();
				return Nil.NIL;

			default:
				return null;
		}
	}

}
