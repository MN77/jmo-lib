/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.udp;

import java.io.IOException;
import java.net.SocketException;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Chars;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.lib.udp.UdpPush;


/**
 * @author Michael Nitsche
 * @created 12.04.2020
 */
public class JMo_UdpPush extends A_Object {

	private UdpPush             udp;
	private final ArgCallBuffer par_port;


	public JMo_UdpPush( final Call port ) {
		this.par_port = new ArgCallBuffer( 0, port );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final int port = Lib_Convert.toInt( cr, this.par_port.init( cr, this, I_IntNumber.class ) );

		try {
			this.udp = new UdpPush( port );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "UDP-Network error", e.getMessage() );
		}
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		return Lib_Type.getName( this ) + "(" + this.par_port.get().toString( cr, STYPE.IDENT ) + ")";
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "send":
				try {
					this.send( cr );
				}
				catch( final IOException e ) {
					throw new ExternalError( cr, "UDP-Network error", e.getMessage() );
				}
				return this;

			case "close":
				this.close( cr );
				return Nil.NIL;

			default:
				return null;
		}
	}

	/**
	 * °close()Nil # Close the socket.
	 */
	private void close( final CallRuntime cr ) {
		cr.argsNone();

		try {
			this.udp.close();
		}
		catch( final SocketException e ) {
			throw new ExternalError( cr, "UDP-Network error", e.getMessage() );
		}
	}

	/**
	 * °send(Str address, IntNumber port, Chars|Byte|ByteArray data)Same # Send an UDP packet
	 */
	private void send( final CallRuntime cr ) throws IOException {
		final I_Object[] args = cr.argsExt( this, new Class[]{ JMo_Str.class }, new Class[]{ I_IntNumber.class }, new Class[]{ I_Chars.class, JMo_Byte.class, JMo_ByteArray.class } );
		final String address = ((JMo_Str)args[0]).rawString();
		final int port = Lib_Convert.toInt( cr, args[1] );
		final I_Object datax = args[2];

		if( datax instanceof I_Chars oChars) {
			final String datas = oChars.rawString();
			this.udp.push( address, port, datas.getBytes() );
			return;
		}

		if( datax instanceof JMo_Byte ) {
			final byte data = ((JMo_Byte)datax).rawByte();
			this.udp.push( address, port, new byte[]{ data } );
			return;
		}

		if( datax instanceof JMo_ByteArray ) {
			final byte[] ba = ((JMo_ByteArray)datax).getValue();
			this.udp.push( address, port, ba );
			return;
		}
//		if(datax instanceof JMo_ByteArray) {
//			byte[] ba = ((JMo_ByteArray)datax).getInternalObject();
//			this.udp.push(address, port, ba);
//			return;
//		}
		throw new RuntimeError( cr, "Invalid type of data", "Allowed are Chars,Byte and List, but got: " + datax.getTypeName() );
	}

}
