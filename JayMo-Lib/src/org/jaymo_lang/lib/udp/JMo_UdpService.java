/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.udp;

import java.io.IOException;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Chars;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.JMo_Error;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.object.struct.JMo_FunctionMap;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.lib.udp.UDP;
import de.mn77.lib.udp.UDP_Packet;


/**
 * @author Michael Nitsche
 * @created 2020-04-12
 */
public class JMo_UdpService extends A_EventObject {

	private UDP                 udp = null;
	private final ArgCallBuffer par_port;


	public JMo_UdpService( final Call port ) {
		this.par_port = new ArgCallBuffer( 0, port );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final int port = Lib_Convert.toInt( cr, this.par_port.init( cr, this, I_IntNumber.class ) );

		try {
			this.udp = new UDP( port );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "TCP-Network error", e.getMessage() );
		}

		this.udp.onRecieve( paket -> {
			final SimpleList<String> keys = new SimpleList<>();
			final SimpleList<I_Object> objects = new SimpleList<>();

			keys.add( "host" );
			objects.add( new JMo_Str( paket.getAddress().getHostAddress() ) );
			keys.add( "port" );
			objects.add( new JMo_Int( paket.getPort() ) );
			keys.add( "bytes" );
			objects.add( this.iToByteList( paket.getData() ) );
			keys.add( "string" );
			objects.add( new JMo_Str( paket.getString() ) );

			final JMo_FunctionMap con = new JMo_FunctionMap( cr, keys, objects );
			this.eventRun( cr, "@recieve", con );
		} );
		this.udp.onError( ( final IOException ioe ) -> {
			final JMo_Error e = new JMo_Error( ioe );
			this.eventRun( cr, "@error", e );
		} );
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		return super.toString() + "(" + this.par_port.get().toString( cr, STYPE.IDENT ) + ")";
	}

	/**
	 * °@recieve # Executed, if a package comes in.
	 * °@error # If a error occures, this event will be executed.
	 */
	@Override
	public boolean validateEvent( final String event ) {

		switch( event ) {
			case "@recieve":
			case "@error":
				return true;

			default:
				return false;
		}
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °send(Str address, IntNumber port, Chars|Byte|ByteArray data)Same # Send an UDP packet
	 * °start()Same # Start the UDP-Service.
	 * °stop()Nil # Stop the UDP-Service.
	 */
	@Override
	protected I_Object callMethod( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "send":
				final I_Object[] args = cr.argsExt( this, new Class[]{ JMo_Str.class }, new Class[]{ I_IntNumber.class }, new Class[]{ I_Chars.class, JMo_Byte.class, JMo_ByteArray.class } );
				final String address = ((JMo_Str)args[0]).rawString();
				final int port = Lib_Convert.toInt( cr, args[1] );
				final I_Object datax = args[2];

				try {

					if( datax instanceof I_Chars oChars) {
						final String datas = oChars.rawString();
						final UDP_Packet paket = new UDP_Packet( address, port, datas );
						this.udp.send( paket );
					}
					else if( datax instanceof JMo_Byte oByte) {
						final byte data = oByte.rawByte();
						final UDP_Packet paket = new UDP_Packet( address, port, new byte[]{ data } );
						this.udp.send( paket );
					}
					else if( datax instanceof JMo_ByteArray oBA) { // List of Numbers
						final byte[] ba = oBA.getValue();
						final UDP_Packet paket = new UDP_Packet( address, port, ba );
						this.udp.send( paket );
					}
//					else if(datax instanceof JMo_ByteArray) {  // List of JMo_Byte's
//						byte[] ba = ((JMo_ByteArray)datax).getInternalObject();
//						UDP_Paket paket = new UDP_Paket(address, port, ba);
//						this.udp.send(paket);
//					}
					else
						throw new RuntimeError( cr, "Invalid type of data", "Allowed are Chars,Byte and List, but got: " + datax.getTypeName() );
				}
				catch( final IOException e ) {
					throw new ExternalError( cr, "UDP-Network error", e.getMessage() );
				}

				return this;

			case "start":
				cr.argsNone();
				this.udp.start();
				return this;

			case "stop":
				cr.argsNone();
				this.udp.stop();
				return Nil.NIL;

			default:
				return null;
		}
	}

//	@Override
//	protected String resolveDefault() {
//		return "@recieve";
//	}

	private JMo_List iToByteList( final byte[] bytes ) {
		final SimpleList<I_Object> al = new SimpleList<>( bytes.length );
		for( final byte b : bytes )
			al.add( new JMo_Byte( b ) );
		return new JMo_List( al );
	}

}
