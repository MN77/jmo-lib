/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.emoji._test;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 26.01.2021
 */
public class Test_EmojiToChars {

	private static String[] emojis = { "😎", "😉", "🤗", "🦁" };


	public static void main( final String[] args ) {

		for( final String emoji : Test_EmojiToChars.emojis ) {
			MOut.print( emoji );
			final char[] chars = emoji.toCharArray();
			MOut.print( "UTF-8-Chars: " + chars.length );
			for( final char c : chars )
				MOut.print( (int)c );
		}
	}

}
