/*******************************************************************************
 * Copyright (C) 2017-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib._test;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.MFile;


/**
 * @author Michael Nitsche
 *
 */
public class TestFile {

	public static void main( final String[] args ) {

		try {
			final Stopwatch uhr = new Stopwatch();
			TestFile.start();
			uhr.print();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	private static void datei( final String s, final String... args ) throws Err_FileSys {
		final MFile d = new MFile( s );
		TestFile.start( d, args );
	}

	private static void start() throws Err_FileSys {
//		MOut.setMode(DEBUG_MODE.MINIMAL);
		MOut.setJavaErrors( true );

		final String work = "/home/mike/Prog/JayMo/work/";
		final String lib_auto = "/home/mike/Prog/JayMo/lib/tests_auto/";
		final String lib_local = "/home/mike/Prog/JayMo/lib/tests_local/";
		final String lib_man = "/home/mike/Prog/JayMo/lib/tests_man/";
		final String lib_work = "/home/mike/Prog/JayMo/lib/work/";
		final String bricklink = "/home/mike/Prog/JayMo/bricklink/";


//		TestFile.datei(bricklink + "test/test.jmo");
//		TestFile.datei(work + "bugfix_2021-11-28c.jmo");
//		TestFile.datei("/home/mike/Projekte/Turm der Ewigkeit/turm_mastermind.jmo");
//		TestFile.datei("/home/mike/Projekte/Turm der Ewigkeit/git/singleplay.jmo");
//		TestFile.datei(lib_work + "http_server_2.jmo");


//		datei(pfad4+"swing_say4.jmo");
//		TestFile.datei("/mnt/guru/lego/MN77-Steine/Rechnungen/rechnung.jmo");
//		datei(pfad2+"swing_Grid.jmo");
//		datei(pfad4+"events_2_2.jmo");
//		datei(pfad4+"events_2_1.jmo");
//		datei(lib2 + "tcp_server.jmo");

//		TestFile.datei(lib_work + "image_1.jmo");
//		TestFile.datei(lib_work + "rgb_1.jmo");
//		TestFile.datei(lib_work + "swing_windows.jmo");
//		TestFile.datei(lib_work + "swing_layout_border.jmo");
//		TestFile.datei(lib_work + "swing_layout_grid.jmo");
//		TestFile.datei(lib_work + "swing_layout_flow.jmo");
//		TestFile.datei(lib_work + "swing_layout_flex.jmo");
//		TestFile.datei(lib_work + "swing_layout_brick.jmo");

//		TestFile.datei(lib_work + "tcp_server.jmo");
//		TestFile.datei(lib_work + "tcp_client.jmo");

//		TestFile.datei(lib_work + "udp_service.jmo");
//		TestFile.datei("/home/mike/Prog/JMo/Projekt/WieIchDieWeltSehe/" + "abfrage.jmo");

//		TestFile.datei(lib_work + "swing_components.jmo");

//		TestFile.datei(lib_work + "calculator_2.jmo");

//		TestFile.datei(lib_work + "xml_1.jmo");
//		TestFile.datei(lib_work + "swing_test.jmo");

//		TestFile.datei(work + "raster_1.jmo");


//		TestFile.datei(lib_work + "swing_layout_panel.jmo");
//		TestFile.datei("/home/mike/Prog/JMo/bricklink/" + "add_it_low.jmo");
//		TestFile.datei(work + "swing_event_error.jmo");


		TestFile.datei( lib_work + ".jmo" );
	}

	private static void start( final I_File datei, final String... args ) throws Err_FileSys {
		final Parser_App parser = new Parser_App();

		MOut.print( JayMo.NAME + "  " + parser.getVersionString( false, true, true ) );

		final Stopwatch uhr = new Stopwatch();
		MOut.print( "===== " + datei.getName() );
		final App app = parser.parseFile( datei.getFile() );
		MOut.print( uhr );
		MOut.print( "-----------------------------------" );
		app.describe();
		MOut.print( uhr );
		MOut.print( "-----------------------------------" );
//		main.exec();
		app.exec( args );
		MOut.print( uhr );
	}

}
