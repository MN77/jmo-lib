/*******************************************************************************
 * Copyright (C) 2017-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib._test;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 *
 */
public class TestLine {

	public static void main( final String[] args ) {

		try {
			final Stopwatch uhr = new Stopwatch();
			TestLine.start();
			MOut.print( "-----------------------------------" );
			MOut.setDebug( DEBUG_MODE.MINIMAL );
			uhr.print();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

//--------------------------------------------------------------------------------------------------------------------

	private static void run( final String s, final String... args ) throws Err_FileSys {
		final Parser_App parser = new Parser_App();
		final App app = parser.parseText( s );

		MOut.print( JayMo.NAME + "  " + parser.getVersionString( false, true, true ) );

		MOut.print( "-----------------------------------" );
		app.describe();
		MOut.print( "-----------------------------------" );
		app.exec( args );
	}

	private static void start() throws Err_FileSys {
		MOut.setDebug( DEBUG_MODE.NO );

		// Läuft
//		run("Test.hello.print");
//		run("a=AutoTest; a.getTrue.print; a.getFalse.print; a.test.print; a.test(false).print");
//		run("a=LibAutoTest; a.getTrue.print; a.getFalse.print; a.test.print; a.test(false).print");


//		run("Speak.say(\"Hallo\")");
//		run("s=Speak; s.setVoice(\"dfki-pavoque-neutral\"); s.say(\"Hallo\"); s.say(\"5\")");	//TODO Warten bis MOut fertig!
//		run("s=Speak; s.list.print; s.setVoice(\"dfki-pavoque-neutral\"); s.say(\"Hallo Stephan! Wie gehts dir? Was machst du heute noch?\")");

//		run("db=HSqlDB; db.exec(\"CREATE TABLE test1 (nummer INTEGER NOT NULL BLA PRIMARY KEY);\")"); // TODO MOut

//		TestLine.run("tcp=TCP_Server(33333)");

//		TestLine.run("SwingMain(800,300, \"foo\").run");
//		TestLine.run("SwingFrame(\"foo\").size(200,100).title(\"bar\").tee($.looks.print).look(\"CDE/Motif\").add(SwingButton(\"xyz\")).pack.run");
//		TestLine.run("SwingFrame(\"foo\").size(200,100).title(\"bar\").look(\"Nimbus\").add(SwingButton(\"xyz\")).run");
//		TestLine.run("SwingMain(\"foo\").size(200,100).title(\"bar\").run; (5*5).print");

//		TestLine.run("Swing_Main.title(\"Foo\").setSize(600,400).run");
//		TestLine.run("Swing_Main.run");
//		TestLine.run("Swing_Main(200,100).run");
//		TestLine.run("Swing_Main.setSize(200,100).add(Swing_Image( Image(300,300).clear.fill(255,0,255) )).run.maximize");
		TestLine.run( "" );
	}

}
