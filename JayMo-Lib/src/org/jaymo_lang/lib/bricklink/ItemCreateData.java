/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.bricklink;

import java.util.HashMap;
import java.util.Map;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Chars;
import org.jaymo_lang.object.atom.I_DecNumber;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.util.Lib_Math;


/**
 * @author Michael Nitsche
 * @created 22.04.2021
 */
public class ItemCreateData {

	private String item_no        = null; // item.no
	private String item_type      = null; // item.type
	private String color_id       = null;
	private String quantity       = null;
	private String unit_price     = null;
	private String new_or_used    = null;
	private String completeness   = null; //(only when item.type is "set")
	private String description    = ""; // Default
	private String remarks        = ""; // Default
	private String bulk           = "1"; // Default
	private String is_retain      = null;
	private String is_stock_room  = null;
	private String stock_room_id  = null;// (only when is_sock_room is "true")
	private String my_cost        = null;
	private String sale_rate      = null;
	private String tier_quantity1 = null;
	private String tier_price1    = null;
	private String tier_quantity2 = null;
	private String tier_price2    = null;
	private String tier_quantity3 = null;
	private String tier_price3    = null;


	public void check( final CallRuntime cr ) {
		String missing = null;

		if( this.item_no == null )
			missing = "item.no";
		if( this.item_type == null )
			missing = "item.type";
		if( this.color_id == null )
			missing = "color_id";
		if( this.quantity == null )
			missing = "quantity";
		if( this.unit_price == null )
			missing = "unit_price";
		if( this.new_or_used == null )
			missing = "new_or_used";
		if( this.description == null )
			missing = "description";
		if( this.remarks == null )
			missing = "remarks";
		if( this.bulk == null )
			missing = "bulk";
		if( this.is_retain == null )
			missing = "retain";
		if( this.is_stock_room == null )
			missing = "stock_room";
//		if( stock_room_id (only when is_sock_room is "true")	//TODO?
//		if( this.my_cost == null)	//TODO?
//			missing = "my_cost";
		if( this.sale_rate == null )
			missing = "sale_rate";

		if( this.item_type != null && this.item_type.toLowerCase().equals( "set" ) && this.completeness == null )
			missing = "completeness";

		if( this.tier_quantity1 != null
			|| this.tier_price1 != null
			|| this.tier_quantity2 != null
			|| this.tier_price2 != null
			|| this.tier_quantity3 != null
			|| this.tier_price3 != null ) {
			if( this.tier_quantity1 == null )
				missing = "tier_quantity1";
			if( this.tier_price1 == null )
				missing = "tier_price1";
			if( this.tier_quantity2 == null )
				missing = "tier_quantity2";
			if( this.tier_price2 == null )
				missing = "tier_price2";
			if( this.tier_quantity3 == null )
				missing = "tier_quantity3";
			if( this.tier_price3 == null )
				missing = "tier_price3";
		}

		if( missing != null )
			throw new RuntimeError( cr, "Missing value to create inventory", "Missing: " + missing );
	}

	public void fill( final CallRuntime cr, final HashMap<I_Object, I_Object> data ) {

		for( final Map.Entry<I_Object, I_Object> e : data.entrySet() ) {
			final String key = Lib_Convert.toStr( cr, e.getKey() ).rawString().toLowerCase();

			// TODO Wenn der Typ eines Wertes nicht stimmt, ist die Fehlermeldungen nicht ganz klar. Es geht nicht daraus hervor, dass es sich nicht um die Map, sondern um einen Wert IN der Map handelt!
			switch( key ) {
				case "item.no":
					this.item_no = cr.argType( e.getValue(), JMo_Str.class ).toString();
					break;
				case "item.type":
					this.item_type = cr.argType( e.getValue(), JMo_Str.class ).toString();
					this.item_type = Util_Bricklink.checkItemType( cr, this.item_type );
					break;
				case "color_id":
					this.color_id = cr.argType( e.getValue(), I_IntNumber.class ).toString();
					break;
				case "quantity":
					this.quantity = cr.argType( e.getValue(), I_IntNumber.class ).toString();
					break;
				case "unit_price":
					this.unit_price = cr.argType( e.getValue(), I_DecNumber.class ).toString();
					break;
				case "new_or_used":
					final I_Object nuObject = cr.argTypeExt( e.getValue(), new Class[]{ JMo_Bool.class, I_Chars.class } );
					boolean isNew = true;
					if( nuObject instanceof JMo_Bool )
						isNew = Lib_Convert.toBoolean( cr, nuObject );
					else
						isNew = Lib_Convert.toStr( cr, nuObject ).rawString().toUpperCase().equals( "N" );
					this.new_or_used = isNew ? "N" : "U";
					break;
				case "completeness": //(only when this.item.type is "set")	// C: Complete, B: Incomplete, S: Sealed
					this.completeness = cr.argType( e.getValue(), JMo_Str.class ).toString();
					this.completeness = Util_Bricklink.checkSetCompleteness( cr, this.completeness );
					break;
				case "description":
					this.description = cr.argType( e.getValue(), JMo_Str.class ).toString();
					break;
				case "remarks":
					this.remarks = cr.argType( e.getValue(), JMo_Str.class ).toString();
					break;
				case "bulk":
					this.bulk = cr.argType( e.getValue(), I_IntNumber.class ).toString();
					break;
				case "is_retain":
					this.is_retain = cr.argType( e.getValue(), JMo_Bool.class ).toString();
					break;
				case "is_stock_room":
					this.is_stock_room = cr.argType( e.getValue(), JMo_Bool.class ).toString();
					break;
				case "stock_room_id": //(only when is_sock_room is "true")	// A, B, C
					this.stock_room_id = cr.argType( e.getValue(), JMo_Str.class ).toString();
					this.stock_room_id = Util_Bricklink.checkStockRoomID( cr, this.stock_room_id );
					break;
				case "my_cost":
					this.my_cost = cr.argType( e.getValue(), I_DecNumber.class ).toString();
					break;
				case "sale_rate":
					final int sale = Lib_Convert.toInt( cr, cr.argType( e.getValue(), I_IntNumber.class ) );
					this.sale_rate = "" + Lib_Math.limit( 0, 100, sale );
					break;

				case "tier_quantity1":
					this.tier_quantity1 = cr.argType( e.getValue(), I_IntNumber.class ).toString();
					break;
				case "tier_price1":
					this.tier_price1 = cr.argType( e.getValue(), I_DecNumber.class ).toString();
					break;
				case "tier_quantity2":
					this.tier_quantity2 = cr.argType( e.getValue(), I_IntNumber.class ).toString();
					break;
				case "tier_price2":
					this.tier_price2 = cr.argType( e.getValue(), I_DecNumber.class ).toString();
					break;
				case "tier_quantity3":
					this.tier_quantity3 = cr.argType( e.getValue(), I_IntNumber.class ).toString();
					break;
				case "tier_price3":
					this.tier_price3 = cr.argType( e.getValue(), I_DecNumber.class ).toString();
					break;

				default:
					throw new RuntimeError( cr, "Invalid key", "Key is not known for inventory item: " + key );
			}
		}
	}

	public Map<String, String> getMap() {
		final Map<String, String> map = new HashMap<>();
		map.put( "item.no", this.item_no );
		map.put( "item.type", this.item_type );
		map.put( "color_id", this.color_id );
		map.put( "quantity", this.quantity );
		map.put( "unit_price", this.unit_price );
		map.put( "new_or_used", this.new_or_used );
		if( this.completeness != null && this.item_type.toUpperCase().equals( "SET" ) )
			map.put( "completeness", this.completeness ); //(only when item.type is "set")
		map.put( "description", this.description );
		map.put( "remarks", this.remarks );
		map.put( "bulk", this.bulk );
		map.put( "is_retain", this.is_retain );
		map.put( "is_stock_room", this.is_stock_room );
		if( this.stock_room_id != null && this.is_stock_room.equals( "true" ) )
			map.put( "stock_room_id", this.stock_room_id ); //(only when is_sock_room is "true");
		map.put( "my_cost", this.my_cost );
		map.put( "sale_rate", this.sale_rate );
		map.put( "tier_quantity1", this.tier_quantity1 );
		map.put( "tier_price1", this.tier_price1 );
		map.put( "tier_quantity2", this.tier_quantity2 );
		map.put( "tier_price2", this.tier_price2 );
		map.put( "tier_quantity3", this.tier_quantity3 );
		map.put( "tier_price3", this.tier_price3 );

		return map;
	}

}
