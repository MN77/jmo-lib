/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.bricklink;

import java.util.HashSet;
import java.util.function.Function;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.convert.ConvertString;


/**
 * @author Michael Nitsche
 * @created 23.04.2021
 */
public class BricklinkIncExc {

	private final String[]  values;
	private final boolean[] not;


	public BricklinkIncExc( final CallRuntime cr, I_Object value ) {
		final HashSet<String> valueSet = new HashSet<>();

		value = cr.argTypeExt( value, new Class<?>[]{ Nil.class, JMo_Str.class, JMo_List.class, I_IntNumber.class } );

		if( value instanceof Nil ) {}
		else if( value instanceof JMo_List )
			for( final I_Object o : ((JMo_List)value).getInternalCollection() )
				valueSet.add( Lib_Convert.toStr( cr, o ).rawString() );
		else { // Str, IntNumber
			final String s = Lib_Convert.toStr( cr, value ).rawString();
			valueSet.addAll( ConvertString.toList( ',', s ) );
		}

		this.values = valueSet.toArray( new String[valueSet.size()] );

		this.not = new boolean[this.values.length];

		for( int i = 0; i < this.values.length; i++ ) {
			this.not[i] = this.values[i].startsWith( "-" );
			if( this.not[i] )
				this.values[i] = this.values[i].substring( 1 );
		}

		// TODO The following is currently necessary, because more arguments lead to signatur errors
		if( this.values.length > 1 )
			throw new RuntimeError( cr, "Got more than one argument", ConvertArray.toString( ",", (Object[])this.values ) );
	}

	public void check( final Function<String, String> f ) {
		for( int i = 0; i < this.values.length; i++ )
			this.values[i] = f.apply( this.values[i] );
	}

	public String compute() {
		final StringBuilder result = new StringBuilder();

		for( int i = 0; i < this.values.length; i++ ) {
			if( i > 0 )
				result.append( ',' );
			if( this.not[i] )
				result.append( '-' );
			result.append( this.values[i] );
		}
		return result.toString();
	}

//	public String get(int index) {
//		return this.values[index];
//	}

//	public void set(int index, String value) {
//		this.values[index] = value;
//	}

	public boolean hasItems() {
		return this.values.length > 0;
	}

	public int size() {
		return this.values.length;
	}

}
