/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.bricklink;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.lib.json.JsonObject;


/**
 * @author Michael Nitsche
 * @created 21.08.2020
 */
public class Util_Bricklink {

	private Util_Bricklink() {}
	
	
	public static String checkCouponState( final CallRuntime cr, final String s ) {
		final String s2 = s.toUpperCase();

		switch( s2 ) {
			case "O": //open
			case "S": //redeemed
			case "D": //denied
			case "E": //expired
				return s2;
			default:
				throw new RuntimeError( cr, "Invalid coupon status", "Valid are ('O','S','D','E') but got: " + s );
		}
	}

	public static int checkInteger( final CallRuntime cr, final String value ) {

		try {
			return Integer.parseInt( value );
		}
		catch( final NumberFormatException e ) {
			throw new RuntimeError( cr, "Invalid integer number", "Got: " + value );
		}
	}

	public static String checkInventoryStatus( final CallRuntime cr, final String id ) {
		final String s2 = id.toUpperCase();

		switch( s2 ) {
			case "Y": //available
			case "S": //in stockroom A
			case "B": //in stockroom B
			case "C": //in stockroom C
			case "N": //unavailable
			case "R": //reserved
				return s2;
			default:
				throw new RuntimeError( cr, "Invalid set inventory status", "Valid are ('Y','S','B','C','N','R') but got: " + id );
		}
	}

	public static String checkItemType( final CallRuntime cr, final String type ) {
		final String s2 = type.toUpperCase();

		switch( s2 ) {
			case "MINIFIG":
			case "PART":
			case "SET":
			case "BOOK":
			case "GEAR":
			case "CATALOG":
			case "INSTRUCTION":
			case "UNSORTED_LOT":
			case "ORIGINAL_BOX":
				return s2;
			default:
				throw new RuntimeError( cr, "Invalid item type", "Unknown item type: " + type );
		}
	}

	public static String checkOrderState( final CallRuntime cr, final String s ) {
		final String s2 = s.toLowerCase();

		switch( s2 ) {
			case "pending":
			case "updated":
			case "processing":
			case "ready":
			case "paid":
			case "packed":
			case "shipped":
			case "received":
			case "completed":
			case "ocr":
			case "npb":
			case "npx":
			case "nrs":
			case "nss":
			case "cancelled":
			case "purged":
				return s2;
			default:
				throw new RuntimeError( cr, "Invalid order state", "Unknown order state: " + s );
		}
	}

	public static String checkPaymentState( final CallRuntime cr, final String s ) {
		final String s2 = Lib_String.capitalizeWords( s.toLowerCase() );

		switch( s2 ) {
			case "None":
			case "Sent":
			case "Received":
			case "Clearing":
			case "Returned":
			case "Bounced":
			case "Completed":
				return s2;
			default:
				throw new RuntimeError( cr, "Invalid payment state", "Unknown payment state: " + s );
		}
	}

	public static String checkSetCompleteness( final CallRuntime cr, final String id ) {
		final String s2 = id.toUpperCase();

		switch( s2 ) {
			case "C": // C: Complete, B: Incomplete, S: Sealed
			case "B":
			case "S":
				return s2;
			default:
				throw new RuntimeError( cr, "Invalid set complete status", "Valid are ('C','B','S') but got: " + id );
		}
	}

	public static String checkStockRoomID( final CallRuntime cr, final String id ) {
		final String s2 = id.toUpperCase();

		switch( s2 ) {
			case "A":
			case "B":
			case "C":
				return s2;
			default:
				throw new RuntimeError( cr, "Invalid stockroom id", "Valid are ('A','B','C') but got: " + id );
		}
	}

	public static Map<String, Object> convertDotToTree( final Map<String, String> props ) {
		final Map<String, Object> target = new HashMap<>();
		for( final Entry<String, String> e : props.entrySet() )
			Util_Bricklink.iconvertDotAdd( target, e.getKey(), e.getValue() );

		return target;
	}

	/**
	 * @apiNote
	 *          color_id can be null
	 */
	public static String getImageURL( final String type, final String no, final Integer color_id ) {
		final boolean nc = color_id == null;

		switch( type ) {
			case "PART":
				return nc ? "https://www.bricklink.com/PL/" + no + ".jpg" : "https://img.bricklink.com/ItemImage/PN/" + color_id + "/" + no + ".png";
			case "MINIFIG":
				return "https://www.bricklink.com/ML/" + no + ".jpg";
			case "SET":
				return "https://img.bricklink.com/ItemImage/SN/0/" + no + ".png";
			case "BOOK":
				return "https://www.bricklink.com/BL/" + no + ".jpg";
			case "GEAR":
				return "https://img.bricklink.com/ItemImage/GN/0/" + no + ".png";
			case "CATALOG":
				return "https://img.bricklink.com/ItemImage/CN/0/" + no + ".png";
			case "INSTRUCTION":
				return "https://img.bricklink.com/ItemImage/IN/0/" + no + ".png";
			case "UNSORTED_LOT":
				return null; // Must be requested from Bricklink
			case "ORIGINAL_BOX":
				return "https://img.bricklink.com/ItemImage/ON/0/" + no + ".png";
			default:
				return null;
		}
	}

	static String getBricklinkUrl( final CallRuntime cr, final BL_METHOD method, final String url, final BL_Auth auth, final Map<String, String> propertys ) {
		final Map<String, String> params = Util_Bricklink.getBricklinkMap( cr, method, url, auth, propertys );
//		MOut.temp(params);
		final JsonObject obj = new JsonObject();
		obj.addAll( params );

		final String jsonString = obj.toString(); //.toJSONString();
//		System.out.println( jsonString );

		String args = "?";
		if( propertys != null )
			for( final Map.Entry<String, String> e : propertys.entrySet() )
				args += e.getKey() + "=" + e.getValue() + "&";

		try {
//			URI uri = new URI("https", "//api.bricklink.com/api/store/v1/orders?direction=in&Authorization="+jsonString, null);
//			URI uri = new URI("https", "//api.bricklink.com/api/store/v1"+"/items/PART/3070b/price?color_id=11&new_or_used=U&guide_type=sold&country_code=DE&Authorization="+jsonString, null);
			final URI uri = new URI( "https", "//api.bricklink.com/api/store/v1" + url + args + "Authorization=" + jsonString, null );

			return uri.toASCIIString();
		}
		catch( final URISyntaxException e ) {
			throw Err.exit( e );
		}
	}

	private static Map<String, String> getBricklinkMap( final CallRuntime cr, final BL_METHOD method, final String url, final BL_Auth auth, final Map<String, String> propertys ) {
		if( auth == null )
			throw new RuntimeError( cr, "Authentification-Error", "No authentification informations set." );

		final BLAuthSigner signer = new BLAuthSigner( auth.consumerKey( cr ), auth.consumerSecret( cr ) );
		signer.setToken( auth.tokenValue( cr ), auth.tokenSecret( cr ) );
//		signer.setVerb( "GET" );
		signer.setVerb( method.getString() );
//		signer.setURL( "https://api.bricklink.com/api/store/v1/orders" );
//		signer.addParameter( "direction", "in" );
		signer.setURL( "https://api.bricklink.com/api/store/v1" + url );

		if( propertys != null )
			for( final Map.Entry<String, String> e : propertys.entrySet() )
				signer.addParameter( e.getKey(), e.getValue() );

//		signer.addParameter( "color_id", "11" );
//		signer.addParameter( "new_or_used", "U" );
//		signer.addParameter("guide_type", "sold");
//		signer.addParameter("country_code", "DE");

		Map<String, String> params = Collections.emptyMap();

		try {
			params = signer.getFinalOAuthParams();
		}
		catch( final Exception e ) {
			e.printStackTrace();
		}
		return params;
	}

	@SuppressWarnings( "unchecked" )
	private static void iconvertDotAdd( final Map<String, Object> target, final String key, final String value ) {

		if( !key.contains( "." ) )
			target.put( key, value );
		else {
			final int dotIdx = key.indexOf( '.' );
			final String base = key.substring( 0, dotIdx );
			final String next = key.substring( dotIdx + 1 );
//			MOut.temp(dotIdx, base, next);

			Map<String, Object> target2 = null;

			if( target.containsKey( base ) )
				target2 = (Map<String, Object>)target.get( base );
			else {
				target2 = new HashMap<>();
				target.put( base, target2 );
			}
			Util_Bricklink.iconvertDotAdd( target2, next, value );
		}
	}

}
