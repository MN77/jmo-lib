/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jmo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.bricklink;

public class OAuthConstants {

	public static final String TIMESTAMP       = "oauth_timestamp";
	public static final String SIGN_METHOD     = "oauth_signature_method";
	public static final String SIGNATURE       = "oauth_signature";
	public static final String CONSUMER_SECRET = "oauth_consumer_secret";
	public static final String CONSUMER_KEY    = "oauth_consumer_key";
	public static final String CALLBACK        = "oauth_callback";
	public static final String VERSION         = "oauth_version";
	public static final String NONCE           = "oauth_nonce";
	public static final String PARAM_PREFIX    = "oauth_";
	public static final String TOKEN           = "oauth_token";
	public static final String TOKEN_SECRET    = "oauth_token_secret";
	public static final String OUT_OF_BAND     = "oob";
	public static final String VERIFIER        = "oauth_verifier";
	public static final String HEADER          = "Authorization";
	public static final String SCOPE           = "scope";

}
