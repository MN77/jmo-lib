/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.bricklink;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 21.08.2020
 */
public class BL_Auth {

	private String consumerKey    = null;
	private String consumerSecret = null;
	private String tokenValue     = null;
	private String tokenSecret    = null;


	public String consumerKey( final CallRuntime cr ) {
		this.error( cr, this.consumerKey, "Consumer-Key" );
		return this.consumerKey;
	}

	public String consumerSecret( final CallRuntime cr ) {
		this.error( cr, this.consumerSecret, "Consumer-Secret" );
		return this.consumerSecret;
	}

	public void setAuth( final String consumerKey, final String consumerSecret, final String tokenValue, final String tokenSecret ) {
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
		this.tokenValue = tokenValue;
		this.tokenSecret = tokenSecret;
	}

	public void setConsumer( final String consumerKey, final String consumerSecret ) {
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
	}

	public void setToken( final String tokenValue, final String tokenSecret ) {
		this.tokenValue = tokenValue;
		this.tokenSecret = tokenSecret;
	}

	public String tokenSecret( final CallRuntime cr ) {
		this.error( cr, this.tokenSecret, "Token-Secret" );
		return this.tokenSecret;
	}

	public String tokenValue( final CallRuntime cr ) {
		this.error( cr, this.tokenValue, "Token-Value" );
		return this.tokenValue;
	}

	private void error( final CallRuntime cr, final String value, final String what ) {
		if( value == null )
			throw new RuntimeError( cr, "Authentication incomplete", what + " is missing" );
	}

}
