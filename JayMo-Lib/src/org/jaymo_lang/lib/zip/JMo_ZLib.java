/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.zip;

import java.io.UnsupportedEncodingException;
import java.util.zip.DataFormatException;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.lib.zip.Lib_ZLib;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 */
public class JMo_ZLib extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "encode":
			case "compress":
				return this.mCompress( cr );
			case "decode":
			case "decompress":
				return this.mDecompress( cr );
		}
		return null;
	}


	/*
	 * °encode ^ compress
	 * °compress(Str|ByteArray data)ByteArray # Compress bytes of argument with ZLib.
	 */
	private JMo_ByteArray mCompress( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_Str.class, JMo_ByteArray.class );

		byte[] ba = null;

		try {
			if( arg instanceof JMo_Str )
				ba = Lib_ZLib.compress( ((JMo_Str)arg).rawString() );
			else // instanceof JMo_ByteArray
				ba = Lib_ZLib.compress( ((JMo_ByteArray)arg).getValue() );
		}
		catch( final UnsupportedEncodingException e ) {
			throw new RuntimeError( cr, "Compress error", e.getMessage() );
		}

		return new JMo_ByteArray( ba );
	}

	/*
	 * °decode ^ decompress
	 * °decompress(ByteArray data)ByteArray # Decompress a ByteArray with ZLib.
	 */
	private JMo_ByteArray mDecompress( final CallRuntime cr ) {
		final JMo_ByteArray arg = (JMo_ByteArray)cr.args( this, JMo_ByteArray.class )[0];

		try {
			final byte[] ba = Lib_ZLib.decompress( arg.getValue() );
			return new JMo_ByteArray( ba );
		}
		catch( UnsupportedEncodingException | DataFormatException e ) {
			throw new RuntimeError( cr, "Decompress error", e.getMessage() );
		}
	}

}
